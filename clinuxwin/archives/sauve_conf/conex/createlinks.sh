#!/bin/bash

# Creation du dossier pour les liens
if [ ! -e "/home/$USER/link" ]; then
mkdir /home/$USER/link
fi

# Liens sur le bureau des utilisateurs
#ln -s /media/classes /home/$USER/Desktop/Classes
#ln -s /media/partages /home/$USER/Desktop/Partages

# Copie des raccourcis de base
cp /media/partages/persolinks/base/*.desktop /home/$USER/Desktop/
cp /media/partages/persolinks/base/* /home/$USER/link/

# Copie des raccourcis du parc du client
PARC=$(cat /etc/parc.txt)
cp /media/partages/persolinks/$PARC/*.desktop /home/$USER/Desktop/
cp /media/partages/persolinks/$PARC/* /home/$USER/link/

# Reglage de l'affichage en liste dans les fenetres de pcmanfm
sed -i 's/icon/list/g' /home/$USER/.config/pcmanfm/LXDE/pcmanfm.conf
sed -i 's/hidden=1/hidden=0/g' /home/$USER/.config/pcmanfm/LXDE/pcmanfm.conf

# Applications par defaut
cat /mnt/netlogon/conex/mimeapps.list > /home/$USER/.config/mimeapps.list

# Autres scripts a lancer
bash /mnt/netlogon/conex/alsavolume.sh
bash /mnt/netlogon/conex/profil_ff.sh
bash /mnt/netlogon/conex/clean_vartmp.sh
#bash /mnt/netlogon/conex/warn_overfill.sh

# Mise à jour des menus ?
update-menus

exit 0

