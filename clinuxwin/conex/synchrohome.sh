#!/bin/bash

DATE1=$(date +%F+%0kh%0M)

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

lulu=$(who | grep -v root | cut -d" " -f1)

LOG=/tmp/synchro_$lulu.log

(
echo ""
echo "======================================================="
echo "Lancement de la synchronisation : $DATE1"
# Test et copie des raccourcis de groupe
w=$(groups $lulu | grep eleves)
        if [ ! -n "$w" ]; then

        # Resultat vrai, donc le compte n'appartient pas au groupe eleves.
        echo "$USER n'est pas membre du groupe eleves"

        # On interroge alors le groupe professeurs.
        z=$(groups $lulu | grep professeurs)
                if [ ! -n "$z" ]; then

                # Resultat vrai, donc le compte n'appartient pas au groupe professeurs.
                echo "$lulu n'est pas membre du groupe professeurs"
                else

                # Sinon résultat faux, le compte appartient au groupe professeurs.
                echo "$lulu est membre du groupe professeurs"

                # Synchro des documents de l'utilisateur prof
                #mkdir -p /media/Professeurs/$USER/MesDocs
                rsync -avz --exclude=*.desktop /home/$lulu/* /media/Professeurs/$lulu/MesDocs/
		rsync -avz /home/$lulu/.mozilla /media/Professeurs/$lulu/MonFF/
                #cp -r /home/$lulu/Desktop/* /media/Professeurs/$lulu/MesDocs
                #chown -R $lulu: /media/Professeurs/$lulu/MesDocs
                #exit
                fi
        else
	# Sinon le compte appartient au groupe eleves.
        echo "$USER est membre du groupe eleves"

        # Synchro des documents de l'utilisateur eleve
        #mkdir -p /media/Eleves/$USER/MesDocs
        rsync -avz --exclude=*.desktop /home/$lulu/* /media/Eleves/$lulu/MesDocs/
	rsync -avz /home/$lulu/.mozilla /media/Eleves/$lulu/MonFF/
	#chown -R $lulu: /media/Eleves/$lulu/MesDocs
        #exit
        fi


#sed -i 's/icon/list/g' /home/$USER/.config/pcmanfm/LXDE/pcmanfm.conf
#killall -u $USER

sleep 1
#rm -f /home/$USER/.mozilla/firefox/default/.parentlock
#umount /media/Professeurs
#umount /media/Eleves
#umount /media/Echange
#umount /home/$USER
) 2>&1 | tee -a $LOG

echo -e "$VERT"
echo "Synchro terminée!"
echo ""
echo "Appuyez sur une touche pour sortir"
echo -e "$COLTXT"
read lapin
exit 0


