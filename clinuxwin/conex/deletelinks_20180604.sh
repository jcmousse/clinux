#!/bin/sh

DATELOG=$(date +%F+%0k:%0M:%S)

lulu=$(cat /tmp/pamuser.log)
#lulu=$(who | grep -v root | cut -d" " -f1)

LOG=/tmp/deconex_$lulu.log

#exec 1>/root/deconnexion1.log
#exec 2>/root/deconnexion2.log
#exec 3>/root/deconnexion3.log

(
echo "=========================================="
echo "utilisateur : $lulu - date : $DATELOG" 

# Suppression des liens bureau
rm -f /home/$lulu/Desktop/Echange
rm -f /home/$lulu/Desktop/Documents_perso

# Suppression des raccourcis copiés à la connexion
ls /home/$lulu/link | while read A ; do rm -f /home/$lulu/Desktop/$A ; done
ls /home/$lulu/link | while read A ; do rm -f /home/$lulu/Bureau/$A ; done
sleep 1
rm -rf /home/$lulu/link/*

#############################
# Test et copie des raccourcis de groupe
u=$(groups $lulu | grep eleves)
	if [ ! -n "$u" ]; then

        	# Resultat vrai, donc le compte n'appartient pas au groupe eleves.
		echo "$lulu n'est pas membre du groupe eleves"

		# On interroge alors le groupe professeurs.
        	v=$(groups $lulu | grep professeurs)

		if [ ! -n "$v" ]; then
		# Resultat vrai, donc le compte n'appartient pas au groupe professeurs.
		echo "$lulu n'est pas membre du groupe professeurs"
    		else
       		# Sinon résultat faux, le compte appartient au groupe professeurs.
		echo "$lulu est membre du groupe professeurs"

		# Synchro documents profs
		#rsync -avz --exclude=*.desktop /home/$lulu/ /media/Professeurs/$lulu/MesDocs/ &
		#rsync -avz --delete-after /home/$lulu/.mozilla /media/Professeurs/$lulu/MonFF/ &
		rsync -rv --exclude=*.desktop /home/$lulu/ /media/Professeurs/$lulu/MesDocs/ &
		rsync -rv --delete-after /home/$lulu/.mozilla /media/Professeurs/$lulu/MonFF/ &
		sleep 1
    		fi
	else
        # Sinon, le compte appartient au groupe eleves.
	echo "$lulu est membre du groupe eleves"

	# Synchro documents eleves
	#rsync -avz --exclude=*.desktop /home/$lulu/* /media/Eleves/$lulu/MesDocs &
	#rsync -avz --delete-after /home/$lulu/.mozilla /media/Eleves/$lulu/MonFF/ &
	rsync -rv --exclude=*.desktop /home/$lulu/* /media/Eleves/$lulu/MesDocs &
	rsync -rv --delete-after /home/$lulu/.mozilla /media/Eleves/$lulu/MonFF/ &
	sleep 1
	fi

################################
# Synchro des documents
#rsync -avz --exclude=*.desktop /home/$lulu/* /media/Professeurs/$lulu/MesDocs/
#rsync -avz --exclude=*.desktop /home/$lulu/* /media/Eleves/$lulu/MesDocs/
#chown -R $lulu:utilisateurs\ du\ domaine /media/Professeurs/$lulu/MesDocs
#chown -R $lulu:utilisateurs\ du\ domaine /media/Eleves/$lulu/MesDocs

sleep 1
#sed -i 's/icon/list/g' /home/$lulu/.config/pcmanfm/LXDE/pcmanfm.conf
#killall -u $lulu
#sleep 1
#rm -f /home/$lulu/.mozilla/firefox/default/.parentlock
#umount /media/Professeurs
#umount /media/Eleves
#umount /media/Echange
#umount /home/$lulu

# Droits sur les répertoires utilisateur
#lulu=$(who | grep -v root | cut -d" " -f1)
chown -R $lulu:utilisateurs\ du\ domaine /home/$lulu
chown -R $lulu:utilisateurs\ du\ domaine /media/Professeurs/$lulu
chown -R $lulu:utilisateurs\ du\ domaine /media/Eleves/$lulu

) 2>&1 | tee -a $LOG
#killall -u $lulu
exit 0
