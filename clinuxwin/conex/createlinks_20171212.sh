#!/bin/bash

# Creation du dossier pour les liens
if [ ! -e "/home/$USER/link" ]; then
mkdir /home/$USER/link
fi

# Liens sur le bureau des utilisateurs
#ln -s /media/classes /home/$USER/Desktop/Classes
#ln -s /mnt/netlogon /home/$USER/Desktop/Partages

# Copie des raccourcis de base
cp /mnt/netlogon/persolinks/base/*.desktop /home/$USER/Desktop/
cp /mnt/netlogon/persolinks/base/*.desktop /home/$USER/Bureau/
cp /mnt/netlogon/persolinks/base/* /home/$USER/link/

# Copie des raccourcis du parc du client
PARC=$(cat /etc/parc.txt)
cp /mnt/netlogon/persolinks/$PARC/*.desktop /home/$USER/Desktop/
cp /mnt/netlogon/persolinks/$PARC/*.desktop /home/$USER/Bureau/
cp /mnt/netlogon/persolinks/$PARC/* /home/$USER/link/

# Test et copie des raccourcis de groupe
u=$(groups $USER | grep eleves)
	if [ ! -n "$u" ]; then
        # Resultat vrai, donc le compte n'appartient pas au groupe eleves.
		echo "$USER n'est pas membre du groupe eleves"
	# On interroge alors le groupe profs.
        	v=$(groups $USER | grep profs)
		if [ ! -n "$v" ]; then
		echo "$USER n'est pas membre du groupe profs"
		# Resultat vrai, donc le compte n'appartient pas au groupe profs.
    		else
       		# Sinon, la recherche a donné un résultat, donc le compte appartient au groupe profs.
		echo "$USER est membre du groupe profs"
        	# Copie des raccourcis profs
		cp /mnt/netlogon/persolinks/profs/*.desktop /home/$USER/Desktop/
		cp /mnt/netlogon/persolinks/profs/*.desktop /home/$USER/Bureau/
		cp /mnt/netlogon/persolinks/profs/* /home/$USER/link/
			# Si veyon-master est installé sur le client, on copie aussi un raccourci veyon-prof sur le bureau
			#if [ -e \"/usr/bin/veyon-master\" ] ; then
			#echo \"veyon-master est installé sur ce poste, on copie le raccourci.\"
			#cp /mnt/netlogon/persolinks/autres/veyon-prof.desktop /home/\$USER/Bureau/
			#cp /mnt/netlogon/persolinks/autres/veyon-prof.desktop /home/\$USER/Desktop/
			#cp /mnt/netlogon/persolinks/autres/veyon-prof.desktop /home/\$USER/links/
			#else
			#echo \"veyon-master n'est pas installé sur ce poste."
			#fi
    		fi
	else
        # Sinon, la recherche a donné un résultat, donc le compte appartient au groupe eleves.
	echo "$USER est membre du groupe eleves"
        # Copie des raccourcis eleves
	cp /mnt/netlogon/persolinks/eleves/*.desktop /home/$USER/Bureau/
	cp /mnt/netlogon/persolinks/eleves/*.desktop /home/$USER/Desktop/
	cp /mnt/netlogon/persolinks/eleves/* /home/$USER/link/
	fi

#############################################################################

# Reglage de l'affichage en liste dans les fenetres de pcmanfm
sed -i 's/icon/list/g' /home/$USER/.config/pcmanfm/LXDE/pcmanfm.conf
sed -i 's/hidden=1/hidden=0/g' /home/$USER/.config/pcmanfm/LXDE/pcmanfm.conf

# Applications par defaut
cat /mnt/netlogon/conex/mimeapps.list > /home/$USER/.config/mimeapps.list

# Autres scripts a lancer
bash /mnt/netlogon/conex/alsavolume.sh
#bash /mnt/netlogon/conex/profil_ff.sh
#bash /mnt/netlogon/conex/clean_vartmp.sh
#bash /mnt/netlogon/conex/warn_overfill.sh

# Mise à jour des menus ?
update-menus

exit 0

