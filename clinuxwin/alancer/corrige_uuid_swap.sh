#!/bin/bash
#
# 
#*********************************************************************
# corrige_uuid_swap.sh
# Ce script permet de corriger les problemes d'uuid de la partition swap apres clonage.
# 
#*********************************************************************
# Section a completer
DATESCRIPT="20171215"
VERSION="1.6"

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# On rend le script "cretin-resistant"
[ -e /home/netlogon/clients-linux ] && echo "Malheureux... Ce script est a exécuter sur les clients Linux, pas sur le serveur !" && exit 1

# Si la machine a été clonée, il faut corriger l'uuid de la partition swap
echo -e "$COLINFO"
echo "Correction de l'uuid de la partition swap après clonage..."
echo -e "$COLTXT"
# On recupère l'uuid correct
blkid | grep 'swap' | cut -d= -f2 | awk '{ print $1}' > /etc/swapuuid
sed -i 's/"//g' /etc/swapuuid
swuid=$(cat /etc/swapuuid)
echo "identifiant correct : $swuid"

# On recupere l'uuid contenu dans /etc/fstab
cat /etc/fstab | grep 'swap' | cut -d= -f2 | awk '{ print $1}' > /etc/wronguuid
sed -i 's/#//g' /etc/wronguuid
sed -i '/^$/d' /etc/wronguuid
wrong=$(cat /etc/wronguuid)
echo "identifiant à corriger : $wrong"
echo "contenu de resume : $(cat /etc/initramfs-tools/conf.d/resume)"

# On remplace par la bonne valeur
sed -i 's/'$wrong'/'$swuid'/g' /etc/fstab
echo "RESUME=UUID=$swuid" > /etc/initramfs-tools/conf.d/resume
#sed -i 's/'$wrong2'/'$swuid'/g' /etc/initramfs-tools/conf.d/resume
#swaplabel -U $swuid $perswap
echo -e "$COLINFO"
echo "On lance update-initramfs..."
echo -e "$COLTXT"
update-initramfs -u
echo -e "$COLDEFAUT"
echo "Correction uuid swap terminée !"
echo -e "$COLTXT"
