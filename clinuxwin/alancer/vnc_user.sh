#!/bin/bash
#
# 
#*********************************************************************
# vnc_user.sh
# Ce script configure vnc pour l'utilisateur courant
#*********************************************************************
# Section a completer
DATESCRIPT="20180707"
VERSION="0.1"

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

FTP_SERVER="https://raw.githubusercontent.com/jcmousse/clinux/master"

cd
rm -rf .vnc
mkdir .vnc
cd .vnc
wget --no-check-certificate --quiet $FTP_SERVER/commun/vnc/xstartup
chmod 755 xstartup
echo "vnc configuré pour cet utilisateur."
exit 0


