#!/bin/bash
# **********************************************************
# maj_client_stretch.sh
# Script de maj des clients Stretch
# 20171215
# **********************************************************

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# On rend le script "cretin-resistant"
[ -e /home/netlogon/clients-linux ] && echo "Malheureux... Ce script est a executer sur les clients Linux, pas sur le serveur !" && exit 1

# Test de la version Debian du client
TEST_VERSION=$(cat /etc/debian_version | grep "9.")
if [ ! -n "$TEST_VERSION" ]; then
echo -e "$COLDEFAUT"
echo "Ce script est destiné à un système Debian 9 (Stretch)."
echo "Il n'est pas adapté à ce système."
echo -e "$COLTXT"
exit 1
else
echo -e "$VERT"
echo "Debian 9 (Stretch) détectée... on poursuit."
echo -e "$COLTXT"
fi

# On s'assure que le cache apt est bien présent avant de lancer l'update-upgrade
echo -e "$COLINFO"
echo "Activation du proxy..."
echo -e "$COLTXT"
echo "#Configuration du proxy pour l'établissement
Acquire::http { Proxy \"http://##ipcache##:3142\"; };" > /etc/apt/apt.conf.d/99proxy
# Ecriture du témoin d'état du cache sur le client
echo "Avec cache" > /etc/monproxy

# On règle les problèmes de mise à jour automatiques inachevées...
echo -e "$COLINFO"
echo "Il y a peut-être des màj inachevées..."
echo -e "$COLTXT"
apt-get update
apt-get install -f -y
# Il y a parfois des problèmes avec les màj de vlc
echo -e "$COLINFO"
echo "Mise à jour de vlc..."
echo -e "$COLTXT"
apt-get install -y vlc
# Mise à jour du client
echo -e "$COLINFO"
echo "Mise à jour du client..."
echo -e "$COLTXT"
apt-get upgrade -y && apt-get dist-upgrade -y
echo -e "$COLINFO"
echo "Mise à jour effectuée."
echo ""
echo -e "$COLTXT"

echo -e "$COLINFO"
echo "On fait un peu de ménage..."
echo -e "$COLTXT"
apt-get autoremove -y
apt-get autoclean
echo -e "$VERT"
echo "Terminé !"
echo -e "$COLTXT"
