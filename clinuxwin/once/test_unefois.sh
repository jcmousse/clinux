#!/bin/bash
#
#***********************************************
# test_unefois.sh
# Peur servir de modèle pour les scripts unefois
# 20151212
#***********************************************

# Témoin de passage du script sur le client
temoin=/root/temoins/temoin.testunefois

# Exécution du script unefois
# Ne sera exécuté que si le témoin n'existe pas

if [ ! -e $temoin ] ; then
        echo "Sasséfait" > $temoin
        else
        exit 0
fi
