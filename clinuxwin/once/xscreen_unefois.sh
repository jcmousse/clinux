#!/bin/sh
#
# xscreen_unefois.sh
# installe xscreensaver debian sid sur le client
# 20170515
#
#####################################################
#DATE1=$(date +%F+%0kh%0M)

rm -f /root/temoins/temoin.xscreen_5.34
temoin=/root/temoins/temoin.xscreen_5.36

# Execution du script
if [ ! -e $temoin ] ; then
	TEST_VERSION=$(uname -a | grep "i686")
	if [ ! -n "$TEST_VERSION" ]; then
	dpkg -i /mnt/netlogon/alancer/xscreensaver_5.36-1_amd64.deb
	else
	dpkg -i /mnt/netlogon/alancer/xscreensaver_5.36-1_i386.deb
	fi
	echo "fait" > $temoin
else	
	echo "Xscreensaver est en version 5.36"
	exit 0
fi
