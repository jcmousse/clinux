#!/bin/bash

# ****************************************************************
# rejoint_clinux_win_auto.sh
# intégration "automatique" d'un client linux à partir d'un fichier de paramètres
# 20180604
# ****************************************************************

DATERAPPORT=$(date +%F+%0kh%0M)

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

DATESCRIPT="20171214"
VERSION="0.1.5"

ftp=https://gitlab.com/jcmousse/clinux/raw/master
ftp2=http://jcmousse.free.fr

#####################

# Paramètres complétés par le script de préparation
IPPART=##ippart##
IPDNS=##ipdns##
IPSERV=##ippart##
DOM1=##dom1##
NOMDNS=##nomdns##
NOMAD=##nomad##
IPCLINUX=##se3ip##

# Passage en majuscules du nom de domaine
DOM2=${DOM1^^}

# Nom de domaine samba pour smb.conf
DOM3=$(echo $DOM2 | cut -d'.' -f1)

# Test de la version Debian du client
TEST_VERSION=$(cat /etc/debian_version | grep "9.")
if [ ! -n "$TEST_VERSION" ]; then
echo "Ce script est destiné à un système Debian 9 (Stretch)."
echo "Il n'est pas adapté à ce système."
exit
else
echo "Debian 9 (Stretch) détectée... on poursuit."
fi

# Configuration du pavé numérique au démarrage si c'est un pc de bureau
echo -e "$COLDEFAUT" "Cet ordinateur est-il un portable ? [o/N] " ; tput sgr0
read repport
if [ ! "$repport" = "o" -a ! "$repport" = "oui" ]; then
	echo -e "$COLINFO" "Cet ordinateur n'est pas un portable."
	echo -e "$COLINFO" "On activera le verrouillage numérique au démarrage."
	echo -e "$COLTXT"
	NUMLOCK=o
else
	echo -e "$COLINFO" "Cet ordinateur est un portable."
	echo -e "$COLINFO" "Pas de verrouillage numérique au démarrage."
	echo -e "$COLTXT"
	NUMLOCK=n
fi

# Configuration de l'integration de la machine : hostname et parc
		echo "Configuration des paramètres de l'intégration..."
		echo ""
		echo -e "$COLINFO" "Nom actuel de la machine : $(hostname)" ; tput sgr0
		ANCIENNOM=$(hostname)
		echo ""
		echo -e "$COLDEFAUT" "Entrez un nom pour cette machine, " ; tput sgr0
		echo -e "$COLDEFAUT" "Ou validez pour conserver son nom actuel :" ; tput sgr0
		read NOMMACH
			if [ -z "$NOMMACH" ]; then
			NOMMACH=$ANCIENNOM
			fi
		echo ""
		echo -e "$COLINFO" "Le nom choisi pour la machine est $NOMMACH." ; tput sgr0
		echo ""
		echo -e "$COLDEFAUT" "Dans quel parc sera intégrée cette machine ?" ; tput sgr0
		echo -e "$COLINFO" "Attention : seuls choix possibles : cdi ou sprofs" ; tput sgr0
		echo ""
		read PARK
		echo ""
		echo -e "$COLINFO" "Le client sera affecté dans le parc $PARK." ; tput sgr0
		echo ""

# Inscription du nom du parc sur le client
echo $PARK > /etc/parc.txt
chmod 744 /etc/parc.txt

# Mise a jour de la machine
export DEBIAN_FRONTEND=noninteractive
export DEBIAN_PRIORITY=high

echo -e "$VERT" "Mise à jour du système..."
echo -e "$COLTXT"

# Resolution du probleme de lock
if [ -e "/var/lib/dpkg/lock" ]; then
	rm -f /var/lib/dpkg/lock
fi
# On lance une maj
apt-get update
apt-get -y upgrade

# Installation des paquets necessaires
echo "Installation des paquets nécessaires:"
echo "Ne rien remplir, les fichiers seront configurés/modifiés automatiquement ensuite..."
echo ""
apt-get install -y krb5-user libpam-krb5 samba smbclient cifs-utils winbind libpam-winbind libpam-mount libnss-winbind ntp net-tools numlockx

# Configuration des fichiers
echo "Configuration des fichiers pour le serveur..."
echo ""

# Configuration du fichier /etc/hosts
echo "Configuration du fichier /etc/hosts"
echo ""
cp /etc/hosts /etc/hosts_sauve_$DATERAPPORT
sed -i 's/^127.0.1.1.*/127.0.1.1\	'$NOMMACH'.'$DOM1'\	'$NOMMACH'/g' /etc/hosts

# Configuration du fichier /etc/hostname
echo "Configuration du fichier /etc/hostname"
echo ""
cp /etc/hostname /etc/hostname_sauve_$DATERAPPORT
echo "$NOMMACH" > /etc/hostname

# Configuration du fichier /etc/krb5.conf
echo "Configuration du fichier /etc/krb5.conf"
cp /etc/krb5.conf /etc/krb5.conf_sauve_$DATERAPPORT

echo "[libdefaults]
	default_realm = $DOM2
	dns_lookup_realm = false
	dns_lookup_kdc = false

[realms]
	$DOM2 = {
		kdc = $IPDNS
		admin_server = $IPDNS
		default_domain = $DOM1
	}

[domain_realm]
	.acad-clermont.local = $DOM2
	acad-clermont.local = $DOM2

[login]
	krb4_convert = true
	krb4_get_tickets = false" > /etc/krb5.conf

# Configuration du fichier /etc/ntp.conf
echo "Configuration du fichier /etc/ntp.conf"
cp /etc/ntp.conf /etc/ntp.conf_sauve_$DATERAPPORT
echo -e "# /etc/ntp.conf, configuration for ntpd; see ntp.conf(5) for help

driftfile /var/lib/ntp/ntp.drift

statistics loopstats peerstats clockstats
filegen loopstats file loopstats type day enable
filegen peerstats file peerstats type day enable
filegen clockstats file clockstats type day enable

# You do need to talk to an NTP server or two (or three).
#server ntp.your-provider.example
server $NOMDNS.$DOM1

# pool.ntp.org maps to about 1000 low-stratum NTP servers.  Your server will
# pick a different set every time it starts up.  Please consider joining the
# pool: <http://www.pool.ntp.org/join.html>
#pool 0.debian.pool.ntp.org iburst
#pool 1.debian.pool.ntp.org iburst
#pool 2.debian.pool.ntp.org iburst
#pool 3.debian.pool.ntp.org iburst

# Access control configuration; see /usr/share/doc/ntp-doc/html/accopt.html

# By default, exchange time with everybody, but don't allow configuration.
restrict -4 default kod notrap nomodify nopeer noquery limited
restrict -6 default kod notrap nomodify nopeer noquery limited

# Local users may interrogate the ntp server more closely.
restrict 127.0.0.1
restrict ::1

# Needed for adding pool entries
restrict source notrap nomodify noquery" > /etc/ntp.conf

# Configuration du fichier /etc/nsswitch.conf
echo "Configuration du fichier /etc/nsswitch.conf"
cp /etc/nsswitch.conf /etc/nsswitch.conf_sauve_$DATERAPPORT
echo -e "# /etc/nsswitch.conf
#
passwd:         compat winbind
group:          compat winbind
shadow:         compat
gshadow:        files

hosts:          files mdns4_minimal [NOTFOUND=return] dns
networks:       files

protocols:      db files
services:       db files
ethers:         db files
rpc:            db files

netgroup:       nis" > /etc/nsswitch.conf

# Configuration du fichier /etc/samba/smb.conf
echo "Configuration du fichier /etc/samba/smb.conf"
cp /etc/samba/smb.conf /etc/samba/smb.conf_sauve_$DATERAPPORT
echo -e "
[global]

   workgroup = $DOM3

# prevent nmbd to search for NetBIOS names through DNS.
   dns proxy = no

####### Authentication #######

   server role = standalone server
   security = ads
   realm = $DOM2
   passdb backend = tdbsam
   obey pam restrictions = yes
   unix password sync = yes
   passwd program = /usr/bin/passwd %u
   passwd chat = *Enter\snew\s*\spassword:* %n\n *Retype\snew\s*\spassword:* %n\n *password\supdated\ssuccessfully* .
   pam password change = yes
   map to guest = bad user

############ Misc ############

	idmap uid = 10000-20000
	idmap gid = 10000-20000
	winbind enum users = yes
	winbind enum groups = yes
	template homedir = /home/%U
	template shell = /bin/bash
	winbind separator = +
	winbind cache time = 10
#	idmap config $DOM1 = ad
	client use spnego = yes
	client ntlmv2 auth = yes
	encrypt passwords = yes
	winbind use default domain = yes
	domain master = no
	local master = no
	preferred master = no" > /etc/samba/smb.conf

# Demarrage du service samba
rm /lib/systemd/system/samba.service 
systemctl daemon-reload
service samba start

# Création des points de montage
mkdir -p /media/classes
mkdir -p /media/partages

# Configuration du fichier /etc/security/pam_mount.conf.xml
echo "Configuration du fichier /etc/security/pam_mount.conf.xml"
cp /etc/security/pam_mount.conf.xml /etc/security/pam_mount.conf.xml_sauve_$DATERAPPORT
echo "Configuration du fichier /etc/security/pam_mount.conf.xml"
echo ""

echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>

<!--
/etc/security/pam_mount.conf.xml
Configuration pour le serveur
-->

<pam_mount>
  <debug enable=\"1\" />
  <logout wait=\"0\" hup=\"0\" term=\"0\" kill=\"0\" />
  <mkmountpoint enable=\"1\" remove=\"true\" />

  <umount>umount %(MNTPT)</umount>

  <volume
    pgrp=\"utilisateurs du domaine\"
    sgrp=\"PROFESSEURS\"
    fstype=\"cifs\"
    server=\"$IPPART\"
    path=\"Professeurs\"
    mountpoint=\"/media/Professeurs\"
    options=\"username=%(USER),uid=%(USERUID),gid=%(USERGID),file_mode=0700,dir_mode=0700,vers=2.1\"
  />

  <volume
    pgrp=\"utilisateurs du domaine\"
    sgrp=\"ELEVES\"
    fstype=\"cifs\"
    server=\"$IPPART\"
    path=\"Eleves\"
    mountpoint=\"/media/Eleves\"
    options=\"username=%(USER),uid=%(USERUID),gid=%(USERGID),file_mode=0700,dir_mode=0700,vers=2.1\"
  />

  <volume
    pgrp=\"utilisateurs du domaine\"
    fstype=\"cifs\"
    server=\"$IPPART\"
    path=\"Echange\"
    mountpoint=\"/media/Echange\"
    options=\"username=%(USER),uid=%(USERUID),gid=%(USERGID),file_mode=0700,dir_mode=0700,vers=2.1\"
   />

<msg-authpw>Mot de passe :</msg-authpw>
<msg-sessionpw>Mot de passe :</msg-sessionpw>

</pam_mount>" > /etc/security/pam_mount.conf.xml

# Configuration du fichier /etc/pam.d/common-auth
echo "Configuration du fichier /etc/pam.d/common-auth"
echo ""
cp /etc/pam.d/common-auth /etc/pam.d/common-auth_sauve_$DATERAPPORT
echo "
# /etc/pam.d/common-auth
auth optional pam_mount.so
auth sufficient pam_winbind.so use_first_pass
auth sufficient pam_unix.so nullok_secure use_first_pass
auth requisite   pam_deny.so
auth required pam_permit.so" > /etc/pam.d/common-auth

# Configuration du fichier /etc/pam.d/common-session
echo "Configuration du fichier /etc/pam.d/common-session"
echo ""
cp /etc/pam.d/common-session /etc/pam.d/common-session_sauve_$DATERAPPORT
echo "
# /etc/pam.d/common-session
session optional	pam_umask.so
session optional	pam_mount.so
session required	pam_unix.so use_first_pass
session required	pam_mkhomedir.so umask=0022 skel=/etc/skel
session optional	pam_exec.so type=open_session seteuid /bin/bash /usr/bin/logon1.sh
session optional	pam_exec.so type=close_session seteuid /bin/bash /usr/bin/logout1.sh
session	optional	pam_systemd.so" > /etc/pam.d/common-session

# Configuration du fichier /etc/pam.d/common-account
echo "Configuration du fichier /etc/pam.d/common-account"
echo ""
cp /etc/pam.d/common-account /etc/pam.d/common-account_sauve_$DATERAPPORT
echo "
# /etc/pam.d/common-account

account sufficient      pam_winbind.so
account required        pam_unix.so" > /etc/pam.d/common-account

# Configuration du fichier /etc/pam.d/common-password
echo "Configuration du fichier /etc/pam.d/common-password"
echo ""
cp /etc/pam.d/common-password /etc/pam.d/common-password_sauve_$DATERAPPORT
echo "# /etc/pam.d/common-password
password	[success=3 default=ignore]	pam_krb5.so minimum_uid=1000
password	[success=2 default=ignore]	pam_unix.so obscure use_authtok try_first_pass sha512
password	[success=1 default=ignore]	pam_winbind.so use_authtok try_first_pass
password	requisite			pam_deny.so
password	required			pam_permit.so
#password	optional	pam_gnome_keyring.so" > /etc/pam.d/common-password

# Configuration du fichier /etc/security/group.conf
echo "Configuration du fichier /etc/security/group.conf"
echo ""
cp /etc/security/group.conf /etc/security/group.conf_sauve_$DATERAPPORT
echo "
# /etc/security/group.conf
lightdm;*;*;Al0000-2400;floppy,cdrom,audio,video,plugdev
gdm3;*;*;Al0000-2400;floppy,cdrom,audio,video,plugdev
xdm;*;*;Al0000-2400;floppy,cdrom,audio,video,plugdev" > /etc/security/group.conf

# Modifications pour le montage sur les clients d'un partage du serveur
if [ ! -e /mnt/netlogon ] ; then
mkdir -p /mnt/netlogon
#chown -R admin:admins /mnt/netlogon
chmod -R 755 /mnt/netlogon
fi

# Création d'une entrée dans /etc/fstab pour monter un répertoire partagé du serveur
sed -i '/netlogon/d' /etc/fstab
sed -i '/^$/d' /etc/fstab
echo "//$IPCLINUX/netlogon /mnt/netlogon  cifs  guest" >> /etc/fstab

# On teste si lxde est installé
lxde=/etc/xdg/lxsession
if [ -e $lxde ]; then
# Lancement des scripts "unefois" au chargement de lightdm
echo "#!/bin/bash
mount -a
sleep 2
sh /mnt/netlogon/conex/init_unefois.sh
exit 0" > /usr/bin/init1.sh
fi

# Activation du pavé numérique au démarrage si demandé
	if [ $NUMLOCK = "o" ] ; then
	sed -i '/exit 0/d' /usr/bin/init1.sh
	sed -i '/^$/d' /usr/bin/init1.sh
	echo "/usr/bin/X11/numlockx on
exit 0" >> /usr/bin/init1.sh
	fi
chmod +x /usr/bin/init1.sh

# Lancement du script init1
sed -i 's/#greeter-setup-script=/greeter-setup-script=\/usr\/bin\/init1.sh/g' /etc/lightdm/lightdm.conf


# Configuration de l'ouverture de session
# Créations de liens sur le bureau si on utilise lxde
echo "#!/bin/sh
echo \"$PAM_USER\" > /tmp/pamuser.log
sh /mnt/netlogon/conex/createlinks_20180604.sh
exit 0" > /usr/bin/logon1.sh
chmod +x /usr/bin/logon1.sh

# Configuration de la fermeture de session
# kill des processus de l'utilisateur courant et effacement du lock firefox si présent
# Effacement des raccourcis (liens symboliques) créés a l'ouverture de session
echo "#!/bin/sh
sh /mnt/netlogon/conex/deletelinks_20180604.sh
#userscript=$(who | grep -v root | cut -d\" \" -f1)
#sudo -u $userscript -c sh /mnt/netlogon/conex/deletelinks_20180604.sh
#su - $userscript -c \"sh /mnt/netlogon/conex/deletelinks_20180604.sh\"
exit 0" > /usr/bin/logout1.sh
chmod +x /usr/bin/logout1.sh

# Droits sudoers pour les groupes eleves et professeurs pour l'execution de la connexion et de la deconnexion
#echo -e "%eleves	ALL = NOPASSWD: /mnt/netlogon/conex/deletelinks_20180604.sh,/mnt/netlogon/conex/createlinks_20180604.sh
#%professeurs	ALL = NOPASSWD: /mnt/netlogon/conex/deletelinks_20180604.sh,/mnt/netlogon/conex/createlinks_20180604.sh" > /etc/sudoers.d/20-user

# Alias pour l'extinction par root
testpoweroff=$(cat /root/.bashrc | grep poweroff)
if [ ! -n "$testpoweroff" ] ; then
echo "alias halt='poweroff'" >> /root/.bashrc
fi

# Activation des maj automatiques
apt-get install --assume-yes unattended-upgrades
cp /usr/share/unattended-upgrades/20auto-upgrades /etc/apt/apt.conf.d/20auto-upgrades
cd /etc/apt/apt.conf.d
rm -f 50unattended-upgrades
wget --no-check-certificate $ftp/clinuxwin/stretch/50unattended-upgrades

# Suppression de clipit et de screenlock
cd /usr/share/applications
mv lxde-screenlock.desktop lxde-screenlock.desktop.bak
cd /etc/xdg/autostart
mv clipit-startup.desktop clipit-startup.desktop.bak
mv /etc/xdg/autostart/light-locker.desktop /etc/xdg/autostart/light-locker.desktop.bak

# Suppression de wicd
echo "Suppression de wicd..."
echo ""
systemctl stop wicd
systemctl disable wicd
systemctl mask wicd
apt-get remove --purge -y wicd
cd /etc/xdg/autostart
mv wicd-tray.desktop wicd-tray.desktop.bak
apt-get -y autoremove
apt-get clean
apt-get autoclean


# Installation et configuration de network-manager
apt-get install -y network-manager
sed -i 's/false/true/g' /etc/NetworkManager/NetworkManager.conf

if [ ! -e /etc/xdg/autostart ] ; then
mkdir -p /etc/xdg/autostart
chmod -R 755 /etc/xdg/autostart
fi

####################################################################################
# Jonction du poste au domaine
echo -e "$COLINFO" "Lancement de la jonction du domaine.... "

# Demande d'un ticket kerberos
kdestroy
echo -e "$COLINFO" "Demande d'un ticket Kerberos..."
echo -e "$COLTITRE" "Entrez un identifiant d'utilisateur du domaine :"
echo -e "$COLTXT"
read kinituser
#echo -e "$COLTXT" "Entrez le mot de passe pour $kinituser :"
kinit $kinituser
sleep 5
#echo ""

# Verification ticket
klist
echo ""
echo -e "$COLDEFAUT" "Le ticket Kerberos est-il correct ?"
echo -e "$COLINFO" "Si tout va bien, validez par "Entrée"."
read lulu

echo -e "$COLTITRE" "Jonction du domaine : entrez un administrateur qui a les droits nécessaires :"
echo -e "$COLINFO" "(Il faudra fournir son mot de passe)"
echo -e "$COLTXT"
read joinuser
sleep 3

net ads join -S $NOMAD -U $joinuser
sleep 5

echo -e "$VERT" "Intégration terminée !"
echo -e "$COLTXT"


echo -e "$COLINFO" "Intégration de la machine terminée. "
echo -e "$COLDEFAUT" "La machine redémarrera dans 5s "
sleep 5
reboot

echo -e "$COLTXT"
