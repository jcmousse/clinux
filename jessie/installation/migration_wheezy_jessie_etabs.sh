#!/bin/bash
# **********************************************************
# Migration automatisée d'un client wheezy en version jessie
# jc mousseron 11/2016
# Ce script est distribué selon les termes de la licence GPL
# version du 20161130
# **********************************************************
DATEINST=$(date +%F+%0kh%0M)
FTP=https://raw.githubusercontent.com/jcmousse/clinux/master

ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# Test pour les distraits
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a exécuter sur les clients Linux, pas sur le serveur !" && exit 1

# Test de la version Debian du client
TEST_VERSION=$(cat /etc/debian_version | grep "7.")
if [ ! -n "$TEST_VERSION" ]; then
echo "Ce script est destiné à migrer un client Debian 7 (Wheezy) en version 8 (Jessie)."
echo "Il n'est pas adapté à ce système."
exit
else
echo "Debian 7 (Wheezy) détectée... on lance la migration en version 8 (jessie)"
fi

echo -e "$ROUGE" "Utilisez-vous un cache APT pour la migration ?" ; tput sgr0
		PS3='Repondre par o ou n: '   # le prompt
		LISTE=("[o] oui" "[n]  non")  # liste de choix disponibles
		select CHOIX in "${LISTE[@]}" ; do
			case $REPLY in
				1|o)
				echo "Cache apt présent pour la migration (cool!)"
				APTCACHE=o
				break
				;;
				2|n)
				echo "Pas de cache apt pour la migration (dommage ;)"
				APTCACHE=n
				break
				;;
			esac
		done

if [ $APTCACHE = "o" ] ; then
	# Définition de l'adresse du proxy apt pour l'installation
	defaultcache="192.168.143.243:3142"
	echo -e "$COLINFO" "Vous utilisez un cache APT pour la migration. " ; tput sgr0
		echo -e "$COLINFO" "Donnez l'adresse IP et le port du cache APT sous la forme 192.168.143.243:3142"
		echo -e "$COLDEFAUT" "Cache par défaut : [$defaultcache]"
		echo "Validez par entrée si cette adresse convient, ou entrez une autre adresse (sans oublier le port) :"
		read APTPROXY
		if [ -z "$APTPROXY" ]; then
		APTPROXY=$defaultcache
		fi
		echo -e "$DEFAUT" "L'adresse IP du cache apt est $APTPROXY"
		echo -e "$COLTXT"
		
	echo ""
	echo "Ajout temporaire du proxy apt à la configuration..."
	# Déclaration du proxy pour apt
	touch /etc/apt/apt.conf.d/99proxy
	echo "#Configuration du proxy apt pour l'installation
Acquire::http { Proxy \"http://$APTPROXY\"; };" > /etc/apt/apt.conf.d/99proxy
fi

# Désactivation de l'entrée cle usb dans /etc/fstab si le systeme a ete installé avec une clé
sed -i 's/\/dev\/sdb/#\/dev\sdb/g' /etc/fstab

echo -e "$ROUGE" "C'est parti!" ; tput sgr0
echo -e "$JAUNE" "En fonction de la connexion internet, la migration prendra de 30 à 90 minutes. " ; tput sgr0
echo -e "$JAUNE" "Soyez patient(e)..." ; tput sgr0
echo ""
# echo -e "$VERT" "Installation de lxde..." ; tput sgr0
#apt-get update
#apt-get install --assume-yes lxde

echo -e "$VERT" "Modifications des sources..." ; tput sgr0
cp /etc/apt/sources.list /etc/apt/sources.save

echo "deb http://ftp.ch.debian.org/debian/ jessie main contrib non-free
deb-src http://ftp.ch.debian.org/debian/ jessie main contrib non-free

deb http://security.debian.org/ jessie/updates main contrib non-free
deb-src http://security.debian.org/ jessie/updates main contrib non-free

# Jessie-updates, previously known as 'volatile'
deb http://ftp.ch.debian.org/debian/ jessie-updates main contrib non-free
deb-src http://ftp.ch.debian.org/debian/ jessie-updates main contrib non-free

# Jessie-backports
deb http://ftp.debian.org/debian/ jessie-backports main contrib non-free" > /etc/apt/sources.list

echo "# Deb-multimedia
deb ftp://ftp.deb-multimedia.org jessie main non-free
#deb ftp://ftp.deb-multimedia.org jessie-backports main" > /etc/apt/sources.list.d/deb-multimedia.list

echo "# Java installer
deb http://ppa.launchpad.net/webupd8team/java/ubuntu precise main
deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu precise main" > /etc/apt/sources.list.d/webupd8team-java.list

# Ajout du dépot pour geogebra 5
echo "# Geogebra
deb http://www.geogebra.net/linux/ stable main" > /etc/apt/sources.list.d/geogebra.list


echo -e "$VERT" "Sources modifiées, mise à jour des paquets essentiels..." ; tput sgr0
apt-get update
echo -e "$VERT" "Installation des clés des nouveau dépots" ; tput sgr0
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886
# Ajout de la cle pour le depot geogebra
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys C072A32983A736CF
apt-get install --allow-unauthenticated --assume-yes deb-multimedia-keyring

apt-get update

echo -e "$JAUNE" "C'est parti, soyez patient(e)... " ; tput sgr0
echo -e "$JAUNE" "Mise à jour des paquets essentiels... " ; tput sgr0
apt-get upgrade
echo ""
echo -e "$JAUNE" "Les paquets essentiels ont été mis à jour," ; tput sgr0
echo -e "$JAUNE" "On termine la migration de version :" ; tput sgr0
apt-get dist-upgrade
echo ""
#Mise à jour de lxde
echo -e "$VERT"  "Mise à jour de lxde" ; tput sgr0
echo -e "$VERT" "Modification de lxsession-logout" ; tput sgr0
echo "Récupération des sources..."
cd
rm -rf /root/lxsession*
wget --no-check-certificate $FTP/jessie/lxsession-0.5.1.tar.xz
echo -e "$VERT" "Décompression de l'archive" ; tput sgr0
tar -Jxvf lxsession-0.5.1.tar.xz

echo -e "$VERT" "Modification de lxsession-logout.c" ; tput sgr0
cd /root/lxsession-0.5.1/lxsession-logout/
rm -f lxsession-logout.c
wget --no-check-certificate $FTP/jessie/lxsession-logout.c

echo -e "$VERT" "Installation des outils de compilation" ; tput sgr0
apt-get install --assume-yes gcc intltool pkg-config xorg-dev glib-2.0 libdbus-glib-1-dev libspice-client-gtk-2.0-dev libpolkit-agent-1-dev valac
echo -e "$VERT" "Installation et compilation des modifications" ; tput sgr0
cd /root/lxsession-0.5.1
./configure
make && make install
rm -rf /root/lxsession*

export DEBIAN_FRONTEND=noninteractive

echo -e "$ROUGE" "On commence une longue série d'installations...." ; tput sgr0
echo ""
apt-get install debconf

echo -e "$VERT" "installation de java 8 derniere version" ; tput sgr0
echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 boolean true" | debconf-set-selections

#if [ $APTCACHE = "o" ] ; then
#echo -e "$JAUNE" "Neutralisation du proxy APT de l'installation" ; tput sgr0
#sed -i 's/Acquire/#Acquire/g' /etc/apt/apt.conf.d/99proxy
#fi
#apt-get install --assume-yes oracle-java8-installer
#if [ $APTCACHE = "o" ] ; then
#echo -e "$JAUNE" "Rétablissement du proxy APT de l'installation" ; tput sgr0
#sed -i 's/#Acquire/Acquire/g' /etc/apt/apt.conf.d/99proxy
#fi

echo -e "$VERT" "Installation de libreoffice V5" ; tput sgr0
apt-get install --assume-yes -t jessie-backports libreoffice libreoffice-l10n-fr
echo -e "$VERT" "Installation d'iceweasel" ; tput sgr0
apt-get install --assume-yes firefox-esr firefox-esr-l10n-fr
echo -e "$VERT" "Installation d'un tas de trucs divers et variés..." ; tput sgr0
echo -e "$JAUNE" "Il y en a pour un moment, vous pouvez aller boire un coup!" ; tput sgr0
apt-get install --assume-yes flashplugin-nonfree vlc gecko-mediaplayer gimp scribus audacity lame
apt-get install --assume-yes gstreamer0.10-fluendo-mp3 gstreamer0.10-ffmpeg ffmpeg sox twolame vorbis-tools faad totem
TEST_VERSION=$(uname -a | grep "i686")
if [ ! -n "$TEST_VERSION" ]; then
echo -e "$VERT" "Version 64 bits détectée" ; tput sgr0
apt-get install --assume-yes w64codecs
else
echo -e "$VERT" "Version 32 bits détectée" ; tput sgr0
apt-get install --assume-yes w32codecs
fi
apt-get install --assume-yes alsa-base alsa-utils alsamixergui alsa-oss ntpdate ethtool nictools-pci screen apt-listchanges
apt-get install --assume-yes ttf-mscorefonts-installer libdvdcss2 evince xdg-user-dirs anacron unrar vnc4server
apt-get install --assume-yes chromium chromium-l10n pepperflashplugin-nonfree
apt-get install --assume-yes kdenlive kde-l10n-fr okular ksnapshot
apt-get install --assume-yes geogebra5
apt-get install --assume-yes firmware-linux-nonfree
echo -e "$VERT" "Ouf, les installations sont terminées !" ; tput sgr0
echo ""
chmod 755 /etc/X11/xinit/xinitrc
echo -e "$JAUNE" "On fait un peu de ménage..." ; tput sgr0
apt-get --assume-yes autoremove
apt-get clean
apt-get autoclean

#echo -e "$VERT" "Desactivation des autres os dans Grub" ; tput sgr0
#chmod -x /etc/grub.d/30_os-prober
#update-grub

# Installation et configuration des mises à jour automatiques (si demandées)
#if [ $MAJAUTO = "o" ] ; then
echo ">>>> Installation des maj automatiques <<<<<"
apt-get install --assume-yes unattended-upgrades
rm -f /etc/apt/apt.conf.d/20auto-upgrades
cp /usr/share/unattended-upgrades/20auto-upgrades /etc/apt/apt.conf.d/20auto-upgrades
cd /etc/apt/apt.conf.d
rm -f 50unattended-upgrades
wget --no-check-certificate $FTP/jessie/50unattended-upgrades
echo -e "$VERT" "Mises à jour automatiques installées et configurées" ; tput sgr0
#fi

echo -e "$VERT" "Modifications pour le démarrage a distance...." ; tput sgr0
echo -e "$VERT" "Désactivation de NETDOWN dans /etc/init.d/halt" ; tput sgr0
sed -i 's/NETDOWN=yes/NETDOWN=no/g' /etc/init.d/halt
echo -e "$VERT" "Activation de la fonction WOL sur la carte réseau" ; tput sgr0
ethtool -s eth0 wol g
sed -i '/exit 0/d' /etc/rc.local
echo "ethtool -s eth0 wol g"  >> /etc/rc.local
echo "exit 0" >> /etc/rc.local

# On passe à lightdm
dpkg-reconfigure lightdm

# Datage du script (temoin de passage dans /root)
cd /root
rm -f installation_*
echo "Installation le $DATEINST" >> /root/migration_$DATEINST


# Messages de fin d'installation
echo ""
echo -e "$VERT" "La migration est terminée." ; tput sgr0
echo ""
echo ""
echo "Vous pouvez maintenant rebooter la machine !"
