#!/bin/sh
#===============================
# extinction.sh
# Script d'extinction du client
# 20181014
#===============================

# On rend le script "cretin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a executer sur les clients Linux, pas sur le serveur !" && exit 1

echo "Extinction "forcée" du client..."
echo ""
sleep 1
# Détermination de l'utilisateur qui est resté connecté
lulu=$(who | grep -v root | cut -d" " -f1)

# On met lulu dehors
echo "Deconnexion de l'utilisateur $lulu"
echo ""
sleep 1
killall -u $lulu
echo "Extinction "forcée" du client..."

# Bonne nuit les petits
poweroff

exit 0
