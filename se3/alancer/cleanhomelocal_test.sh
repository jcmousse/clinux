#!/bin/bash
#
#***********************************************
# cleanhomelocal.sh
# Fait le menage dans le répertoire /home *local* du client
# après avoir vérifié que personne n'est connecté!
# 20190507
#***********************************************

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# On rend le script "cretin-resistant"
[ -e /home/netlogon/clients-linux ] && echo "Malheureux... Ce script est a exécuter sur les clients Linux, pas sur le serveur !" && exit 1

#lulu=$(who | grep -v root | cut -d" " -f1)
lulu=$(mount | grep /home | grep 172.17)

if [ -z "$lulu" ]; then
	echo ""
	echo -e "$VERT" "Pas d'utilisateur connecté, on peut faire le ménage!" ; tput sgr0
	echo ""
	echo -e "$COLDEFAUT" "Pour vérifier, voici le résultat de mount et de w :" ; tput sgr0
	echo ""
	echo "=============================================="
	mount | grep /home
	echo "=============================================="
	who
	echo "=============================================="
	echo ""
	echo -e "$COLDEFAUT" "Peut-on poursuivre et SIMULER le nettoyage ?" ; tput sgr0
	echo -e "$COLINFO" "Validez par entrée, ou faites ctrl-c pour sortir" ; tput sgr0
	read  lapin
	echo -e "$VERT" "Lancement du script de nettoyage, en SIMULATION :" ; tput sgr0
	echo ""
	ls /home | while read A ; do
	echo "suppression de /home/$A"
	#echo "rm -rf /home/$A"
	done
	echo ""
	echo "======================================================="
	echo ""
	echo -e "$COLDEFAUT" "Peut-on poursuivre et LANCER le nettoyage ?" ; tput sgr0
	echo -e "$COLINFO" "Validez par entrée, ou faites ctrl-c pour sortir" ; tput sgr0
	read  pingouin
	echo ""
	rm -rf /home/profil
	echo "/home/profil supprimé"
	ls /home | while read A ; do
	echo "suppression de /home/$A"
	rm -rf /home/$A
	done
	echo ""
	echo -e "$VERT" "Le répertoire /home *local* a été nettoyé." ; tput sgr0
	echo ""
	else
	echo ""
	echo -e "$ROUGE" "Un utilisateur est connecté, on ne touche à rien !" ; tput sgr0
	echo -e "$COLDEFAUT" "Voici le résultat de mount et de who :" ; tput sgr0
	echo ""
	echo "=============================================="
	mount | grep /home | grep 172.17
	echo "=============================================="
	who
	echo "=============================================="
	echo ""
	echo -e "$ROUGE" "On sort sans rient toucher !" ; tput sgr0
	echo ""
	exit 1
fi
exit 0
