#!/bin/sh

# **********************************************************
# active_wol.sh
# Activation du wol sur l'interface eth0
# 20190214
# **********************************************************

DATE1=$(date +%F+%0kh%0M)
FTP=https://raw.githubusercontent.com/jcmousse/clinux/master

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# On rend le script "cretin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a executer sur les clients Linux, pas sur le serveur !" && exit 1

# Ajout et activation de /etc/rc.local
echo ""
echo -e "$JAUNE"
echo "Activation du WOL sur eth0..."
echo ""
echo -e "$COLINFO"
echo "Création de rc-local.service"
echo -e "$COLTXT"

echo "[Unit]
Description=/etc/rc.local
ConditionPathExists=/etc/rc.local

[Service]
Type=forking
ExecStart=/etc/rc.local start
TimeoutSec=0
StandardOutput=tty
RemainAfterExit=yes
SysVStartPriority=99

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/rc-local.service

echo ""
echo -e "$COLINFO"
echo "Création du fichier /etc/rc.local"
echo -e "$COLTXT"
echo "#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.

exit 0" > /etc/rc.local

echo ""
echo -e "$COLINFO"
echo "Droits du fichier /etc/rc.local"
echo -e "$COLTXT"
chmod +x /etc/rc.local

echo ""
echo -e "$COLINFO"
echo "Activation du service"
echo -e "$COLTXT"
systemctl enable rc-local

#echo -e "$VERT" "Modifications pour le démarrage a distance...." ; tput sgr0
#echo -e "$VERT" "Désactivation de NETDOWN dans /etc/init.d/halt" ; tput sgr0
#sed -i 's/NETDOWN=yes/NETDOWN=no/g' /etc/init.d/halt

echo ""
echo -e "$VERT"
echo "Activation de la fonction WOL sur la carte réseau"
echo -e "$COLTXT"
ethtool -s eth0 wol g

echo ""
echo -e "$COLINFO"
echo "Activation de la fonction WOL au boot"
echo -e "$COLTXT"
sed -i '/exit 0/d' /etc/rc.local
echo "ethtool -s eth0 wol g"  >> /etc/rc.local
echo "exit 0" >> /etc/rc.local

echo ""
echo -e "$VERT"
echo "Le WOL a été activé sur la carte eth0 !"
echo -e "$COLTXT"
exit 0
