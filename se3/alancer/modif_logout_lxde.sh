#!/bin/bash

# ****************************************************************************
# modif_logout_lxde.sh
#
# Script de modification du dialogue de sortie sur les clients linux lxde
# 20170925
# ****************************************************************************

DATESCRIPT="20170925"
DATE1=$(date +%F+%0kh%0M)
FTP=https://gitlab.com/jcmousse/clinux/raw/master
#FTP=https://raw.githubusercontent.com/jcmousse/clinux/master
FTP2=http://jcmousse.free.fr

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# On rend le script "crétin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a exécuter sur les clients Linux, pas sur le serveur !" && exit 1

echo -e "$VERT" "Modification de lxsession-logout" ; tput sgr0
echo "Récupération des sources..."
cd
wget --no-check-certificate $FTP2/stretch/lxsession-0.5.3.tar.xz
echo -e "$VERT" "Décompression de l'archive" ; tput sgr0
tar -Jxvf lxsession-0.5.3.tar.xz

echo -e "$VERT" "Modification de lxsession-logout.c" ; tput sgr0
cd /root/lxsession-0.5.3/lxsession-logout/
rm -f lxsession-logout.c
wget --no-check-certificate $FTP2/stretch/lxsession-logout.c

echo -e "$VERT" "Installation des outils de compilation" ; tput sgr0
apt-get install --assume-yes gcc intltool pkg-config xorg-dev glib-2.0 libdbus-glib-1-dev libspice-client-gtk-3.0-dev libpolkit-agent-1-dev valac libunique-dev
echo -e "$VERT" "Installation et compilation des modifications" ; tput sgr0
cd /root/lxsession-0.5.3
./configure
make && make install
rm -rf /root/lxsession*

# Messages de fin d'installation
echo ""
echo -e "$VERT" "La configuration de la fermeture de session est terminée." ; tput sgr0
echo ""
echo "Vous pouvez maintenant rebooter cette machine!"
