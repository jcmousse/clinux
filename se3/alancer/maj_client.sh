#!/bin/bash
# **********************************************************
# maj_client_v3.sh
# Script de maj des clients linux hors domaine 8, 9 ou 10
# 20190220
# **********************************************************

DATE1=$(date +%F+%0kh%0M)
FTP=https://gitlab.com/jcmousse/clinux/raw/master
#FTP=https://raw.githubusercontent.com/jcmousse/clinux/master

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# On rend le script "cretin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a executer sur les clients Linux, pas sur le serveur !" && exit 1

# Neutralisation préalable du cache
> /etc/apt/apt.conf.d/99proxy
echo "Sans cache" > /etc/monproxy

TEST_1=$(cat /etc/debian_version | grep "10.")
[ -n "$TEST_1" ] && distro=buster

TEST_2=$(cat /etc/debian_version | grep "8.")
[ -n "$TEST_2" ] && distro=jessie

TEST_3=$(cat /etc/debian_version | grep "9.")
[ -n "$TEST_3" ] && distro=stretch


# Affichage de la version détectée pour validation

	if [ "$distro" == "buster" ]; then
echo ""
echo -e "$JAUNE"
echo "Vous mettez à jour un client Debian 10 (buster)."
# Correction sources.list pour deb-multimedia
echo "# Deb-multimedia
deb http://www.deb-multimedia.org buster main non-free" > /etc/apt/sources.list.d/deb-multimedia.list
echo -e "$COLINFO"
echo "Peut-on poursuivre? Appuyez sur <entrée>"
echo -e "$COLTXT"
echo ""
read lili

	elif [ "$distro" == "jessie" ]; then
echo ""
echo -e "$JAUNE"
echo "Vous mettez à jour un client Debian 8 (Jessie)."
# Correction sources.list pour deb-multimedia
echo "# Deb-multimedia
deb http://www.deb-multimedia.org jessie main non-free" > /etc/apt/sources.list.d/deb-multimedia.list
# Neutralisation sources geogebra (sources obsolètes)
> /etc/apt/sources.list.d/geogebra.list
echo -e "$COLINFO"
echo "Peut-on poursuivre? Appuyez sur <entrée>"
echo -e "$COLTXT"
echo ""
read lulu

	elif [ "$distro" == "stretch" ]; then
echo ""
echo -e "$JAUNE"
echo "Vous mettez à jour un client Debian 9 (Stretch)."
# Correction sources.list pour deb-multimedia
echo "# Deb-multimedia
deb http://www.deb-multimedia.org stretch main non-free" > /etc/apt/sources.list.d/deb-multimedia.list
echo -e "$COLINFO"
echo "Peut-on poursuivre? Appuyez sur <entrée>"
echo -e "$COLTXT"
echo ""
read lala
	fi

# Veut-on corriger les sources?
echo ""
echo -e "$JAUNE" "Ce script peut modifier automatiquement /etc/apt/sources.list" ; tput sgr0
echo -e "$JAUNE" "pour éviter les erreurs de mise à jour." ; tput sgr0
echo -e "$COLINFO" "le fichier sources.list actuel sera sauvegardé sous /etc/apt/sources.list_$DATE1" ; tput sgr0
echo ""
echo -e "$COLDEFAUT" "Voici le contenu du sources.list actuel :" ; tput sgr0
echo ""
cat /etc/apt/sources.list
echo ""

echo -e "$COLDEFAUT" "Voulez-vous modifier le fichier sources.list ? [o/N] " ; tput sgr0
read replist
	if [ "$replist" = "o" -o "$replist" = "oui" ]; then
echo ""
echo -e "$COLINFO" "Modification du fichier sources.liste demandée." ; tput sgr0
echo ""
LIST=o
	else
echo ""
echo -e "$COLINFO" "Pas de modification du fichier sources.list." ; tput sgr0
echo ""
	fi

# Modification du sources.list demandée

if [[ $LIST = "o" ]] ; then
	
	# On sauvegarde sources.list
echo -e "$COLINFO" "Sauvegarde du fichier sources.list actuel..." ; tput sgr0
cp /etc/apt/sources.list /etc/apt/sources.list_$DATE1

	# Changement des sources en fonction de la version
	if [ "$distro" == "buster" ]; then	
	# sources.list buster
echo ""
echo -e "$VERT" "Mise à jour des sources pour Buster." ; tput sgr0
echo ""
echo "deb http://deb.debian.org/debian/ buster main contrib non-free
#deb-src http://deb.debian.org/debian/ buster main

deb http://security.debian.org/ buster/updates main contrib non-free
#deb-src http://security.debian.org/debian-security buster/updates main" > /etc/apt/sources.list

	elif [ "$distro" == "jessie" ]; then
	# sources.list jessie
echo ""
echo -e "$VERT" "Mise à jour des sources pour Jessie." ; tput sgr0
echo ""
echo "deb http://deb.debian.org/debian jessie main contrib non-free
#deb-src http://deb.debian.org/debian jessie main contrib non-free

deb http://security-cdn.debian.org/debian-security/ jessie/updates main contrib non-free
#deb-src http://deb.debian.org/debian-security/ jessie/updates main contrib non-free

# Jessie-backports
# deb http://ftp.debian.org/debian/ jessie-backports main contrib non-free" > /etc/apt/sources.list

	elif [ "$distro" == "stretch" ]; then
	# sources.list stretch
echo ""
echo -e "$VERT" "Mise à jour des sources pour Stretch." ; tput sgr0
echo ""
echo "deb http://deb.debian.org/debian stretch main contrib non-free
#deb-src http://deb.debian.org/debian stretch main contrib non-free

deb http://security-cdn.debian.org/debian-security/ stretch/updates main contrib non-free

deb http://deb.debian.org/debian stretch-updates main contrib non-free
#deb-src http://deb.debian.org/debian stretch-updates main contrib non-free" > /etc/apt/sources.list
	fi

# Mise à jour des paquets après la modification des sources
echo ""
echo -e "$COLINFO" "Sources modifiées, mise à jour des paquets..." ; tput sgr0
echo ""
apt-get update
echo ""
echo -e "$COLINFO" "La modification des sources est terminée, on poursuit." ; tput sgr0
echo ""

else
echo -e "$COLINFO" "Pas de changement de /etc/apt/sources.list" ; tput sgr0
echo -e "$COLINFO" "============================================================" ; tput sgr0
echo ""
fi

# Utilisation d'un cache apt
	# Affichage de la conf du cache apt sur le client
monprox=$(cat /etc/monproxy)

	# Choix cache
echo -e "$COLDEFAUT" "Voulez-vous utiliser un cache APT pour les mises à jour ? [o/N]" ; tput sgr0
echo -e "$COLINFO" "Configuration actuelle : $monprox" ; tput sgr0
read replist2
	if [ "$replist2" = "o" -o "$replist2" = "oui" ]; then
echo ""
echo -e "$COLINFO" "Cache apt activé pour les màj (cool!)" ; tput sgr0
echo ""
APTCACHE=o
	else
echo ""
echo -e "$COLINFO" "Pas de cache apt pour les màj (dommage!)" ; tput sgr0
echo ""
APTCACHE=n
	fi

	# Nettoyage du cache du client avant la maj
echo -e "$COLINFO"
echo "Petit nettoyage préalable... Patientez un peu."
echo -e "$COLTXT"
apt-get update && apt-get autoclean && apt-get clean && apt-get update
echo -e "$VERT"
echo "Nettoyage terminé."
echo -e "$COLTXT"

if [ $APTCACHE = "o" ] ; then

	# Définition de l'adresse du proxy apt pour l'installation
	defaultcache="192.168.1.251:3142"
	echo -e "$COLINFO" "Vous utilisez un cache APT pour la mise à jour. "
	echo ""
		echo -e "$COLINFO" "Donnez l'adresse IP et le port du cache APT sous la forme 192.168.1.251:3142"
		echo -e "$COLDEFAUT" "Cache par défaut : [$defaultcache]"
		echo ""
		echo  -e "$COLTXT" "Validez par entrée si cette adresse convient, ou entrez une autre adresse (sans oublier le port) :"
		echo ""
		read APTPROXY
		if [ -z "$APTPROXY" ]; then
		APTPROXY=$defaultcache
		fi
		echo -e "$COLDEFAUT" "L'adresse IP du cache apt est $APTPROXY"
		echo -e "$COLTXT"
		
	echo ""
	echo -e "$COLINFO"
	echo "Ajout du proxy apt à la configuration..."
	echo -e "$COLTXT"

	# Déclaration du proxy pour apt (activation cache, témoin)
	touch /etc/apt/apt.conf.d/99proxy
echo "#Configuration du proxy apt pour l'installation
Acquire::http::Proxy::download.virtualbox.org "DIRECT";
Acquire::http { Proxy \"http://$APTPROXY\"; };" > /etc/apt/apt.conf.d/99proxy
echo "Avec cache" > /etc/monproxy

else
	# Cache désactivé dans la conf apt, témoin positionné
echo "Neutralisation du proxy apt dans la configuration..."
> /etc/apt/apt.conf.d/99proxy
echo "Sans cache" > /etc/monproxy
fi

# On désinstalle java8 qui commence à être lourdaud!
apt-get purge -y oracle-java8-installer

# On supprime le depot de oracle-java8-installer
> /etc/apt/sources.list.d/webupd8team-java.list 

# On règle les problèmes de mise à jour automatiques inachevées...
echo -e "$COLINFO"
echo "Il y a peut-être des màj inachevées..."
echo -e "$COLTXT"
apt-get update
apt-get install -f -y

# Il y a parfois des problèmes avec les màj de vlc...
echo -e "$COLINFO"
echo "Mise à jour de vlc..."
echo -e "$COLTXT"
apt-get install -y vlc vlc-data

# On met à jour les lecteurs flash
echo -e "$COLINFO"
echo "Mise à jour des lecteurs flash pour firefox et chromium..."
echo -e "$COLTXT"
apt-get install -y flashplayer-mozilla flashplayer-chromium

# Mise à jour du client
echo -e "$COLINFO"
echo "Mise à jour du client..."
echo -e "$COLTXT"
apt-get upgrade -y && apt-get dist-upgrade -y
echo -e "$VERT"
echo "Mise à jour terminée."
echo ""
echo -e "$COLTXT"

# Nettoyage du poste et rechargement de la liste des paquets
echo -e "$COLINFO"
echo "On fait un peu de ménage..."
echo -e "$COLTXT"
apt-get autoremove -y && apt-get autoclean && apt-get update

# redémarrage des services au cas où...
#systemctl enable NetworkManager-wait-online.service
#systemctl enable ssh.service

# désactivation du cache
echo ""
echo -e "$COLINFO"
echo "Neutralisation du proxy apt dans la configuration..."
echo -e "$COLTXT"
> /etc/apt/apt.conf.d/99proxy && echo "Sans cache" > /etc/monproxy

# Messages de fin
echo -e "$VERT"
echo "Terminé !"
echo -e "$COLTXT"
