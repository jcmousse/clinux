#!/bin/bash

# **************************************************************
# correctif_cles_ssh.sh
# Correctif d'importation des cles ssh pour les clients
# A lancer sur les clients linux
# 20151211
# **************************************************************

ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"
DATERAPPORT=$(date +%F+%0kh%0M)

# On rend le script "cretin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a executer sur les clients Linux, pas sur le serveur !" && exit 1

TEST_PROXY=$(cat /etc/profile | grep "http_proxy")
if [ -n "$TEST_PROXY" ]; then
	echo -e $JAUNE "Un proxy est renseigné, on le supprime temporairement." ; tput sgr0
	sed -i '/export/d' /etc/profile
	sed -i '/Configuration/d' /etc/profile
	echo -e $VERT "Le proxy http a été supprimé dans /etc/profile." ; tput sgr0
	echo "Le script va s'interrompre..."
	echo -e "$ROUGE" "Il faut sortir du shell, se relogger (en root) puis relancer le script." ; tput sgr0
	sleep 1
	exit
else echo "On continue... "
fi

# Recuperation des cles ssh depuis le se3
mkdir -p /root/.ssh
chmod 700 /root/.ssh
cd /root/.ssh

SE3_IP=##se3ip##
if [ -f "authorized_keys" ];then
wget -o log_recuperation_cle_pub_se3_$DATERAPPORT -O authorized_keys_se3 $SE3_IP:909/authorized_keys
cat authorized_keys_se3 >> authorized_keys
wget -o log_recuperation_cle_pub_wwwse3_$DATERAPPORT -O authorized_keys_wwwse3 $SE3_IP:909/authorized_keys_wwwse3
cat authorized_keys_wwwse3 >> authorized_keys
else
wget -o log_recuperation_cle_pub_se3_$DATERAPPORT -O authorized_keys $SE3_IP:909/authorized_keys
wget -o log_recuperation_cle_pub_wwwse3_$DATERAPPORT -O authorized_keys_wwwse3 $SE3_IP:909/authorized_keys_wwwse3
cat authorized_keys_wwwse3 >> authorized_keys
fi
chmod 400 /root/.ssh/authorized_keys
cd

PROXY=##ipamon##:3128
# On remet l'adresse du proxy http dans /etc/profile
echo "#Configuration du proxy pour l'établissement
export http_proxy=\"http://$PROXY\"
export https_proxy=\"https://$PROXY\"" >> /etc/profile

echo -e $VERT "Le proxy http a été réinscrit dans /etc/profile."  ; tput sgr0

exit 0
