#!/bin/bash
###########################################################
# se3_cree_rep_parc.sh
# Ce script est a lancer sur les se3.
# Il crée les répertoires nécessaires pour les raccourcis,
# les cles publiques et les configurations italc
# affectés à des parcs.
# 20151216
##########################################################
DATE1=$(date +%F+%0kh%0M)
ftp=https://raw.githubusercontent.com/jcmousse/clinux/master

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# Test de la présence du paquet se3-clients-linux
[ ! -e /home/netlogon/clients-linux ] && echo "Le paquet se3-clients-linux n'est pas installé. Installez le et relancez le script." && exit 1

# Liste des parcs existants
> /var/se3/Docs/persolinks/listeparcs.txt
ls /var/se3/Docs/persolinks|egrep -v "(^base$|^profs$|^eleves$|^italc$|^autres$|^listeparcs.*)"|while read A
do
echo $A >> /var/se3/Docs/persolinks/listeparcs.txt
done

# Affichage des parcs existants
echo -e "$COLINFO"
echo "Voici la liste des parcs existants :"
echo ""
echo -e "$COLTXT"
echo -e "$COLDEFAUT"
cat /var/se3/Docs/persolinks/listeparcs.txt
echo ""
echo -e "$COLTXT"
#echo -e "$COLINFO"
echo "Appuyez sur entrée pour continuer..."
read toto
#echo -e "$COLTXT"

# Choix du parc à créer
echo -e "$COLINFO"
echo "Pour quel parc voulez-vous préparer les répertoires ?"
echo ""
echo -e "$COLTXT"
echo "Indiquez un nom de parc ou de salle : cdi, c4, ..."
echo ""
#echo -e "$COLTXT"
read repparc

# Création du répertoire de raccourcis pour le parc
test1=$(ls /var/se3/Docs/persolinks | grep $repparc)
if [ ! -n "$test1" ]; then 
mkdir -p /var/se3/Docs/persolinks/$repparc
echo -e "$VERT"
echo "/var/se3/Docs/persolinks/$repparc a été créé."
echo -e "$COLTXT"
else
echo -e "$COLDEFAUT"
echo "Le répertoire pour les raccourcis du parc $repparc existe déjà."
echo -e "$COLTXT"
fi
# Réglage des droits sur persolinks
chown -R admin:admins /var/se3/Docs/persolinks
chmod -R 755 /var/se3/Docs/persolinks
chmod 750 /var/se3/Docs/persolinks/listeparcs.txt

sleep 1

# Création du répertoire qui recevra GlobalConfig.xml (italc-master) pour le parc
test2=$(ls /var/se3/Docs/italc | grep $repparc)
if [ ! -n "$test2" ]; then
mkdir -p /var/se3/Docs/italc/$repparc
echo -e "$VERT"
echo "/var/se3/Docs/italc/$repparc a été créé."
echo -e "$COLTXT"
cd /var/se3/Docs/italc/$repparc
wget --no-check-certificate --quiet $ftp/se3/raccourcis/GlobalConfig.xml
echo -e "$VERT"
echo "GlobalConfig.xml a été téléchargé."
echo -e "$COLTXT"
sed -i 's|<classroom\ name.*|<classroom\ name=\"'$repparc'\">|g' /var/se3/Docs/italc/$repparc/GlobalConfig.xml
else
echo -e "$COLDEFAUT"
echo "Le répertoire pour la configuration italc-master de $repparc existe déjà."
echo -e "$COLTXT"
fi
# Droits /var/se3/Docs/italc
chown -R root:admins /var/se3/Docs/italc
chmod -R 775 /var/se3/Docs/italc

sleep 1

# Memento pour le réglage des acl par l'interface
echo -e "$COLDEFAUT"
echo "Pensez à régler les acl sur le répertoire /var/se3/Docs/italc"
echo "par l'interface du se3 !"
echo ""
echo -e "$COLTXT"

sleep 1

# Création du répertoire qui recevra la clé publique teacher (italc-client) pour le parc
test3=$(ls /home/netlogon/clients-linux/divers/italc | grep $repparc)
if [ ! -n "$test3" ]; then
mkdir -p /home/netlogon/clients-linux/divers/italc/$repparc/public/
echo -e "$VERT"
echo "/home/netlogon/clients-linux/divers/italc/$repparc/public/ a été créé."
echo -e "$COLTXT"
else
echo -e "$COLDEFAUT"
echo "Le répertoire pour la clé publique italc-client de $repparc existe déjà."
echo -e "$COLTXT"
fi

sleep 1

## On va récupérer la clé publique depuis le poste italc-master
# Répertoire de la clé publique sur le poste italc-master
repcle=/etc/italc/keys/public/teacher

echo -e "$JAUNE"
echo "###########################################################################"
echo ""
echo "Voulez-vous récupérer la clé publique teacher du poste italc-master ?"
echo ""
echo "Attention, italc-master doit être installé au préalable sur un poste du parc $repparc."
echo "Si ce n'est pas fait, quittez ce script. Vous pourrez le relancer plus tard."
echo ""
echo "Il faudra indiquer l'adresse ip du poste maitre italc du parc $repparc et son mdp root."
echo ""
echo "###########################################################################"
echo -e "$COLTXT"
echo -e "$COLINFO"
read -p "Que voulez-vous faire ?

1 (je veux copier la clé publique sur le se3)
2 (non, je veux sortir !) : " rep
echo -e "$COLTXT"
			case $rep in

				1 )
				echo -e "$JAUNE"
                                echo "Entrez l'adresse ip du poste italc-master : "
                                echo -e "$COLTXT"
                                read IPMAST
				rm -rf /home/netlogon/clients-linux/divers/italc/$repparc/public/*
				cd /home/netlogon/clients-linux/divers/italc/$repparc/public
				scp -r root@$IPMAST:$repcle . || ERREUR="1"
                                if [ "$ERREUR" = "1" ];then
                                        echo -e "$COLERREUR"
                                        echo "Erreur lors de la copie de la clé..."
					echo "Vérifier l'adresse ip du poste italc-master puis relancez le script."
                                        echo -e "$COLTXT"
                                else	
					chown -R :Eleves /home/netlogon/clients-linux/divers/italc/$repparc/public
                                        echo -e "$VERT"
                                        echo "La clé a été copiée depuis le poste italc-master $IPMAST pour le parc $repparc"
					echo -e "$COLTXT"
                                fi
                                ;;

				* ) 	
				echo -e "$COLINFO"
				echo "Pas de copie de la clé publique italc pour le parc $repparc."
				echo -e "$VERT"
				echo -e "$COLINFO"
				echo "A bientôt !"
				echo -e "$COLTXT" 
				exit 0
				;;
			esac

# Réglage des droits sur les répertoires des clés
echo "Rétablissement des droits..."
permse3

# Message de fin
echo -e "$VERT"
echo "Terminé."
echo -e "$COLTXT"

