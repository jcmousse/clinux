#!/bin/bash

# ****************************************************************
# se3_prepare_clients_linux.sh
# Script principal de préparation des se3 pour les clients linux
# A lancer sur le se3.
# 20181230
# ****************************************************************

DATE1=$(date +%F+%0kh%0M)

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

ftp=https://gitlab.com/jcmousse/clinux/raw/master
#ftp=https://raw.githubusercontent.com/jcmousse/clinux/master
ftp2=http://jcmousse.free.fr

# Test de la présence du paquet se3-clients-linux
[ ! -e /home/netlogon/clients-linux ] && echo "Le paquet se3-clients-linux n'est pas installé. Installez le et relancez le script." && exit 1

# Test de la connexion

LINE_TEST()
{
if ( ! wget --no-cache --no-cache -q --output-document=/dev/null 'https://google.fr') ; then
	ERREUR "Votre connexion internet ne semble pas fonctionnelle !"
	exit 1
fi
}

##################################################################
# Verification et téléchargement de la dernière version du script
##################################################################

NODL="no"
DEBUG="no"

# Test de l'accès internet
LINE_TEST

if [ "$NODL" != "yes" ]; then
	echo -e "$COLINFO"
	echo "Vérification en ligne que vous avez bien la dernière version du script..."
	echo -e "$COLTXT"

	cd /root
	SCRIPT_FILE="se3_prepare_clients_linux.sh"
	SCRIPT_FILE_MD5="se3_prepare_clients_linux.md5"
	SCRIPTS_DIR="/usr/share/se3/sbin"
	
	# Téléchargement dans /root du script à jour et du fichier md5
	rm -f $SCRIPT_FILE_MD5 $SCRIPT_FILE
	wget --no-cache --no-check-certificate -N --tries=1 --connect-timeout=1 --quiet $ftp/se3/$SCRIPT_FILE || ERREUR="1"
	wget --no-cache --no-check-certificate -N --tries=1 --connect-timeout=1 --quiet $ftp/se3/$SCRIPT_FILE_MD5 || ERREUR="1"
	if [ "$ERREUR" = "1" ];then
		echo -e "$COLERREUR"
		echo "Problème pour récupérer la version en ligne : ABANDON !"
		echo -e "$COLTXT"
		exit 1	
	fi
	
	# Calcul des md5 des fichiers qu'on vient de télécharger
	MD5_CTRL_FILE=$(cat $SCRIPT_FILE_MD5) # contenu du md5 téléchargé
	MD5_CTRL_DL=$(md5sum $SCRIPT_FILE) # md5 du script téléchargé

	# Controle de ce qu'on vient de télécharger
	if [ "$MD5_CTRL_FILE" != "$MD5_CTRL_DL" ]
	then	
		# Il y a une erreur dans ce qui a été téléchargé
		echo -e "$COLERREUR"
		echo "Controle MD5 du script téléchargé incorrect. Relancez le script pour qu'il soit de nouveau téléchargé."
		echo -e "$COLTXT"
		exit 1
	fi

	# On se rend dans /usr/share/se3/sbin
	cd $SCRIPTS_DIR

	# Calcul du md5 de /usr/share/se3/sbin/se3_prepare_clients_linux.sh
	MD5_CTRL_LOCAL=$(md5sum $SCRIPT_FILE)

	# On retourne dans /root et on rend le script téléchargé executable
	cd
	chmod +x *.sh

	# On compare le md5 du script présent sur le se3 avec celui du script téléchargé
	if [ "$MD5_CTRL_FILE" != "$MD5_CTRL_LOCAL" ]; then

		# Si les md5 sont différents, on passe la variable relance à yes
		RELANCE="YES"
		# Puis on copie le script depuis /root vers le repertoire des scripts
		cp $SCRIPT_FILE $SCRIPTS_DIR/
	fi

	if [ "$RELANCE" == "YES" ]
	then
		echo -e "$JAUNE"
		echo "Le script a été mis à jour depuis le dépôt."
		echo "Relancez se3_prepare_clients_linux.sh pour utiliser la nouvelle version."
		echo ""
		sleep 1
		exit 1
		echo -e "$COLTXT"
	fi

	echo -e "$VERT"
	echo ""
	echo "Vous disposez de la dernière version du script, on peut continuer..."
	sleep 3
	echo -e "$COLTXT"
else
echo "Mode debug, pas de téléchargement."
sleep 2
fi

###########################################################################################################

# Modification de smb.conf pour affichage de l'utilisateur connecté dans l'interface
TEST_PORT=$(cat /etc/samba/smb.conf | grep "smb ports = 139")
if [ ! -n "$TEST_PORT" ]; then
echo -e "$COLDEFAUT"
echo "On modifie smb.conf..."
echo -e "$COLTXT"
cp /etc/samba/smb.conf /etc/samba/smb.conf_$DATE1
sed -i 's/unix\ extensions\ =\ no/unix\ extensions\ =\ no \n\	smb\ ports\ =\ 139/g' /etc/samba/smb.conf
else
echo -e "$COLINFO"
echo "smb.conf a déjà été modifié."
echo -e "$COLTXT"
fi

# Récupération des variables du se3 ip_se3, base_dn et rne
TEST_ADR=$(ifconfig eth0 | grep "inet adr")
if [ -n "$TEST_ADR" ]; then
# ifconfig contient inet adr
getse3ip=$(ifconfig eth0 | grep inet\ adr | cut -d: -f2 | awk '{ print $1}')
else
# ifconfig contient inet addr
getse3ip=$(ifconfig eth0 | grep inet\ addr | cut -d: -f2 | awk '{ print $1}')
fi
getsuffix=$(cat /etc/samba/smb.conf | grep ldap\ suffix |  cut -c16-)
getrne=$(cat /etc/samba/smb.conf | grep ldap\ suffix |  cut -c19- | cut -c-8)

# Création des répertoires indispensables
echo -e "$COLINFO"
echo "Création des répertoires indispensables..."
echo -e "$COLTXT"
rep2=/home/netlogon/clients-linux/alancer
rep3=/home/netlogon/clients-linux/once
rep4=/home/netlogon/clients-linux/conex
rep11=/home/netlogon/clients-linux/divers/italc/cdi/public

	if [ ! -e $rep2 ]; then
	mkdir -p /home/netlogon/clients-linux/alancer
	fi
	if [ ! -e $rep3 ]; then
	mkdir -p /home/netlogon/clients-linux/once
	fi
	if [ ! -e $rep4 ]; then
	mkdir -p /home/netlogon/clients-linux/conex
	fi
	if [ ! -e $rep11 ]; then
	mkdir -p /home/netlogon/clients-linux/divers/italc/cdi/public
	fi
echo ""

# On affiche l'ip du se3
echo -e "$COLDEFAUT"
echo " Adresse IP actuelle du se3 : $getse3ip"
echo -e "$COLTXT"


# Configuration des proxys http et apt
rm -f /root/proxys*
mkdir -p /etc/clinux
touch /etc/clinux/proxys.data

dirprox=/etc/clinux
fichproxy=proxys.data
testapt=$(cat $dirprox/$fichproxy | grep ip_cache)
testamon=$(cat $dirprox/$fichproxy | grep ip_amon)

if [ ! -n "$testapt" ] ; then
#echo -e "$JAUNE" "testapt faux" ; tput sgr0
echo -e "$COLTITRE"
echo "Aucun cache apt n'a été déclaré sur ce réseau."
echo "Y a-t-il un cache apt dans l'etablissement ?"
echo -e "$COLTXT"
		PS3='Repondre par o ou n: '
		LISTE=("[o] oui" "[n]  non")
		select CHOIX in "${LISTE[@]}" ; do
			case $REPLY in
				1|o)
				echo -e "$VERT"
				echo "Il y a un cache apt dans cet etablissement (cool)."
				echo -e "$COLTXT"
				CACHE=o
				break
				;;
				2|n)
				echo -e "$COLDEFAUT"
				echo "Pas de cache apt ici (beurk)."
				echo -e "$COLTXT"
				echo ""
				CACHE=n
				break
				;;
			esac
done
else CACHE=o
fi

	# Définition de l'adresse du cache apt de l'etablissement
if [ $CACHE = "o" ] ; then
	if [ ! -n "$testapt" ] ; then
		echo "Adresse du CACHE APT de l'etablissement :"
		echo -e "$COLINFO"
		echo "Entrez l'adresse IP du cache APT sous la forme 172.17.31.251"
		echo -e "$COLTXT"
		read APTCACHE
		echo "ip_cache=$APTCACHE" >> $dirprox/$fichproxy
		echo -e "$JAUNE" "ip du cache stockée dans le fichier" ; tput sgr0
		echo -e "$COLINFO"
		echo "L'adresse du CACHE APT est $APTCACHE"
		echo -e "$COLTXT"
		echo ""
	else		
		APTCACHE=$(cat $dirprox/$fichproxy | grep ip_cache | cut -d= -f2 | awk '{ print $1}')
		echo -e "$COLINFO" "valeur lue pour le cache : $APTCACHE " ; tput sgr0
	fi
else
	if [ $CACHE = "o" ] ; then
	# On affiche l'adresse ip du cache
	echo "Voici l'adresse du cache apt enregistrée sur ce serveur :"
	echo -e "$testapt"	
	fi
fi

	# Définition de l'adresse du proxy amon de l'etablissement
	if [ ! -n "$testamon" ] ; then
		echo "Adresse du proxy AMON de l'etablissement :"
		echo -e "$COLINFO"
		echo "Entrez l'adresse du proxy Amon de l'etablissement sous la forme 172.17.31.254"
		echo -e "$COLTXT"
		read IPAMON
		echo "ip_amon=$IPAMON" >> $dirprox/$fichproxy
		echo -e "$JAUNE" "ip de l'Amon stockée dans le fichier" ; tput sgr0
		echo -e "$COLINFO"
		echo "L'adresse du proxy Amon est $IPAMON"
		echo -e "$COLTXT"
		echo ""
	else
		IPAMON=$(cat $dirprox/$fichproxy | grep ip_amon | cut -d= -f2 | awk '{ print $1}')
		echo -e "$COLINFO" "valeur lue pour l'amon : $IPAMON " ; tput sgr0
	fi

	# On affiche les adresses lues dans le fichier $dirprox/$fichproxy
		echo -e "$COLINFO"
		echo "Voici les valeurs enregistrées actuellement pour le proxy amon,"
		echo "et éventuellement pour le cache apt (si présent) : "
		echo -e "$COLDEFAUT"
			cat $dirprox/$fichproxy
		echo -e "$COLINFO"
		echo "Les adresses sont-elles correctes? "
		echo -e "$COLTXT"
		read -p "	o (oui)
	n (non) : " rep

	case $rep in
			o )
	# On met les bonnes valeurs dans les scripts
		cd $rep2
		rm -f $rep2/maj_java*
		rm -f $rep2/maj_client_*
		#wget --no-cache --no-check-certificate --quiet $ftp/se3/alancer/maj_java_local.sh
		#wget --no-cache --no-check-certificate --quiet $ftp/se3/alancer/maj_client_stretch.sh
		#wget --no-cache --no-check-certificate --quiet $ftp/se3/alancer/maj_client_jessie.sh
		wget --no-cache --no-check-certificate --quiet $ftp/se3/alancer/maj_client_linux.sh
		#wget --no-cache --no-check-certificate --quiet $ftp/se3/alancer/maj_client_linux_v2.sh
	if [ -n "$testapt" -o $CACHE = "o" ] ; then
		#sed -i 's/##ipcache##/'$APTCACHE'/g' $rep2/maj_java_local.sh
		#sed -i 's/##ipamon##/'$IPAMON'/g' $rep2/maj_java_local.sh
		sed -i 's/##ipcache##/'$APTCACHE'/g' $rep2/maj_client_*
		sed -i 's/##ipamon##/'$IPAMON'/g' $rep2/maj_client_*
		echo -e "$COLINFO"
		echo "maj_client_*.sh téléchargés ou mis à jour. "
		echo -e "$COLTXT"
		rm -f change_proxy_client.sh
		wget --no-cache --no-check-certificate --quiet $ftp/se3/alancer/change_proxy_client.sh
		sed -i 's/##ipcache##/'$APTCACHE'/g' $rep2/change_proxy_client.sh
		sed -i 's/##ipamon##/'$IPAMON'/g' $rep2/change_proxy_client.sh
		echo -e "$COLINFO"
		echo "change_proxy_client.sh téléchargé ou mis à jour. "
		echo -e "$COLTXT"
		rm -f $rep3/chg_proxy_unefois.sh
		cd $rep3
		wget --no-cache --no-check-certificate --quiet $ftp/se3/once/chg_proxy_unefois.sh
		sed -i 's/##ipcache##/'$APTCACHE'/g' $rep3/chg_proxy_unefois.sh
	else
		#sed -i 's/##ipcache##/'$IPAMON'/g' $rep2/maj_java*
		#sed -i 's/##ipamon##/'$IPAMON'/g' $rep2/maj_java*
		#sed -i 's/3142/3128/g' $rep2/maj_java*
		sed -i 's/##ipcache##/'$IPAMON'/g' $rep2/maj_client_*
		sed -i 's/##ipamon##/'$IPAMON'/g' $rep2/maj_client_*	

		echo -e "$COLINFO"
		echo "Scripts de mise à jour des clients téléchargés ou mis à jour. "
		echo -e "$COLTXT"
	fi

	# Scripts de correction des clés ssh, de renommage et d'installation
		cd $rep2
		rm -f $rep2/correctif_cles_ssh*
		wget --no-cache --no-check-certificate --quiet $ftp/se3/alancer/correctif_cles_ssh.sh
		sed -i 's/##ipamon##/'$IPAMON'/g' $rep2/correctif_cles_ssh.sh
		sed -i 's/##se3ip##/'$getse3ip'/g' $rep2/correctif_cles_ssh.sh
		echo -e "$COLINFO"
		echo "correctif_cles_ssh.sh téléchargé ou mis à jour. "
		echo -e "$COLTXT"
		rm -f $rep2/renomme_client_linux*
		wget --no-cache --no-check-certificate --quiet $ftp/se3/alancer/renomme_client_linux_v2.sh
		sed -i 's/##se3rne##/'$getrne'/g' $rep2/renomme_client_linux_v2.sh
		echo -e "$COLINFO"
		echo "renomme_client_linux_v2.sh téléchargé ou mis à jour. "
		echo -e "$COLTXT"
		rm -f $rep2/corrige_uuid_swap*
		#wget --no-cache --no-check-certificate --quiet $ftp/se3/alancer/corrige_uuid_swap.sh
		#echo -e "$COLINFO"
		#echo "corrige_uuid_swap.sh téléchargé ou mis à jour. "
		#echo -e "$COLTXT"
		#echo -e "$COLTXT"
		rm -f $rep2/copy_printer*
		wget --no-cache --no-check-certificate --quiet $ftp/se3/alancer/copy_printer.sh
		echo -e "$COLINFO"
		echo "copy_printer.sh téléchargé ou mis à jour. "
		echo -e "$COLTXT"
		# Obsolete
		rm -f $rep2/create_machine*
		#wget --no-cache --no-check-certificate --quiet $ftp/se3/alancer/create_machine.sh
		#sed -i 's/##se3ip##/'$getse3ip'/g' $rep2/create_machine.sh
		#echo -e "$COLINFO"
		#echo "create_machine.sh téléchargé ou mis à jour. "
		#echo -e "$COLTXT"
		rm -f $rep2/chg_uuid_swap*
		#wget --no-cache --no-check-certificate --quiet $ftp/se3/alancer/chg_uuid_swap.sh
		rm -f $rep2/copie_clepub_italc*
		rm -f $rep2/install_italc-*
		#wget --no-cache --no-check-certificate --quiet $ftp/se3/alancer/copie_clepub_italc.sh
		#wget --no-cache --no-check-certificate --quiet $ftp/se3/alancer/install_italc-master.sh
		#wget --no-cache --no-check-certificate --quiet $ftp/se3/alancer/install_italc-client.sh
		#echo -e "$COLINFO"
		#echo "scripts d'installation italc téléchargés ou mis à jour. "
		rm -f $rep2/copie_clepub_veyon*
		wget --no-cache --no-check-certificate --quiet $ftp/se3/alancer/copie_clepub_veyon.sh
		echo "script d'installation veyon téléchargé ou mis à jour. "
		echo -e "$COLTXT"
		#rm -f $rep2/change_sources_list*
		#wget --no-cache --no-check-certificate --quiet $ftp/se3/alancer/change_sources_list.sh
		#echo -e "$COLINFO"
		#echo "script change_sources_list.sh téléchargé ou mis à jour. "
		#echo -e "$COLTXT"
		
		rm -f $rep2/xscreensaver_*
		wget --no-cache $ftp2/se3/xscreensaver_5.36-1_amd64.deb
		wget --no-cache $ftp2/se3/xscreensaver_5.36-1_i386.deb
		echo -e "$JAUNE" "paquets xscreensaver téléchargés." ; tput sgr0
		cd
		echo ""
		echo -e "$VERT" "Variables remplacées dans les scripts." ; tput sgr0
		cd
	;;
			* )
		echo -e "$COLDEFAUT"
		echo "Corrigez les valeurs dans $dirprox/$fichproxy et relancez ce script."
		echo -e "$COLTXT"
		exit 1
		;;
	esac

if [ -d $rep4 ]; then
[ -f $rep4/panel ] && rm -f $rep4/panel*
cd $rep4
wget --no-cache --no-check-certificate --quiet $ftp/se3/raccourcis/panel
cd
echo -e "$COLINFO"
echo "panel téléchargé."
echo -e "$COLTXT"
fi

# On importe sur le se3 le script d'autorisation de vnc pour un utilisateur au choix
cd /usr/share/se3/sbin
rm -f se3_install_vnc_user.sh
wget --no-cache --no-check-certificate --quiet $ftp/commun/vnc/se3_install_vnc_user.sh
chmod +x se3_install_vnc_user.sh
echo -e "$COLINFO"
echo "Script vnc téléchargé."
echo -e "$COLTXT"

# Script de lancement des scripts unefois
echo -e "#!/bin/bash
#
DATE1=\$(date +%F+%0kh%0M)

# On vérifie l'existence de /root/temoins sur le client, on créé le répertoire si besoin
if [ ! -e /root/temoins ]; then
mkdir /root/temoins
fi

# Parcours du répertoire des scripts unefois et lancement de chaque script
cd /mnt/netlogon/once
    for item in * ; do
        echo \"### \$(date) BEGIN \${item}\" > /root/temoins/log.\${item}
        "./\${item}"
        echo \"### \$(date) END   \${item}\" >> /root/temoins/log.\${item}
    done

# Lancement d'italc si présent
if [ -e \"/usr/bin/ica\" ] ; then
#killall ica
#sleep 1
#/usr/bin/ica &
echo \"rien\"
fi" > /home/netlogon/clients-linux/conex/init_unefois.sh

echo -e "$COLINFO"
echo "/conex/init_unefois.sh créé ou mis à jour. "
echo -e "$COLTXT"

# Script de copie des raccourcis sur les bureaux à la connexion
# en fonction du parc et du groupe de l'utilisateur
echo -e "#!/bin/bash
# Si Italc est installé sur le poste
if [ -e \"/usr/bin/ica\" ] ; then
killall ica &
sleep 1
ica &
echo \"Italc lancé\"
fi
# Si Veyon est installé sur le poste
if [ -e \"/usr/bin/veyon-service\" ] ; then
#killall veyon-service &
#sleep 1
veyon-worker &
sleep 1
veyon-service &
echo \"Veyon lancé\"
fi
sleep 1
if [ ! -e \"/home/\$USER/profil/links\" ]; then
mkdir /home/\$USER/profil/links
fi
# Liens sur le bureau des utilisateurs
ln -s /media/Classes /home/\$USER/profil/Bureau/Classes
ln -s /media/Partages /home/\$USER/profil/Bureau/Partages
ln -s /home/\$USER/Docs /home/\$USER/Bureau/Mes_Docs
# Copie des raccourcis des parcs
cp /media/Partages/persolinks/base/*.desktop /home/\$USER/profil/Bureau/
cp /media/Partages/persolinks/base/* /home/\$USER/profil/links/
PARC=\$(cat /etc/parc.txt)
cp /media/Partages/persolinks/\$PARC/*.desktop /home/\$USER/profil/Bureau/
cp /media/Partages/persolinks/\$PARC/* /home/\$USER/profil/links/
# Ménage des raccourcis windoze sur le bureau de l'utilisateur
rm -f /home/\$USER/profil/Bureau/*.lnk
# Copie des raccourcis des groupes
n=\$(ldapsearch -xLLL -h "$getse3ip" -b \"cn=eleves,ou=Groups,$getsuffix\" \"(memberUid=\$USER)\" \"dn\" | wc -l)
	if [ "\$n" -eq "0" ]; then
        # n = 0 signifie que la recherche n a donné aucun résultat, donc le compte n appartient pas au groupe eleves.
		echo \"\$USER n est pas membre du groupe eleves\"
	# On interroge alors le groupe profs.
        	m=\$(ldapsearch -xLLL -h "$getse3ip" -b \"cn=profs,ou=Groups,$getsuffix\" \"(memberUid=\$USER)\" \"dn\" | wc -l)
		if [ "\$m" -eq "0" ]; then
		echo \"\$USER n est pas membre du groupe profs\"
		# m = 0 signifie que la recherche n'a donné aucun résultat, donc le compte n'appartient pas au groupe profs.
        	#return 1
    		else
       		# Sinon, la recherche a donné un résultat, donc le compte appartient au groupe profs.
		echo \"\$USER est membre du groupe profs\"
        	# Copie des raccourcis profs
		cp /media/Partages/persolinks/profs/*.desktop /home/\$USER/profil/Bureau/
		cp /media/Partages/persolinks/profs/* /home/\$USER/profil/links/
			# Si italc-master est installé sur le client, on copie aussi un raccourci italc-prof sur le bureau
			if [ -e \"/usr/bin/italc\" ] ; then
			echo \"italc-master est installé sur ce poste, on copie le raccourci.\"
			cp /media/Partages/persolinks/autres/italc-prof.desktop /home/\$USER/profil/Bureau/
			cp /media/Partages/persolinks/autres/italc-prof.desktop /home/\$USER/profil/links/
			# Si veyon-master est installé  et exécutable sur le client, on copie un raccourci veyon-master sur le bureau
			elif [ -x \"/usr/bin/veyon-master\" ];then
			echo \"veyon-master est exécutable sur ce poste, on copie le raccourci.\"
			cp /media/Partages/persolinks/autres/veyon-master.desktop /home/\$USER/profil/Bureau/
			cp /media/Partages/persolinks/autres/veyon-master.desktop /home/\$USER/profil/links/
			else
			echo \"Aucune solution de supervision n'est installée sur ce poste.\"
			fi
    		fi
	else
        # Sinon, la recherche a donné un résultat, donc le compte appartient au groupe eleves.
	echo \"\$USER est membre du groupe eleves\"
        # Copie des raccourcis eleves
	cp /media/Partages/persolinks/eleves/*.desktop /home/\$USER/profil/Bureau/
	cp /media/Partages/persolinks/eleves/* /home/\$USER/profil/links/
	fi

# Avertissement en cas de dépassement de quota
p=\$(ldapsearch -xLLL -h "$getse3ip" -b \"cn=overfill,ou=Groups,$getsuffix\" \"(memberUid=\$USER)\" \"dn\" | wc -l)
	if [ "\$p" -eq "0" ]; then
        # p=0 signifie que la recherche n a donné aucun résultat, donc le compte n appartient pas au groupe overfill.
	echo \"Pas de dépassement de quota pour ce compte\"
	else
	# Le compte est dans overfill, il faut avertir l'utilisateur
	echo \"Depassement de quota pour \$USER\"
	firefox http://$getse3ip:909 &
	sleep 3
	leafpad /mnt/netlogon/conex/message_overfill.txt &
	fi

sed -i 's/icon/list/g' /home/\$USER/.config/pcmanfm/LXDE/pcmanfm.conf
sed -i 's/hidden=1/hidden=0/g' /home/\$USER/.config/pcmanfm/LXDE/pcmanfm.conf
sed -i 's/_exec=0/_exec=1/g' /home/\$USER/.config/libfm/libfm.conf
cat /mnt/netlogon/conex/mimeapps.list > /home/\$USER/.config/mimeapps.list

bash /mnt/netlogon/conex/alsavolume.sh
bash /mnt/netlogon/conex/profil_ff.sh
bash /mnt/netlogon/conex/clean_vartmp.sh
#bash /mnt/netlogon/conex/warn_overfill.sh

update-menus
exit 0" > /home/netlogon/clients-linux/conex/createlinks.sh

echo -e "$COLINFO"
echo "/conex/createlinks.sh créé ou mis à jour."
echo -e "$COLTXT"

# script de déconnexion des utilisateurs
echo -e "#!/bin/bash
# Fin d'italc
if [ -e \"/usr/bin/ica\" ] ; then
killall ica &
echo \"Italc stoppé\"
fi
# Fin de Veyon
if [ -e \"/usr/bin/veyon-service\" ] ; then
killall veyon-service &
echo \"Veyon stoppé\"
fi
# Suppression des liens bureau
rm -f /home/\$USER/profil/Bureau/Classes
rm -f /home/\$USER/profil/Bureau/Partages
rm -f /home/\$USER/profil/Bureau/Mes_Docs
# Suppression des raccourcis copiés à la connexion
ls /home/\$USER/profil/links | while read A ; do rm -f /home/\$USER/profil/Bureau/\$A ; done
sleep 1
rm -f /home/\$USER/profil/links/*
# Effacement du profil lxrandr s'il existe
rm -f /home/\$USER/.config/autostart/lxrandr*
# Restauration de l'affichage détaillé dans les vues de dossier
sed -i 's/icon/list/g' /home/\$USER/.config/pcmanfm/LXDE/pcmanfm.conf
# Suppression du dialogue de lancement des raccourcis
sed -i 's/_exec=0/_exec=1/g' /home/\$USER/.config/libfm/libfm.conf
# Opérations de déconnexion finales
sed -i 's/Desktop/Bureau/g' /home/\$USER/.config/user-dirs.dirs
rm -f /home/\$USER/.mozilla/firefox/default/.parentlock
sleep 1
killall -u \$USER
sleep 1
umount /media/Classes
sleep 1
umount /media/Partages
sleep 1
umount /home/\$USER
sleep 1
exit 0" > /home/netlogon/clients-linux/conex/deletelinks.sh

echo -e "$COLINFO"
echo "/conex/deletelinks.sh créé ou mis à jour."
echo -e "$COLTXT"

# Création du script de nettoyage du profil à la demande par l'utilisateur
echo -e "#!/bin/bash
# Ce script peut etre lancé par l'utilisateur courant pour nettoyer son profil linux
# Le profil est nettoyé puis la session est fermée automatiquement
cd
rm -rf .adobe .cache .config/lxpanel .config/lxsession .config/pcmanfm .dbus .dmrc .fontconfig .gconf .gconfd .gnome2 .gksu* .gstreamer* .gtk-bookmarks .gvfs .java .kde .local .macromedia .pki .pulse .thumbnails
[ -e /usr/bin/logout1.sh ] && sh /usr/bin/logout1.sh
[ -e /etc/gdm3/PostSession ] && sh /etc/gdm3/PostSession/Default
exit 0" > /home/netlogon/clients-linux/conex/reparer_mon_compte.sh

echo -e "$COLINFO"
echo "/conex/reparer_mon_compte.sh créé ou mis à jour."
echo -e "$COLTXT"

# Téléchargement des scripts à exécuter à la connexion sur les clients
cd $rep4
rm -f profil_ff*
wget --no-cache --no-check-certificate --quiet $ftp/se3/conex/profil_ff.sh
echo -e "$COLINFO"
echo "/conex/profil_ff.sh créé ou mis à jour."
echo -e "$COLTXT"
rm -f alsavolume*
wget --no-cache --no-check-certificate --quiet $ftp/se3/conex/alsavolume.sh
echo -e "$COLINFO"
echo "/conex/alsavolume.sh créé ou mis à jour."
echo -e "$COLTXT"
rm -f mimeapps.list*
wget --no-cache --no-check-certificate --quiet $ftp/se3/conex/mimeapps.list
echo -e "$COLINFO"
echo "/conex/mimeapps.list créé ou mis à jour."
echo -e "$COLTXT"
rm -f clean_vartmp*
wget --no-cache --no-check-certificate --quiet $ftp/se3/conex/clean_vartmp.sh
echo -e "$COLINFO"
echo "/conex/clean_vartmp.sh créé ou mis à jour."
echo -e "$COLTXT"
rm -f message_overfill*
wget --no-cache --no-check-certificate --quiet $ftp/se3/conex/message_overfill.txt
sed -i 's/##se3ip##/'$getse3ip'/g' message_overfill.txt
echo -e "$COLINFO"
echo "/conex/message_overfill.txt créé ou mis à jour."
echo -e "$COLTXT"

echo ""
echo -e "$VERT" "## Tous les scripts de connexion-deconnexion ont été créés. ##"
echo ""
echo -e "$COLTXT"

# Préparation des clés ssh pour les clients linux
echo -e "$COLINFO" "Préparation des clés ssh..."
echo -e "$COLTXT"
# Copie des clés publiques de root et de www-se3 dans /var/www/se3/
# pour que les clients puissent venir les récupérer pendant l'intégration
mv /var/www/se3/authorized_keys /var/www/se3/authorized_keys_sav
cat /root/.ssh/id_rsa.pub > /var/www/se3/authorized_keys
mv /var/www/se3/authorized_keys_wwwse3 /var/www/se3/authorized_keys_wwwse3_sav
cat /var/remote_adm/.ssh/id_rsa.pub > /var/www/se3/authorized_keys_wwwse3

echo -e "$COLINFO"
echo "Clés ssh prêtes!"
echo -e "$COLTXT"
echo ""

# Téléchargement des scripts d'installation, d'intégration et de copie sur le client
cd /root
rm -f *rejoint_autoconfig* se3_copie_script_integration* se3_copie_script_* se3_cree_rep_parc* se3_get_*
wget --no-cache --no-check-certificate --quiet $ftp/se3/se3_rejoint_autoconfig.sh
wget --no-cache --no-check-certificate --quiet $ftp/se3/se3_copie_script_installation.sh
wget --no-cache --no-check-certificate --quiet $ftp/se3/se3_copie_script_integration.sh
wget --no-cache --no-check-certificate --quiet $ftp/se3/se3_copie_script_migration.sh
wget --no-cache --no-check-certificate --quiet $ftp/se3/se3_cree_rep_parc.sh
wget --no-cache --no-check-certificate --quiet $ftp/se3/se3_get_java_clinux.sh
wget --no-cache --no-check-certificate --quiet $ftp/se3/se3_get_scratch2.sh
chmod +x se3_*.sh
cd $rep2
rm -f install_stretch_etabs*
rm -f install_jessie_etabs*
rm -f install_buster_etabs*
rm -f migration_wheezy_jessie_etabs*
wget --no-cache --no-check-certificate --quiet $ftp/stretch/installation/install_stretch_etabs.sh
wget --no-cache --no-check-certificate --quiet $ftp/buster/installation/install_buster_etabs.sh
sed -i 's/##se3ip##/'$getse3ip'/g' $rep2/install_buster_etabs.sh
wget --no-cache --no-check-certificate --quiet $ftp/jessie/installation/install_jessie_etabs_evo.sh
wget --no-cache --no-check-certificate --quiet $ftp/jessie/installation/migration_wheezy_jessie_etabs.sh
cd
echo -e "$VERT"
echo "Scripts d'aide à l'installation et à l'intégration téléchargés."
echo -e "$COLTXT"


# Téléchargement de scripts unefois "indispensables"
cd $rep3
rm -f test_unefois.sh modif_lxde_icons.sh lxde_icons_unefois.sh cfguu_unefois.sh xscreen_unefois.sh mount-a_unefois.sh
rm -f test_unefois* lxde_icons_unefois* config_wol_unefois* cfguu_unefois* corrige_smb_unefois*
wget --no-cache --no-check-certificate --quiet $ftp/se3/once/test_unefois.sh
wget --no-cache --no-check-certificate --quiet $ftp/se3/once/lxde_icons_unefois.sh
wget --no-cache --no-check-certificate --quiet $ftp/se3/once/cfguu_unefois.sh
wget --no-cache --no-check-certificate --quiet $ftp/se3/once/xscreen_unefois.sh
wget --no-cache --no-check-certificate --quiet $ftp/se3/once/mount-a_unefois.sh
wget --no-cache --no-check-certificate --quiet $ftp/se3/once/corrige_smb_unefois.sh

echo -e "$COLINFO"
echo "Création du répertoire /var/se3/Docs/persolinks et des sous-répertoires..."
echo -e "$COLTXT"

# Test de l'existence du répertoire persolinks
rep5=/var/se3/Docs/persolinks/base
if [ ! -e $rep5 ]; then
mkdir -p /var/se3/Docs/persolinks/base
fi

# Création des sous-répertoires de parcs
rep6=/var/se3/Docs/persolinks/cdi
rep7=/var/se3/Docs/persolinks/sprofs

if [ ! -e $rep6 ]; then
mkdir -p /var/se3/Docs/persolinks/cdi
fi
if [ ! -e $rep7 ]; then
mkdir -p /var/se3/Docs/persolinks/sprofs
fi

# Création des sous-répertoires de groupes
rep8=/var/se3/Docs/persolinks/eleves
rep9=/var/se3/Docs/persolinks/profs
rep10=/var/se3/Docs/persolinks/autres

if [ ! -e $rep8 ]; then
mkdir -p /var/se3/Docs/persolinks/eleves
fi
if [ ! -e $rep9 ]; then
mkdir -p /var/se3/Docs/persolinks/profs
fi
if [ ! -e $rep10 ]; then
mkdir -p /var/se3/Docs/persolinks/autres
fi

echo -e "$VERT"
echo "Dossiers de raccourcis créés."
echo -e "$COLTXT"

# Téléchargement des raccourcis pour base
echo -e "$COLINFO"
echo "Téléchargement des raccourcis dans base pour le navigateur et l'interface se3..."
echo -e "$COLTXT"

# Se3.desktop contient l'ip de l'interface du se3, il faut la renseigner
cd $rep5
link1=$rep5/Se3.desktop
rm -f $link1
wget --no-cache --no-check-certificate --quiet $ftp/se3/raccourcis/Se3.desktop
sed -i 's/##se3ip##/'$getse3ip'/g' $link1

echo -e "$COLINFO"
echo "Se3.desktop configuré pour ce se3."
echo -e "$COLTXT"

# Téléchargement des raccourcis usuels
cd $rep5
rm -f iceweasel.desktop
rm -f repare_profil.desktop
rm -f ent*.desktop
wget --no-cache --no-check-certificate --quiet $ftp/se3/raccourcis/iceweasel.desktop
wget --no-cache --no-check-certificate --quiet $ftp/se3/raccourcis/repare_profil.desktop
wget --no-cache --no-check-certificate --quiet $ftp/se3/raccourcis/entlra.desktop
#wget --no-cache --no-check-certificate --quiet $ftp/se3/raccourcis/ent.desktop
cd $rep8
rm -f eleves.desk*
wget --no-cache --no-cache --no-check-certificate --quiet $ftp/se3/raccourcis/eleves.desktop
cd $rep9
rm -f prof.desk*
wget --no-cache --no-cache --no-check-certificate --quiet $ftp/se3/raccourcis/prof.desktop
cd $rep10
rm -f italc-prof*
wget --no-cache --no-cache --no-check-certificate --quiet $ftp/se3/raccourcis/italc-prof.desktop
echo -e "$VERT"
echo "Téléchargement des raccourcis terminé."
echo -e "$COLTXT"
echo -e "$COLINFO"
echo "Ajustement des droits sur les répertoires de raccourcis..."
echo -e "$COLTXT"
cd
chown -R admin:admins /var/se3/Docs/persolinks
chmod -R 755 /var/se3/Docs/persolinks
echo -e "$COLINFO"
echo "Fait."
echo -e "$COLTXT"
#echo -e "$COLDEFAUT"
#echo "Ne pas oublier pas de modifier les acl sur le répertoire persolinks"
#echo "par l'interface du se3."
#echo -e "$COLTXT"
echo -e "$COLINFO"
echo "Rétablissement des droits sur les répertoires..."
echo -e "$COLTXT"
permse3
echo -e "$VERT"
echo "Ce se3 est prêt pour les clients linux !"
echo -e "$COLTXT"
echo -e "$COLINFO"
echo "On télécharge les derniers scripts d'intégration..."
echo -e "$COLTXT"
sh /root/se3_rejoint_autoconfig.sh
# Dépot d'un témoin dans /root
rm -f /root/prep_*
touch /root/prep_$DATE1
echo -e "$VERT"
echo "Terminé !"
echo -e "$COLTXT"
exit 0
