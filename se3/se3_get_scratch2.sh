#!/bin/bash
######################################
# se3_get_scratch2.sh
# Script de récupération des fichiers pour l'installation de scratch2 sur des clients linux debian 64
# Ce script doit être lancé sur le se3
# 20170211
######################################
DATE1=$(date +%F+%0kh%0M)

# Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

ftp=https://raw.githubusercontent.com/jcmousse/clinux/master
ftp2=http://jcmousse.free.fr

# Test de la présence du paquet se3-clients-linux
[ ! -e /home/netlogon/clients-linux ] && echo "Le paquet se3-clients-linux n'est pas installé. Installez le et relancez le script." && exit 1

cd /home/netlogon/clients-linux/alancer
rm -f AdobeAIRInstaller*
rm -f Scratch-*
rm -f install_scratch2*
wget $ftp2/se3/AdobeAIRInstaller.bin
wget $ftp2/se3/Scratch-454.air
wget --no-check-certificate $ftp/commun/autres/scratch2/install_scratch2.sh
echo -e "$COLINFO"
echo "Rétablissement des droits sur les fichiers..."
echo -e "$COLTXT"
permse3
echo -e "$COLDEFAUT"
echo "Les fichiers nécessaires à l'installation de scratch2 sur les clients"
echo "ont été téléchargés."
echo ""
echo "Vous pouvez lancer /mnt/netlogon/alancer/install_scratch2.sh sur chaque client."
echo ""
echo -e "$VERT"
echo "Terminé !"
echo -e "$COLTXT"
