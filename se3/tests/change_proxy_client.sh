#!/bin/bash
# **********************************************************
# 
# **********************************************************
ROUGE="\\033[1;31m"

# On rend le script "cretin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a executer sur les clients Linux, pas sur le serveur !" && exit 1

echo -e "Comment voulez-vous modifier le proxy apt de cette machine ?"
read -p "Choisissez dans cette liste :
1 (1 - AVEC cache apt)
2 (2 - SANS cache apt) : " rep
			case $rep in

				1|cache )
				echo -e "$ROUGE" "Configuration : AVEC cache apt" ; tput sgr0
echo "#Configuration du proxy apt-cache
Acquire::http { Proxy \"http://apt.apt.apt.apt:3142\"; };" > /etc/apt/apt.conf.d/99proxy ;;

				2|nocache )
                                echo -e "$ROUGE" "Configuration : SANS cache apt" ; tput sgr0
echo "#Configuration du proxy sans apt-cache
Acquire::http { Proxy \"http://amon.amon.amon:3128\"; };" > /etc/apt/apt.conf.d/99proxy ;;

				* ) echo -e "$ROUGE" "Erreur - Relancez le script !" ; tput sgr0 ;;
			esac
