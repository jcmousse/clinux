#!/bin/bash
# alsavolume.sh
# script d'inscription de l'icone de réglage du volume audio dans la barre utilisateur
# 20151128
#

# Test de la presence des repertoires lxde
panel1=/home/$USER/.config/lxpanel/LXDE/panels/panel
if [ ! -e "$panel1" ]; then
# le repertoire n existe pas, il faut le creer
mkdir -p /home/$USER/.config/lxpanel/LXDE/panels
cp /mnt/netlogon/conex/panel /home/$USER/.config/lxpanel/LXDE/panels/
else
        TEST_ALSA=$(cat /home/$USER/.config/lxpanel/LXDE/panels/panel | grep volumealsa)
        if [ -n "$TEST_ALSA" ]; then
        exit
        else
        echo "Plugin {
        type = volumealsa
        Config {
        }
}" >> /home/$USER/.config/lxpanel/LXDE/panels/panel
        fi
fi
exit 0
