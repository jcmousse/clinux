#!/bin/bash
#
# warn_overfill.sh
# Script d'avertissement de l'utilisateur en cas de dépassement de quota
# 20161115
#

timestamp=$(date +%F_%N)

# Récupération des variables du se3 ip_se3, base_dn et rne
TEST_ADR=$(ifconfig eth0 | grep "inet adr")
if [ -n "$TEST_ADR" ]; then
# ifconfig contient inet adr
getse3ip=$(ifconfig eth0 | grep inet\ adr | cut -d: -f2 | awk '{ print $1}')
else
# ifconfig contient inet addr
getse3ip=$(ifconfig eth0 | grep inet\ addr | cut -d: -f2 | awk '{ print $1}')
fi
getsuffix=$(cat /etc/samba/smb.conf | grep ldap\ suffix |  cut -c16-)
getrne=$(cat /etc/samba/smb.conf | grep ldap\ suffix |  cut -c19- | cut -c-8)

# Avertissement en cas de dépassement de quota
p=\$(ldapsearch -xLLL -h "$getse3ip" -b \"cn=overfill,ou=Groups,$getsuffix\" \"(memberUid=\$USER)\" \"dn\" | wc -l)
	if [ "\$p" -eq "0" ]; then
        # p=0 signifie que la recherche n a donné aucun résultat, donc le compte n appartient pas au groupe overfill.
	echo \"Pas de dépassement de quota pour ce compte\"
	else
	# Le compte est dans overfill, il faut avertir l'utilisateur
	echo \"Depassement de quota pour \$USER\"
	leafpad /media/Partages/persolinks/overfill.txt
	fi
