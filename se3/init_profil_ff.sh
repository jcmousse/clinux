#!/bin/bash
#
# Script de synchronisation des profils firefox linux <-> windoze de l'utilisateur sur le se3
# Ce script est à lancer en root sur le se3

ls /home|egrep -v "(^_|^netlogon$|^templates$|^aquota.user$|^aquota.group$|^profiles$)"|while read A ; do

if [ ! -e "/home/$A/profil/appdata/Mozilla/Firefox/Profiles/default" ]; then
mkdir -p /home/$A/profil/appdata/Mozilla/Firefox/Profiles/default
fi

if [ ! -e "/home/\$A/.mozilla/firefox/default" ]; then
mkdir -p /home/$A/.mozilla/firefox/default
fi

# On synchronise le contenu du profil FF linux avec celui du profil FF windoze
rsync -rtu /home/$A/profil/appdata/Mozilla/Firefox/Profiles/default/ /home/$A/.mozilla/firefox/default/
#rsync -a /home/$A/.mozilla/firefox/default/ /home/$A/profil/appdata/Mozilla/Firefox/Profiles/default/

# Création ou modification du profiles.ini pour windoze
echo "[General]
StartWithLastProfile=1

[Profile0]
Name=default
IsRelative=1
Path=Profiles/default" > /home/$A/profil/appdata/Mozilla/Firefox/profiles.ini

# Création ou modification du profiles.ini pour linux
echo "[General]
StartWithLastProfile=1

[Profile0]
Name=default
IsRelative=1
Path=default" > /home/$A/.mozilla/firefox/profiles.ini
touch /home/$A/profil/appdata/sync_ff.txt

exit 0
