#!/bin/bash
# Ce script désactive tarcker-miner sur les clients linux

temoin=/root/temoins/temoin.disable_tracker

# Execution du script si le témoin n'existe pas
if [ ! -e $temoin ] ; then
tracker-control -r
sed -i 's/^X-GNOME-Autostart.*/X-GNOME-Autostart-enabled=false/g' /etc/xdg/autostart/tracker-miner-fs.desktop
sed -i 's/^X-GNOME-Autostart.*/X-GNOME-Autostart-enabled=false/g' /etc/xdg/autostart/tracker-store.desktop
echo "tracker-miner disabled" > $temoin
else
exit 0
fi

