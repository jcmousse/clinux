#!/bin/bash
# **********************************************************
# Modifications sur les clients linux apres installation
# d'un cache apt
# v1 du 20150206
# **********************************************************
DATEINST=$(date +%F+%0kh%0M)
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a executer sur les clients Linux, pas sur le serveur !" && exit 1
> /etc/apt/apt.conf
> /etc/apt/apt.conf.d/proxy

echo -e "$ROUGE" "Voulez-vous declarer un cache apt sur cette machine ?" ; tput sgr0
		PS3='Repondre par o ou n: '   # le prompt
		LISTE=("[o] oui" "[n]  non")  # liste de choix disponibles
		select CHOIX in "${LISTE[@]}" ; do
			case $REPLY in
				1|o)
				echo "Nouveau cache apt à déclarer (cool !) "
				APTCACHE=o
				break
				;;
				2|n)
				echo "Pas de cache nouveau cache apt (dommage ;)"
				APTCACHE=n
				break
				;;
			esac
		done

if [ $APTCACHE = "o" ] ; then
	# Definition de l'adresse du proxy apt pour l'installation
	echo -e "$ROUGE" "Vous allez utiliser un cache APT :" ; tput sgr0
		echo "Donnez l'adresse IP et le port du cache APT sous la forme 172.17.139.251:3142"
		read APTPROXY
		echo "L'adresse IP du cache apt est $APTPROXY"
	echo ""
	echo "Ajout du proxy apt à la configuration..."
	# Declaration du proxy pour apt
	echo "#Configuration du proxy apt
Acquire::http { Proxy \"http://$APTPROXY\"; };" > /etc/apt/apt.conf.d/99proxy
fi

echo -e "$JAUNE" "Adresse du proxy (Amon) de l'etablissement où est installé le pc :" ; tput sgr0
		echo ""
		echo "Donnez l'adresse IP et le port du proxy HTTP sous la forme 172.17.139.253:3128"
		read HTTPPROXY
		echo -e "$VERT" "L'adresse IP du cache apt est $HTTPPROXY" ; tput sgr0


if [ $APTCACHE = "n" ] ; then
echo "#Configuration du proxy apt
Acquire::http { Proxy \"http://$HTTPPROXY\"; };" > /etc/apt/apt.conf.d/99proxy
fi


# Declaration du proxy pour apt dans l'etablissement si existant
echo -e "$VERT" "Telechargement du script de mise à jour JRE" ; tput sgr0
cd /root
rm -f maj_java.sh
wget --no-cache http://jcmousse.free.fr/maj_java.sh
if [ $APTCACHE = "o" ] ; then
sed -i 's/172.17.139.253:3128/'$HTTPPROXY'/g' /root/maj_java.sh
sed -i 's/172.17.139.251:3142/'$APTPROXY'/g' /root/maj_java.sh
else
sed -i 's/172.17.139.253:3128/'$HTTPPROXY'/g' /root/maj_java.sh
sed -i 's/172.17.139.251:3142/'$HTTPPROXY'/g' /root/maj_java.sh
fi

# Declaration du proxy http dans /etc/profile
echo ""
echo -e "$VERT" "Configuration du proxy HTTP (amon)" ; tput sgr0
echo "Ajout du proxy HTTP à /etc/profile"
echo ""
sed -i '/export/d' /etc/profile
sed -i '/Configuration/d' /etc/profile
echo "export http_proxy=\"http://$HTTPPROXY\"" >> /etc/profile
echo "export https_proxy=\"https://$HTTPPROXY\"" >> /etc/profile

# Configuration des maj automatiques
echo ">>>> Installation des maj automatiques <<<<<"
dpkg-reconfigure -plow unattended-upgrades
echo "Mises à jour automatiques installées et configurées"

echo -e "$ROUGE" "Vous pouvez maintenant rebooter la machine !" ; tput sgr0
