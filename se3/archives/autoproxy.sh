#!/bin/bash

# Test autocompletion des valeurs proxy et amon

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

ftp=https://raw.githubusercontent.com/jcmousse/clinux/master

fichproxy=proxys.txt
testapt=$(cat /root/$fichproxy | grep ip_cache)
testamon=$(cat /root/$fichproxy | grep ip_amon)

if [ ! -n "$testapt" ] ; then
echo -e "$JAUNE" "testapt faux" ; tput sgr0
echo -e "$COLTITRE"
echo "Y a-t-il un cache apt dans l'etablissement ?"
echo -e "$COLTXT"
		PS3='Repondre par o ou n: '
		LISTE=("[o] oui" "[n]  non")
		select CHOIX in "${LISTE[@]}" ; do
			case $REPLY in
				1|o)
				echo -e "$VERT"
				echo "Il y a un cache apt dans cet etablissement (cool)."
				echo -e "$COLTXT"
				CACHE=o
				break
				;;
				2|n)
				echo -e "$COLINFO"
				echo "Pas de cache apt ici (beurk)."
				echo -e "$COLTXT"
				echo ""
				CACHE=n
				break
				;;
			esac
done
else CACHE=o
fi

	# Definition de l'adresse du cache apt de l'etablissement
if [ $CACHE = "o" ] ; then
	if [ ! -n "$testapt" ] ; then
		echo "Adresse du CACHE APT de l'etablissement :"
		echo -e "$COLINFO"
		echo "Entrez l'adresse IP du cache APT sous la forme 172.17.31.251"
		echo -e "$COLTXT"
		read APTCACHE
		echo "ip_cache=$APTCACHE" >> /home/jc/clinux/se3/tests/proxys.txt
		echo -e "$JAUNE" "ip du cache stockée dans le fichier" ; tput sgr0
		echo -e "$COLINFO"
		echo "L'adresse du CACHE APT est $APTCACHE"
		echo -e "$COLTXT"
		echo ""
	else
		echo -e "$JAUNE" "lecture de la valeur cache depuis le fichier" ; tput sgr0		
		APTCACHE=$(cat /root/proxys.txt | grep ip_cache | cut -d= -f2 | awk '{ print $1}')
		echo -e "$JAUNE" "valeur lue pour le cache : $APTCACHE " ; tput sgr0
	fi
else
	if [ $CACHE = "o" ] ; then
	# On affiche l'adresse ip du cache pour controle"
	echo "Voici l'adresse du cache apt enregistree sur ce serveur :"
	echo "$testapt"	
	echo "Si l'adresse est incorrecte, corrigez /root/proxys.txt"
	echo "Et relancez le script."
	fi
fi

	# Definition de l'adresse du proxy amon de l'etablissement
	if [ ! -n "$testamon" ] ; then
		echo "Adresse du proxy AMON de l'etablissement :"
		echo -e "$COLINFO"
		echo "Entrez l'adresse du proxy Amon de l'etablissement sous la forme 172.17.31.254"
		echo -e "$COLTXT"
		read IPAMON
		echo "ip_amon=$IPAMON" >> /root/proxys.txt
		echo -e "$JAUNE" "ip de amon stockée dans le fichier" ; tput sgr0
		echo -e "$COLINFO"
		echo "L'adresse du proxy Amon est $IPAMON"
		echo -e "$COLTXT"
		echo ""
	else
		echo -e "$JAUNE" "lecture de la valeur amon depuis le fichier" ; tput sgr0
		IPAMON=$(cat /root/proxys.txt | grep ip_amon | cut -d= -f2 | awk '{ print $1}')
		echo -e "$JAUNE" "valeur lue pour l'amon : $IPAMON " ; tput sgr0
	fi
	# On met les valeurs dans les fichiers
		cd $rep2
	if [ -n "$testapt" -o $CACHE = "o" ] ; then	
		rm -f maj_java.*
		wget --no-check-certificate $ftp/se3/alancer/maj_java.sh
		sed -i 's/##ipcache##/'$APTCACHE'/g' $rep2/maj_java.sh
		sed -i 's/##ipamon##/'$IPAMON'/g' $rep2/maj_java.sh
		rm -f change_proxy_client.sh
		wget --no-check-certificate $ftp/se3/alancer/change_proxy_client.sh
		sed -i 's/##ipcache##/'$APTCACHE'/g' $rep2/change_proxy_client.sh
		sed -i 's/##ipamon##/'$IPAMON'/g' $rep2/change_proxy_client.sh
	fi
		echo ""
		echo -e "$JAUNE" "variables remplacées dans les fichiers" ; tput sgr0
		cd

exit 0
