#!/bin/bash
#
#***********************************************
# Corrige une erreur introduite par le script d'intégration
# sur le proxy https déclaré dans /etc/profile
# 20161208
#***********************************************

# Témoin de passage du script sur le client
temoin=/root/temoins/temoin.corrige_https
# Exécution du script unefois
# Ne sera exécuté que si le témoin n'existe pas

if [ ! -e $temoin ] ; then
	sed -i 's/https_proxy=\"https:/https_proxy=\"http:/g' /etc/profile
	echo "/etc/profile a été modifié" > $temoin
	else
	exit 0
fi

