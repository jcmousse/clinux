#!/bin/sh
#
# xscreen_unefois.sh
# installe xscreensaver 5.36 sur les clients jessie et stretch
# 20190209
#
#####################################################
#DATE1=$(date +%F+%0kh%0M)

rm -f /root/temoins/temoin.xscreen_5.34
temoin=/root/temoins/temoin.xscreen_5.36

# Test de la version dy système
if [ ! -e $temoin ] ; then
# Test de la version Debian du client
TEST_1=$(cat /etc/debian_version | grep "buster")

if [ -n "$TEST_1" ]; then
echo "buster"
else
	TEST_2=$(cat /etc/debian_version | grep "8.")
	if [ -n "$TEST_2" ]; then
	echo "jessie"
	else
	echo "stretch"
	fi
fi

# Installation (ou pas)
if [ ! -n "$TEST_1" ]; then
	TEST_VERSION=$(uname -a | grep "i686")
	if [ ! -n "$TEST_VERSION" ]; then
	dpkg -i /mnt/netlogon/alancer/xscreensaver_5.36-1_amd64.deb
	echo "64 bits"
	else
	dpkg -i /mnt/netlogon/alancer/xscreensaver_5.36-1_i386.deb
	echo "32 bits"
	fi
else	
	echo "version Xscreensaver correcte."
	exit 0
fi
fi
echo "fait" > $temoin
exit 0
