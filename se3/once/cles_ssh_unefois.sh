#!/bin/bash
# Correctif d'importation des clés ssh publiques des se3 sur les clients
#
#####################################################
DATE1=$(date +%F+%0kh%0M)
DATERAPPORT=$(date +%F+%0kh%0M)

temoin=/root/temoins/temoin.cles_ssh_unefois

# On rend le script "cretin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a executer sur les clients Linux, pas sur le serveur !" && exit 1

# Execution du script unefois
if [ ! -e $temoin ] ; then
# Recuperation des cles ssh depuis le se3
mkdir -p /root/.ssh
chmod 700 /root/.ssh
cd /root/.ssh

SE3_IP=##se3ip##
if [ -f "authorized_keys" ];then
wget -o log_recuperation_cle_pub_se3_$DATERAPPORT -O authorized_keys_se3 $SE3_IP:909/authorized_keys
cat authorized_keys_se3 >> authorized_keys
wget -o log_recuperation_cle_pub_wwwse3_$DATERAPPORT -O authorized_keys_wwwse3 $SE3_IP:909/authorized_keys_wwwse3
cat authorized_keys_wwwse3 >> authorized_keys
else
wget -o log_recuperation_cle_pub_se3_$DATERAPPORT -O authorized_keys $SE3_IP:909/authorized_keys
wget -o log_recuperation_cle_pub_wwwse3_$DATERAPPORT -O authorized_keys_wwwse3 $SE3_IP:909/authorized_keys_wwwse3
cat authorized_keys_wwwse3 >> authorized_keys
fi
chmod 400 /root/.ssh/authorized_keys
cd

echo "Clés ssh se3 importées" > $temoin
fi
