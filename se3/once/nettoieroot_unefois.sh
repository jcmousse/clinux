#!/bin/bash
# Nettoyage du répertoire /root
# script unefois

DATE1=$(date +%F+%0kh%0M)
temoin=/root/temoins/temoin.nettoieroot

[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a executer sur les clients Linux, pas sur le serveur !" && exit 1

# Execution du script unefois
if [ ! -e $temoin ] ; then
rm -f /root/changesources.sh
rm -f /root/config_wol.sh
rm -f /root/correctif_netlogon_v5*
rm -f /root/correctifs_clients_linux_lra_2.sh
rm -f /root/correctifs_post_install_clients_linux.sh
rm -f /root/correctif_v2*
rm -f /root/install_fusion_agent_v2.sh
rm -f /root/install_wheezy_etab*
rm -f /root/monter_netlogon.sh
rm -f /root/netlogon_*
rm -f /root/rejoint_se3_*
rm -f /root/SE3_rapport*
rm -f /root/renommage_*

echo "nettoyage de /root effectué le $DATE1" > $temoin
fi

