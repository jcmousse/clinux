#!/bin/sh
#
# installrsync_unefois
# installe rsync sur le client si absent
#
#####################################################
#DATE1=$(date +%F+%0kh%0M)

temoin=/root/temoins/temoin.rsyncunefois

# Execution du script unefois
if [ ! -e $temoin ] ; then
	if [ ! -e /usr/bin/rsync ] ; then
	apt-get update
	apt-get install -y rsync
	else
	echo "rien" > /dev/null
	fi
echo "rsync installé" > $temoin
fi

