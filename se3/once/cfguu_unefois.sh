#!/bin/bash
#
#***********************************************
# cfguu_unefois.sh
# Configuration des maj auto avec unattended-upgrades
# avec test sur le système
# 20190209
#***********************************************

# Témoin de passage du script sur le client
rm -f /root/temoins/temoin.cfguu*
temoin=/root/temoins/temoin.cfguu

# Exécution du script unefois
# Ne sera exécuté que si le témoin n'existe pas

if [ ! -e $temoin ] ; then
	cp /usr/share/unattended-upgrades/20auto-upgrades /etc/apt/apt.conf.d/20auto-upgrades
	cd /etc/apt/apt.conf.d
	rm -f 50unattended-upgrades

# Test de la version Debian du client
	TEST_1=$(cat /etc/debian_version | grep "buster")
	if [ -n "$TEST_1" ]; then
	echo "buster"
	echo -e "// Automatically upgrade packages from these origin patterns
Unattended-Upgrade::Origins-Pattern {
        // Archive or Suite based matching:
        // Note that this will silently match a different release after
        // migration to the specified archive (e.g. buster becomes the
        // new stable).
	\"o=Debian,a=buster\";
	\"o=Debian,a=buster-updates\";
	//\"o=Debian,a=buster-backports\";
	//\"o=Debian,a=proposed-updates\";
	\"origin=Debian,archive=stretch,label=Debian-Security\";
	//\"origin=Debian,archive=oldstable,label=Debian-Security\";
	\"origin=Unofficial Multimedia Packages,archive=buster,n=stretch,label=Unofficial Multimedia Packages\";
	//\"o=www.geogebra.net,n=buster,l=apt repository,c=main\";
};

// List of packages to not update
Unattended-Upgrade::Package-Blacklist {
//	\"vim\";
//	\"libc6\";
//	\"libc6-dev\";
//	\"libc6-i686\";
};
// Do automatic removal of new unused dependencies after the upgrade
// (equivalent to apt-get autoremove)
Unattended-Upgrade::Remove-Unused-Dependencies \"true\";" > /etc/apt/apt.conf.d/50unattended-upgrades
echo "buster" > $temoin
	else
	TEST_2=$(cat /etc/debian_version | grep "8.")
		if [ -n "$TEST_2" ]; then
echo "jessie"
echo -e "// Automatically upgrade packages from these origin patterns
Unattended-Upgrade::Origins-Pattern {
        // Archive or Suite based matching:
        // Note that this will silently match a different release after
        // migration to the specified archive (e.g. testing becomes the
        // new stable).
	\"o=Debian,a=jessie\";
	\"o=Debian,a=jessie-updates\";
	\"o=Debian,a=jessie-backports\";
	\"o=Debian,a=proposed-updates\";
	\"origin=Debian,archive=jessie,label=Debian-Security\";
	\"origin=Debian,archive=jessie,label=Debian-Security\";
	\"origin=Unofficial Multimedia Packages,archive=jessie,label=Unofficial Multimedia Packages\";
        \"o=www.geogebra.net,n=stable,l=apt repository,c=main\";
};

// List of packages to not update
Unattended-Upgrade::Package-Blacklist {
//	\"vim\";
//	\"libc6\";
//	\"libc6-dev\";
//	\"libc6-i686\";
};
// Do automatic removal of new unused dependencies after the upgrade
// (equivalent to apt-get autoremove)
Unattended-Upgrade::Remove-Unused-Dependencies \"true\";" > /etc/apt/apt.conf.d/50unattended-upgrades
echo "jessie" > $temoin

		else

echo "stretch"
echo -e "// Automatically upgrade packages from these origin patterns
Unattended-Upgrade::Origins-Pattern {
        // Archive or Suite based matching:
        // Note that this will silently match a different release after
        // migration to the specified archive (e.g. testing becomes the
        // new stable).
	\"o=Debian,a=stretch\";
	\"o=Debian,a=stretch-updates\";
	\"o=Debian,a=stretch-backports\";
	\"o=Debian,a=proposed-updates\";
	\"origin=Debian,archive=stretch,label=Debian-Security\";
	\"origin=Debian,archive=jessie,label=Debian-Security\";
	\"origin=Unofficial Multimedia Packages,archive=stretch,n=stretch,label=Unofficial Multimedia Packages\";
	\"o=www.geogebra.net,n=stretch,l=apt repository,c=main\";
};

// List of packages to not update
Unattended-Upgrade::Package-Blacklist {
//	\"vim\";
//	\"libc6\";
//	\"libc6-dev\";
//	\"libc6-i686\";
};
// Do automatic removal of new unused dependencies after the upgrade
// (equivalent to apt-get autoremove)
Unattended-Upgrade::Remove-Unused-Dependencies \"true\";" > /etc/apt/apt.conf.d/50unattended-upgrades
echo "stretch" > $temoin
		fi
	fi
fi

