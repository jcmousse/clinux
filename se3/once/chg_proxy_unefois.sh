#!/bin/sh
#
# Positionne le proxy apt sur le client
#
#####################################################
DATE1=$(date +%F+%0kh%0M)

rm -f /root/temoins/temoin.chgproxy1
temoin=/root/temoins/temoin.chgproxy2

# Execution du script unefois
if [ ! -e $temoin ] ; then
echo "#Configuration du proxy pour l'établissement
Acquire::http { Proxy \"http://##ipcache##:3142\"; };" > /etc/apt/apt.conf.d/99proxy
echo "Cache apt configuré le $DATE1" > $temoin
fi

