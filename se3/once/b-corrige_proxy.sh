#!/bin/bash
#
#***********************************************
# corrige_proxy.sh
# Correction du proxy dans /etc/profile
# 20151212
#***********************************************

# Témoin de passage du script sur le client
temoin=/root/temoins/temoin.corrige_proxy

# Exécution du script unefois
# Ne sera exécuté que si le témoin n'existe pas

if [ ! -e $temoin ] ; then
	sed -i 's/254\"/254:3128\"/g' /etc/profile
	sed -i 's/126\"/126:3128\"/g' /etc/profile
	echo "Sasséfait" > $temoin
	else
	echo "rien"
	exit
fi

