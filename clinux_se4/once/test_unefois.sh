#!/bin/bash
#
#***********************************************
# test_unefois.sh
# Peur servir de modèle pour les scripts unefois ou chaquefois
# 20191127
#***********************************************

# Témoin de passage du script sur le client
# Si c'est un script chaquefois, supprimer le test sur le temoin.

nom=test_unefois # Mettre ici le nom du script
temoin=/root/temoins/temoin.$nom

# Exécution du script unefois
# Ne sera exécuté que si le témoin n'existe pas

if [ ! -e $temoin ] ; then
	# apt install bonnehumeur
        echo "Sasséfait" > $temoin
else
        exit 0
fi
