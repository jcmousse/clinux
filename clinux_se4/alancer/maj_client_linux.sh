#!/bin/bash
# **********************************************************
# maj_client_linux.sh
# proxy avec adresse et port quelconques
# Script de maj des clients linux 8, 9 ou 10
# 20191221
# **********************************************************

DATE1=$(date +%F+%0kh%0M)

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

CHANGESOURCES=0 # Passer à 1 si les sources doivent être modifies

# On rend le script "cretin-resistant"
[ -e /var/sambaedu/Classes ] && echo "Malheureux... Ce script est a executer sur les clients Linux, pas sur le serveur !" && exit 1

# On désactive le cache dans la conf apt
# au cas où le proxy est resté, pour éviter les erreurs d'update
> /etc/apt/apt.conf
> /etc/apt/apt.conf.d/99proxy
echo "Sans cache" > /etc/monproxy

TEST_1=$(cat /etc/debian_version | grep "10.")
[ -n "$TEST_1" ] && distro=buster

TEST_2=$(cat /etc/debian_version | grep "8.")
[ -n "$TEST_2" ] && distro=jessie

TEST_3=$(cat /etc/debian_version | grep "9.")
[ -n "$TEST_3" ] && distro=stretch


# Affichage de la version détectée pour validation

	if [ "$distro" == "buster" ]; then
echo ""
echo -e "$JAUNE"
echo "Vous mettez à jour un client Debian 10 (buster)."
echo -e "$COLINFO"
echo "Peut-on poursuivre? Appuyez sur <entrée>"
echo -e "$COLTXT"
echo ""
read lili

	elif [ "$distro" == "jessie" ]; then
echo ""
echo -e "$JAUNE"
echo "Vous mettez à jour un client Debian 8 (Jessie)."
# Neutralisation sources geogebra (sources obsolètes)
> /etc/apt/sources.list.d/geogebra.list
echo -e "$COLINFO"
echo "Peut-on poursuivre? Appuyez sur <entrée>"
echo -e "$COLTXT"
echo ""
read lulu

	elif [ "$distro" == "stretch" ]; then
echo ""
echo -e "$JAUNE"
echo "Vous mettez à jour un client Debian 9 (Stretch)."
echo -e "$COLINFO"
echo "Peut-on poursuivre? Appuyez sur <entrée>"
echo -e "$COLTXT"
echo ""
read lala
	fi
#===================================================================
# Veut-on corriger les sources ?
if [ "$CHANGESOURCES" == "1" ]; then

echo ""
echo -e "$JAUNE" "Ce script peut modifier automatiquement /etc/apt/sources.list" ; tput sgr0
echo -e "$JAUNE" "pour éviter les erreurs de mise à jour." ; tput sgr0
echo -e "$COLINFO" "le fichier sources.list actuel sera sauvegardé sous /etc/apt/sources.list_$DATE1" ; tput sgr0
echo ""
echo -e "$COLDEFAUT" "Voici le contenu du sources.list actuel :" ; tput sgr0
echo ""
cat /etc/apt/sources.list
echo ""

echo -e "$COLDEFAUT" "Voulez-vous modifier le fichier sources.list ? [o/N] " ; tput sgr0
read replist
	if [ "$replist" = "o" -o "$replist" = "oui" ]; then
echo ""
echo -e "$COLINFO" "Modification du fichier sources.liste demandée." ; tput sgr0
echo ""
LIST=o
	else
echo ""
echo -e "$COLINFO" "Pas de modification du fichier sources.list." ; tput sgr0
echo ""
	fi

# Modification du sources.list demandée
if [[ $LIST = "o" ]] ; then
	
	# On sauvegarde sources.list
echo -e "$COLINFO" "Sauvegarde du fichier sources.list actuel..." ; tput sgr0
cp /etc/apt/sources.list /etc/apt/sources.list_$DATE1

	# Changement des sources en fonction de la version
	if [ "$distro" == "buster" ]; then	
	# sources.list buster
echo ""
echo -e "$VERT" "Mise à jour des sources pour Buster." ; tput sgr0
echo ""
echo "deb http://deb.debian.org/debian/ buster main contrib non-free
#deb-src http://deb.debian.org/debian/ buster main

deb http://security.debian.org/debian-security buster/updates main contrib non-free
#deb-src http://security.debian.org/debian-security buster/updates main contrib non-free" > /etc/apt/sources.list

	elif [ "$distro" == "jessie" ]; then
	# sources.list jessie
echo ""
echo -e "$VERT" "Mise à jour des sources pour Jessie." ; tput sgr0
echo ""
echo "deb http://deb.debian.org/debian jessie main contrib non-free
#deb-src http://deb.debian.org/debian jessie main contrib non-free

deb http://security-cdn.debian.org/debian-security/ jessie/updates main contrib non-free
#deb-src http://deb.debian.org/debian-security/ jessie/updates main contrib non-free" > /etc/apt/sources.list

	elif [ "$distro" == "stretch" ]; then
	# sources.list stretch
echo ""
echo -e "$VERT" "Mise à jour des sources pour Stretch." ; tput sgr0
echo ""
echo "deb http://deb.debian.org/debian stretch main contrib non-free
#deb-src http://deb.debian.org/debian stretch main contrib non-free

deb http://security-cdn.debian.org/debian-security/ stretch/updates main contrib non-free

deb http://deb.debian.org/debian stretch-updates main contrib non-free
#deb-src http://deb.debian.org/debian stretch-updates main contrib non-free" > /etc/apt/sources.list
	fi

# Mise à jour des paquets après la modification des sources
echo ""
echo -e "$COLINFO" "Sources modifiées, mise à jour des paquets..." ; tput sgr0
echo ""
apt-get update
echo ""
echo -e "$COLINFO" "La modification des sources est terminée, on poursuit." ; tput sgr0
echo ""

else
echo -e "$COLINFO" "Pas de changement de /etc/apt/sources.list" ; tput sgr0
echo -e "$COLINFO" "============================================================" ; tput sgr0
echo ""
	fi
fi

#===================================================================
if [ ! -e "/etc/apt/sources.list.d/deb-multimedia.list" ]; then
# Inclure les sources deb-multimedia ?
echo ""
echo -e "$JAUNE" "Ce script peut ajouter les sources du depot deb-multimedia" ; tput sgr0
echo -e "$JAUNE" "pour ajouter des paquets audio et video." ; tput sgr0
echo ""

echo -e "$COLINFO" "Voulez-vous ajouter /etc/apt/sources.list.d/deb-multimedia.list ? [o/n]" ; tput sgr0
read deblist
	if [ "$deblist" = "o" -o "$deblist" = "oui" ]; then
echo ""
echo -e "$COLINFO" "Ajout des sources deb-multimedia." ; tput sgr0
echo ""
DLIST=o
	else
echo ""
echo -e "$COLINFO" "Pas d'ajout des sources deb-multimedia." ; tput sgr0
echo ""
	fi
#===================================================================
# Ajout des sources deb-multimedia en fonction de la version du client
	if [ "$DLIST" = "o" ] ; then
	
	if [ "$distro" == "buster" ]; then	
	# Buster
echo ""
echo -e "$VERT" "Ajout des sources deb-multimedia pour Buster." ; tput sgr0
echo ""
echo "# Deb-multimedia
deb http://www.deb-multimedia.org buster main non-free" > /etc/apt/sources.list.d/deb-multimedia.list

	elif [ "$distro" == "jessie" ]; then
	# Jessie
echo ""
echo -e "$VERT" "Ajout des sources deb-multimedia pour Jessie." ; tput sgr0
echo ""
echo "# Deb-multimedia
deb http://www.deb-multimedia.org jessie main non-free" > /etc/apt/sources.list.d/deb-multimedia.list

	elif [ "$distro" == "stretch" ]; then
	# Stretch
echo ""
echo -e "$VERT" "Ajout des sources deb-multimedia pour Stretch." ; tput sgr0
echo ""
echo "# Deb-multimedia
deb http://www.deb-multimedia.org stretch main non-free" > /etc/apt/sources.list.d/deb-multimedia.list
	fi

# Desactivation du cache et update
> /etc/apt/apt.conf.d/99proxy
echo "Sans cache" > /etc/monproxy
apt update -o Acquire::AllowInsecureRepositories=true && apt install -y --allow-unauthenticated deb-multimedia-keyring

	fi
fi

#===================================================================
# Utilisation d'un cache apt
	# Affichage de la conf du cache apt sur le client
monprox=$(cat /etc/monproxy)

	# Choix cache
echo -e "$JAUNE" "Voulez-vous utiliser un cache APT pour les mises à jour ?" ; tput sgr0
echo -e "$COLINFO" "Configuration actuelle : $monprox" ; tput sgr0
echo ""
		PS3='Repondre par o ou n: '   # le prompt
		LISTE=("[o] oui" "[n]  non")  # liste de choix disponibles
		select CHOIX in "${LISTE[@]}" ; do
			case $REPLY in
				1|o)
				echo ""
				echo -e "$COLINFO" "Cache apt activé pour les màj (cool!)" ; tput sgr0
				echo ""
				APTCACHE=o
				break
				;;
				2|n)
				echo ""
				echo -e "$COLINFO" "Cache apt désactivé pour les màj (dommage ;)" ; tput sgr0
				echo ""
				APTCACHE=n
				break
				;;
			esac
		done

# Nettoyage du cache du client avant la maj, sans cache apt
echo -e "$COLINFO"
echo "Petit nettoyage préalable... Patientez un peu."
echo -e "$COLTXT"
> /etc/apt/apt.conf.d/99proxy
echo "Sans cache" > /etc/monproxy

apt update && apt autoclean && apt clean && apt update
echo -e "$COLINFO"
echo "Petit nettoyage terminé."
echo -e "$COLTXT"

if [ $APTCACHE = "o" ] ; then
# Cache activé dans la conf apt, témoin positionné
echo "#Configuration du proxy pour l'établissement
Acquire::http::Proxy::download.virtualbox.org "DIRECT";
Acquire::http { Proxy \"##ipcache##\"; };" > /etc/apt/apt.conf.d/99proxy
# Ecriture du témoin d'état du cache sur le client
echo "Avec cache" > /etc/monproxy
else
# Cache désactivé dans la conf apt, témoin positionné
echo "Neutralisation du proxy apt dans la configuration..."
> /etc/apt/apt.conf.d/99proxy
# Ecriture du témoin d'état du cache sur le client
echo "Sans cache" > /etc/monproxy
fi

# On règle les problèmes de mise à jour automatiques inachevées...
echo -e "$COLINFO"
echo "Il y a peut-être des màj inachevées..."
echo -e "$COLTXT"
apt-get update
apt-get install -f -y

#===================================================================
# Mise à jour pour deb-multimedia
if [ -e "/etc/apt/sources.list.d/deb-multimedia.list" ]; then
apt update -o Acquire::AllowInsecureRepositories=true && apt install -y --allow-unauthenticated deb-multimedia-keyring
# Il y a parfois des problèmes avec les màj de vlc
echo -e "$COLINFO"
echo "Mise à jour de vlc..."
echo -e "$COLTXT"
apt-get install -y vlc vlc-data

# On met à jour les lecteurs flash
echo -e "$COLINFO"
echo "Mise à jour des lecteurs flash pour firefox et chromium..."
echo -e "$COLTXT"
apt-get install -y flashplayer-mozilla flashplayer-chromium
fi
#===================================================================

# Mise à jour du client
echo -e "$COLINFO"
echo "Mise à jour du client..."
echo -e "$COLTXT"
apt-get upgrade -y && apt-get dist-upgrade -y
echo -e "$COLINFO"
echo "Mise à jour effectuée."
echo ""
echo -e "$COLTXT"

# On désactive le cache dans la conf apt
echo ""
echo -e "$COLINFO"
echo "Neutralisation du proxy apt dans la configuration..."
echo -e "$COLTXT"
> /etc/apt/apt.conf.d/99proxy
# Ecriture du témoin d'état du cache sur le client
echo "Sans cache" > /etc/monproxy

# Nettoyage du poste et rechargement de la liste des paquets
echo ""
echo -e "$COLINFO"
echo "On fait un peu de ménage..."
echo -e "$COLTXT"
apt-get autoremove -y && apt-get autoclean && apt-get update

# Messages de fin
echo -e "$VERT"
echo "Terminé !"
echo -e "$COLTXT"
