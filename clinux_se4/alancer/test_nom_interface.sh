#!/bin/bash
# **********
# Détermination du nom de l'interface réseau dans /etc/network/interfaces

oldnet1=$(cat /etc/network/interfaces | grep dhcp | grep enp | cut -d' ' -f2 | awk '{ print $1}')
echo "pas enp"
if [ ! -n "$oldnet1" ]; then
echo "pas enp"
oldnet1=$(cat /etc/network/interfaces | grep dhcp | grep eno | cut -d' ' -f2 | awk '{ print $1}')
elif [ ! -n "$oldnet1" ]; then
echo "pas eno"
oldnet1=$(cat /etc/network/interfaces | grep dhcp | grep eth | cut -d' ' -f2 | awk '{ print $1}')
fi
echo "$oldnet1"
