#!/bin/bash

# Script a lancer sur le se4fs
# Crée les homes de tous les utilisateurs de l'annuaire
# 20190816

cd /usr/share/sambaedu/shares/shares.avail/
for i in $(wbinfo -u | grep -v 'guest\|adminse4\|admin\|www-sambaedu\|krbtgt\|administrator')
do
if [ ! -e /home/$i ]; then
bash mkhome.sh $i
echo "/home/$i créé"
fi
done

echo ""
echo "Terminé!"
echo ""

