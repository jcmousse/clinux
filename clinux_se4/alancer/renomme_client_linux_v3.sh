#!/bin/bash
#
# 
#*********************************************************************
# client linux se4
# renomme_client_linux_v3.sh
# Ce script permet de renommer proprement un client linux
# Correction de l'uuid de la partition swap après clonage en fin de script
#*********************************************************************
# Section a completer
DATESCRIPT="20191127"

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

DOM=##domain##

# On rend le script "cretin-resistant"
[ -e /var/sambaedu/Classes ] && echo "Malheureux... Ce script est a exécuter sur les clients Linux, pas sur le serveur !" && exit 1

echo -e "$COLINFO"
echo "Renommage du client..."
echo -e "$COLTXT"

echo -e "$COLDEFAUT"
echo "Nom actuel de la machine : $(hostname)"
echo -e "$COLTXT"

# Choix du nouveau nom de la machine
		echo "Choix du nouveau nom de cette machine :"
		echo ""
		echo "Quel nom choisissez-vous pour cette machine ?"
		read NOMMACH
		echo "Le nom choisi pour la machine est $NOMMACH"

# Recuperation de la date et de l'heure pour la sauvegarde des fichiers
DATE=$(date +%F+%0kh%0M)

# Configuration du fichier /etc/hosts
echo -e "$COLINFO"
echo "Configuration du fichier /etc/hosts"
echo -e "$COLTXT"

ANCIENNOM=$(hostname)
cp /etc/hosts /etc/hosts_sauve_$DATE

#sed -i 's/^127.0.1.1.*/127.0.1.1\	'"$NOMMACH"'.'"$DOM"'\	'"$NOMMACH"'/g' /etc/hosts
sed -i 's/'"$ANCIENNOM"'/'"$NOMMACH"'/g' /etc/hosts
cp /etc/hostname /etc/hostname_sauve_$DATE
echo "$NOMMACH" > /etc/hostname

# Datage du script (temoin de passage dans /root)
cd /root
rm -f renommage_*
echo "Script de renommage version $VERSION du $DATESCRIPT passe le $DATE" >> /root/renommage_$VERSION_$DATESCRIPT

# Si la machine a été clonée, il faut corriger l'uuid de la partition swap
echo -e "$COLINFO"
echo "Correction de l'uuid de la partition swap après clonage..."
echo -e "$COLTXT"
# On recupère l'uuid correct
blkid | grep 'swap' | cut -d= -f2 | awk '{ print $1}' > /etc/swapuuid
sed -i 's/"//g' /etc/swapuuid
swuid=$(cat /etc/swapuuid)
echo "identifiant correct : $swuid"

# On recupere l'uuid contenu dans /etc/fstab
cat /etc/fstab | grep 'swap' | cut -d= -f2 | awk '{ print $1}' > /etc/wronguuid
sed -i 's/#//g' /etc/wronguuid
sed -i '/^$/d' /etc/wronguuid
wrong=$(cat /etc/wronguuid)
echo "Identifiant à corriger : $wrong"
echo "Contenu de resume : $(cat /etc/initramfs-tools/conf.d/resume)"

# On remplace par la bonne valeur
sed -i 's/'"$wrong"'/'"$swuid"'/g' /etc/fstab
echo "RESUME=UUID=$swuid" > /etc/initramfs-tools/conf.d/resume

echo -e "$COLINFO"
echo "On lance update-initramfs..."
echo -e "$COLTXT"
update-initramfs -u
echo -e "$COLDEFAUT"
echo "Correction uuid swap terminée !"
echo -e "$COLTXT"

# Fin de la configuration
echo -e "$VERT"
echo "Fin de l'opération."
echo "Il faut redemarrer la machine pour prendre en compte le nouveau nom !"
echo -e "$COLTXT"

