#!/bin/bash
#
# change_parc.sh
#######################################################
# Script permettant de changer de parc un client linux
#######################################################

DATESCRIPT="20191115"

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# Répertoire de base
dirclinux=/var/sambaedu/clients-linux

# On rend le script "cretin-resistant"
[ -e /var/sambaedu/clients-linux ] && echo "Malheureux... Ce script est a exécuter sur les clients Linux, pas sur le serveur !" && exit 1

echo -e "$COLINFO"
echo "Changement de parc de ce client..."
echo -e "$COLTXT"
ancienparc=$(cat /etc/parc.txt)

# Affichage du parc actuel du client
echo -e "$COLDEFAUT"
echo "Parc actuel de la machine : $ancienparc"
echo -e "$COLTXT"

# Choix et inscription du nouveau parc
		echo -e "$COLINFO"
		echo "Choix du nouveau parc de cette machine :"
		echo ""
		echo -e "$COLDEFAUT"
		echo "Quel parc choisissez-vous pour cette machine ?"
		echo -e "$COLTXT"
		read nouvoparc
		echo -e "$COLINFO"
		echo "Le nouveau parc choisi pour la machine est $nouvoparc"
		echo -e "$COLTXT"


cp /etc/parc.txt /etc/parc_$DATE
echo "$nouvoparc" > /etc/parc.txt

# Information
echo -e "$COLDEFAUT"
echo ""
echo "Si vous ne l'avez pas encore fait, pensez à créer un dossier"
echo "$dirclinux/persolinks/$nouvoparc sur le se4fs."
echo "Vous pourrez y déposer vos raccourcis."
echo ""
echo -e "$COLTXT"

# Fin de la configuration
echo -e "$VERT"
echo "Fin de l'opération."
echo "Le nouveau parc sera pris en compte à la prochaine connexion d'un utilisateur !"
echo -e "$COLTXT"

