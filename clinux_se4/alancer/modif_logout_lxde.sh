#!/bin/bash

# ****************************************************************************
# modif_logout_lxde.sh
# Script de modification du dialogue de sortie sur les clients linux lxde
# Suppression des boutons inutiles (ou nuisibles)
# 20191115
# ****************************************************************************

DATESCRIPT="20191115"
DATE1=$(date +%F+%0kh%0M)
FTP=https://gitlab.com/jcmousse/clinux/raw/master
FTP2=http://jcmousse.free.fr

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# On rend le script "crétin-resistant"
[ -e /var/sambaedu/clients-linux ] && echo "Malheureux... Ce script est a exécuter sur un client Linux, pas sur le serveur !" && exit 1

# Test de la version du client
TEST_VERSION=$(cat /etc/debian_version | grep "10.")
if [ ! -n "$TEST_VERSION" ]; then
echo "Ce script est destiné à un système Debian 10 (Buster)."
echo "Il n'est pas adapté à ce système."
exit 0
else
echo "Debian 10 (Buster) détectée... on poursuit."
echo ""
fi

echo -e "$VERT" "Modification de lxsession-logout" ; tput sgr0
echo "Récupération des sources..."
cd
wget --no-check-certificate $FTP2/buster/lxsession-0.5.4.tar.xz
echo -e "$VERT" "Décompression de l'archive" ; tput sgr0
tar -Jxvf lxsession-0.5.4.tar.xz

echo -e "$VERT" "Modification de lxsession-logout.c" ; tput sgr0
cd /root/lxsession-0.5.4/lxsession-logout/
rm -f lxsession-logout.c
wget --no-check-certificate $FTP2/buster/lxsession-logout.c

echo -e "$VERT" "Installation des outils de compilation" ; tput sgr0
apt-get install --assume-yes gcc intltool pkg-config xorg-dev glib-2.0 libdbus-glib-1-dev libspice-client-gtk-3.0-dev libpolkit-agent-1-dev valac libunique-dev
echo -e "$VERT" "Installation et compilation des modifications" ; tput sgr0
cd /root/lxsession-0.5.4
./configure
make && make install
rm -rf /root/lxsession*

# Messages de fin
echo ""
echo -e "$VERT" "La configuration de la fermeture de session est terminée." ; tput sgr0
echo ""
echo -e "$JAUNE" "La machine redémarrera dans 10 s.... " ; tput sgr0
echo -e "$COLINFO" "(ctrl-c si vous voulez annuler le reboot automatique)" ; tput sgr0
sleep 10
reboot
