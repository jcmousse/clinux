#!/bin/bash

# ****************************************************************
# se4_prepare_clients_linux.sh
# Script principal de préparation des scripts pour les clients linux
# (stretch et buster) sur un domaine se4
# Repertoire de base : /var/sambaedu/clients-linux
# A lancer sur le se4fs
# 20191213
# **************************************************************

# source
. /usr/share/sambaedu/includes/config.inc.sh

DATE1=$(date +%F+%0kh%0M)

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# Protection pour les distraits...
testse4fs=$(hostname | grep se4fs)
[ -z "$testse4fs" ] && echo "Malheureux... Ce script est a executer sur le se4fs, pas sur un client ! (ou vous n'avez pas encore créé les classes sur le se4fs)" && exit 1

ftp=https://gitlab.com/jcmousse/clinux/raw/master/clinux_se4

# Fichier des paramètres de configuration sambaedu
confse4=/etc/sambaedu/sambaedu.conf

# Options de debogage
NODL="no" # "yes" = pas de controle de maj du script
DEBUG="no" # "yes" = config des proxy et apt-cache non executees

# Lecture des parametres sambaedu.conf*
get_config

# affectation des params aux variables pour traitement
IPPART=$config_se4fs_ip
IPDNS=$config_se4ad_ip
DOM1=$config_samba_domain
DOM2=$config_domain
NOMFS=$config_se4fs_name
NOMAD=$config_se4ad_name
AMON=$config_http_proxy
ACACHE=$config_apt_proxy

echo -e "$VERT"
echo "Paramètres système récupérés."
echo ""
echo -e "$COLTXT"

#----------------------------------
# Fonction test de la connexion
LINE_TEST()
{
if ( ! wget -q --output-document=/dev/null 'https://lemonde.fr/index.html') ; then
	echo -e "$ROUGE"
	echo "Le se4fs ne parvient pas à accéder à internet."
	echo "Votre connexion internet ne semble pas fonctionnelle !"
	echo -e "$COLTXT"
	exit 1
fi
}

#----------------------------------
# Répertoire de base des outils clients linux
dirclinux=/var/sambaedu/clients-linux

# Répertoire de telechargement des scripts clients linux
webclinux=/var/sambaedu/unattended/install/os/clinux

###################################################################
# Verification et téléchargement de la dernière version du script #
###################################################################

SCRIPT_FILE="se4_prepare_clients_linux.sh"
SCRIPT_FILE_MD5="se4_prepare_clients_linux.md5"
SCRIPTS_DIR=$dirclinux

# Test de l'accès internet
LINE_TEST

# Preparation initiale, si on installe la machine
if [ ! -e $SCRIPTS_DIR ]; then
	echo -e "$COLINFO"
	echo "Préparation du répertoire principal..."
	echo -e "$COLTXT"
	mkdir -p $dirclinux
	mkdir -p $webclinux
	cd $SCRIPTS_DIR
	echo -e "$COLINFO"
	echo "Téléchargement initial des scripts..."
	echo -e "$COLTXT"
	wget --no-check-certificate --no-cache -N --tries=5 --connect-timeout=10 $ftp/$SCRIPT_FILE || ERREUR="1"
	wget --no-check-certificate --no-cache -N --tries=5 --connect-timeout=10 $ftp/$SCRIPT_FILE_MD5 || ERREUR="1"
	if [ "$ERREUR" = "1" ];then
		echo -e "$COLERREUR"
		echo "Problème pour récupérer le script principal : ABANDON !"
		echo -e "$COLTXT"
		exit 1
	fi
	chmod +x $SCRIPT_FILE
	echo -e "$JAUNE"
	echo "Fin de la préparation."
	echo "============================================================="
	echo "Relancez $dirclinux/$SCRIPT_FILE"
	cd $SCRIPTS_DIR
	echo -e "$COLTXT"
	exit 1
fi


# Test et téléchargement éventuel d'une nouvelle version
if [ "$NODL" != "yes" ]; then
	echo -e "$COLINFO"
	echo "Vérification en ligne que vous avez bien la dernière version du script..."
	echo -e "$COLTXT"

	cd /root
	# Téléchargement dans /root du script à jour et du fichier md5
	rm -f $SCRIPT_FILE_MD5 $SCRIPT_FILE
	wget --no-check-certificate --no-cache -N --tries=5 --connect-timeout=10 --quiet $ftp/$SCRIPT_FILE || ERREUR="1"
	wget --no-check-certificate --no-cache -N --tries=5 --connect-timeout=10 --quiet $ftp/$SCRIPT_FILE_MD5 || ERREUR="1"
	if [ "$ERREUR" = "1" ];then
		echo -e "$COLERREUR"
		echo "Problème pour récupérer la version en ligne : ABANDON !"
		echo -e "$COLTXT"
		exit 1	
	fi
	
	# Calcul des md5 des fichiers qu'on vient de télécharger
	MD5_CTRL_FILE=$(cat $SCRIPT_FILE_MD5) # contenu du md5 téléchargé
	MD5_CTRL_DL=$(md5sum $SCRIPT_FILE) # md5 du script téléchargé

	# Controle de ce qu'on vient de télécharger
	if [ "$MD5_CTRL_FILE" != "$MD5_CTRL_DL" ]
	then	
		# Il y a une erreur dans ce qui a été téléchargé
		echo -e "$COLERREUR"
		echo "Controle MD5 du script téléchargé incorrect. Relancez $SCRIPT_FILE pour qu'il soit de nouveau téléchargé."
		echo -e "$COLTXT"
		exit 1
	fi

	# On se rend dans $dirclinux
	cd $SCRIPTS_DIR

	# Calcul du md5 de $dirclinux/prepare_clinux_se4.sh
	MD5_CTRL_LOCAL=$(md5sum $SCRIPT_FILE)

	# On retourne dans /root et on rend le script téléchargé executable
	cd
	chmod +x *.sh

	# On compare le md5 du script présent sur le serveur clinux avec celui du script téléchargé
	if [ "$MD5_CTRL_FILE" != "$MD5_CTRL_LOCAL" ]; then

		# Si les md5 sont différents, on passe la variable relance à yes
		RELANCE="YES"
		# Puis on copie le script depuis /root vers le repertoire des scripts
		cp $SCRIPT_FILE $SCRIPTS_DIR/
	fi

	if [ "$RELANCE" == "YES" ]
	then
		echo -e "$JAUNE"
		echo "Le script a été mis à jour depuis le dépôt."
		echo "Relancez $dirclinux/$SCRIPT_FILE"
		echo ""
		echo -e "$COLTXT"
		sleep 1
		exit 1	
	fi

	echo -e "$VERT"
	echo ""
	echo "Vous disposez de la dernière version du script, on peut continuer..."
	sleep 3
	echo -e "$COLTXT"
else
echo "Mode debug, pas de téléchargement."
sleep 2

fi # fin nodl

#----------------------------------
# Fonction mise à jour de la machine et installation des paquets indispensables
MAJSERV()
{
apt-get update && apt-get -y upgrade
apt-get install -y cifs-utils net-tools
}

#----------------------------------
# Fonction affichage du fichier de paramètres proxy(s)
AFFICHE_PROXYS()
{
echo "Configuration actuelle des proxys :"
echo "----------------------------------"
echo "Adresse du proxy http (http_proxy) : $config_http_proxy"
echo "Adresse du proxy apt (apt_proxy) : $config_apt_proxy"
}

#----------------------------------
# Fonction affichage du fichier de paramètres serveur
AFFICHE_PARAMS()
{
echo "Nom du domaine samba (samba_domain) : $DOM1"
echo "Domaine (domain) : $DOM2"
echo "Nom du serveur se4fs (se4fs_name) : $NOMFS"
#echo "Nom fqdn du serveur se4fs (se4fs_name.domain) : $NOMFS.$DOM2"
echo "Adresse serveur de fichiers (se4fs_ip) : $IPPART"
echo "Nom du serveur AD (se4ad_name) : $NOMAD"
echo "Adresse serveur dns (se4ad_ip) : $IPDNS"
echo "Adresse du proxy http (http_proxy) : $config_http_proxy"
echo "Adresse du proxy apt (apt_proxy) : $config_apt_proxy"
}

#----------------------------------
# Fonction création des répertoires indispensables
DEF_REP_1()
{
echo -e "$COLINFO"
echo "Création des répertoires indispensables..."
echo -e "$COLTXT"

if [ ! -e $webclinux ]; then
mkdir -p $webclinux
fi

rep01=/etc/clinux
rep02=$dirclinux/alancer
rep03=$dirclinux/once
rep04=$dirclinux/conex
rep05=$dirclinux/persolinks/base
rep06=$dirclinux/persolinks/cdi
rep07=$dirclinux/persolinks/sprofs
rep08=$dirclinux/persolinks/eleves
rep09=$dirclinux/persolinks/profs
rep10=$dirclinux/persolinks/autres
rep11=$dirclinux/divers
rep12=$dirclinux/save
rep13=$dirclinux/divers/fonts
rep101=$dirclinux/persolinks/gnome
rep102=$dirclinux/persolinks/mate
rep103=$dirclinux/persolinks/cinnamon
rep104=$dirclinux/persolinks/xfce
rep105=$dirclinux/persolinks/kde
rep106=$dirclinux/persolinks/clinux

	# doessier de parametres
	if [ ! -e $rep01 ]; then
	mkdir -p /etc/clinux
	fi
	# scripts de maintenance
	if [ ! -e $rep02 ]; then
	mkdir -p $dirclinux/alancer
	fi
	# scripts unefois
	if [ ! -e $rep03 ]; then
	mkdir -p $dirclinux/once
	fi
	# scripts de connexion
	if [ ! -e $rep04 ]; then
	mkdir -p $dirclinux/conex
	fi
	# dossier de liens base
	if [ ! -e $rep05 ]; then
	mkdir -p $dirclinux/persolinks/base
	fi
	# dossier de liens cdi
	if [ ! -e $rep06 ]; then
	mkdir -p $dirclinux/persolinks/cdi
	fi
	# dossier de liens salle des profs
	if [ ! -e $rep07 ]; then
	mkdir -p $dirclinux/persolinks/sprofs
	fi
	# dossier de liens eleves
	if [ ! -e $rep08 ]; then
	mkdir -p $dirclinux/persolinks/eleves
	fi
	# dossier de liens profs
	if [ ! -e $rep09 ]; then
	mkdir -p $dirclinux/persolinks/profs
	fi
	# dossier de liens autres
	if [ ! -e $rep10 ]; then
	mkdir -p /$dirclinux/persolinks/autres
	fi
	# dossier divers
	if [ ! -e $rep11 ]; then
	mkdir -p $dirclinux/divers
	fi
	# # dossier de sauvegarde scripts *_perso
	if [ ! -e $rep12 ]; then
	mkdir -p $dirclinux/save
	fi
	# dossier de depot des polices
	if [ ! -e $rep13 ]; then
	mkdir -p $dirclinux/divers/fonts
	fi
	# gnome
	if [ ! -e $rep101 ]; then
	mkdir -p $rep101
	fi
	# mate
	if [ ! -e $rep102 ]; then
	mkdir -p $rep102
	fi
	# cinnamon
	if [ ! -e $rep103 ]; then
	mkdir -p $rep103
	fi
	# xfce
	if [ ! -e $rep104 ]; then
	mkdir -p $rep104
	fi
	# kde
	if [ ! -e $rep105 ]; then
	mkdir -p $rep105
	fi
	# clinux (install ipxe)
	if [ ! -e $rep106 ]; then
	mkdir -p $rep106
	fi
}
#----------------------------------
# Fonction saisie du fichier de paramètres proxy(s)

CONF_PROXYS()
{
echo -e "$JAUNE"
echo "####################################"
echo "# Saisie des paramètres des proxys #"
echo "####################################"
echo -e "$COLTXT"
echo ""
echo "----------------------------------------------------------------"
echo " NB : La saisie de ces paramètres n'a AUCUN effet sur le se4fs." 
echo " Ils ne sont utilisés que par les clients linux."
echo "----------------------------------------------------------------"
echo ""

CACHE=o
#testamon=$(cat $confse4 | grep http_proxy | grep [1-9])
#testapt=$(cat $confse4 | grep apt_proxy | grep [1-9])
testamon=$(echo $config_http_proxy | grep [1-9])
testapt=$(echo $config_apt_proxy | grep [1-9])

if [ -z "$testapt" ] ; then
echo -e "$COLTITRE"
echo "Aucun cache apt n'est déclaré dans la configuration."
echo "Avez-vous un cache apt sur votre réseau de production ?"
echo -e "$COLTXT"
	PS3='Repondre par o ou n: '
	LISTE=("[o] oui" "[n]  non")
	select CHOIX in "${LISTE[@]}" ; do
		case $REPLY in
		1|o)
		echo -e "$COLINFO"
		echo "Il y a un cache apt sur ce réseau (cool)."
		echo -e "$COLTXT"
		CACHE=o
		break
		;;
		2|n)
		echo -e "$COLINFO"
		echo "Pas de cache apt (beurk)."
		echo -e "$COLTXT"
		echo ""
		CACHE=n
		break
		;;
		esac
	done
fi

# Définition de l'adresse du cache apt de l'etablissement (s'il y en a un)
if [ "$CACHE" = "o" ] ; then
	if [ -z "$testapt" ] ; then
		echo -e "$COLTITRE"
		echo "Adresse du CACHE APT de l'etablissement :"
		echo -e "$COLINFO"
		echo "Entrez l'adresse du cache apt *avec le port* sous la forme 172.17.139.251:3142 :"
		echo -e "$COLTXT"
		read APTCACHE
		if [ -z "$APTCACHE" ]; then
		set_config sambaedu apt_proxy " "
		else
		set_config sambaedu apt_proxy "http://$APTCACHE"
		fi
		echo -e "$COLTXT"
		echo ""
	else	
		#set_config sambaedu apt_proxy ""	
		#echo -e "$COLTXT" "Adresse actuelle du cache apt : $config_apt_proxy" ; tput sgr0
		echo ""
	fi

else # Pas de cache, on vide le champ d'adresse du cache apt
set_config sambaedu apt_proxy " "
echo "Configuration actuelle du cache apt : $config_apt_proxy (pas de cache apt)"
echo ""
fi

# Définition de l'adresse du proxy amon de l'etablissement
if [ -z "$testamon" ] ; then
	echo -e "$COLTITRE" "Adresse du proxy AMON (ou proxy http) de l'etablissement :" ; tput sgr0
	echo -e "$COLINFO"
	echo "Entrez l'adresse du proxy Amon de l'etablissement *avec le port* sous la forme 172.17.139.254:3128 :"
	echo -e "$COLTXT"
	read IPAMON
	if [ -z "$IPAMON" ]; then
	set_config sambaedu http_proxy " "
	else
	set_config sambaedu http_proxy "http://$IPAMON"
	fi
	echo -e "$COLTXT"
	echo -e "$COLTXT"
	echo ""
else
	#echo "Configuration actuelle pour l'amon : $config_http_proxy"
	echo ""
fi
}

#----------------------------------
# Fonction ajout d'un partage "Docs"
CONF_SMB_DOCS()
{
cp /etc/samba/smb.conf /etc/samba/smb.conf_$DATE1

echo "
# Partage docs
[docs]
        path = /var/sambaedu/Docs
        read only = no" >> /etc/samba/smb.conf
}
#----------------------------------
# Fonction ajout d'un partage pour les clients linux
CONF_SMB_CLINUX()
{
cp /etc/samba/smb.conf /etc/samba/smb.conf_$DATE1

echo "
# Partage pour les clients linux
[clinux]
	comment = Partage pour les clients linux
	path = $dirclinux
	browseable = yes
	read only = yes
	guest ok = yes" >> /etc/samba/smb.conf
}
#----------------------------------
# Fonction réglage des droits
PERM()
{
echo -e "$COLINFO" "Réglage des droits sur les scripts... fait."
echo ""
echo -e "$COLTXT"
chmod +x $dirclinux/*.sh
chmod +x $webclinux/*.sh
chmod +x $rep02/*.sh
chmod +x $rep03/*.sh
chmod +x $rep04/*.sh
}
#----------------------------------

# Création des répertoires
DEF_REP_1

# Configuration des proxys
if [ "$DEBUG" != "yes" ]; then
CONF_PROXYS
echo ""
AFFICHE_PROXYS
echo ""
echo -e "$COLTITRE"
#echo "Les paramètres PROXYs sont-ils corrects?"
echo "Les paramètres PROXYs sont-ils corrects? [o/n]"
echo -e "$COLTXT"
		read -p "	o (oui)
	n (non) : " rep

	case $rep in
			o )
		# Tout va bien, on continue
		echo "On poursuit..."
		;;
			n )
		# on remet la conf proxy à 0
		set_config sambaedu http_proxy " "
		set_config sambaedu apt_proxy " "
		echo "Reconfiguration des proxys"
		# on relance la configuration des proxys
		CONF_PROXYS
		;;
	esac
fi

echo -e "$COLINFO"
echo "##############################################################"
echo -e "$COLTXT"

# Affichage des paramètres actuels
echo -e "$JAUNE"
echo "##############################"
echo "###  Paramètres SERVEUR    ###"
echo "##############################"
echo ""
echo "Voici les paramètres actuels : "
echo ""
echo -e "$COLTXT"
echo "------------------------------------------------------------------"
AFFICHE_PARAMS
echo "------------------------------------------------------------------"
echo ""

echo -e "$COLTITRE"
echo "Les paramètres sont-ils corrects? "
echo -e "$COLTXT"
		read -p "	o (oui)
	n (non) : " rep

	case $rep in
			o )
		# Tout va bien, on continue
		echo "On poursuit..."		
		;;
			n )
		# Il y a un problème : on sort
		echo ""
		echo -e "$ROUGE"
		echo "Le script va s'interrompre pour vous permettre de vérifier les paramètres."
		echo -e "$COLTXT"
		sleep 3
		exit 1
		;;
	esac
echo ""

# Mise à jour de la machine et installations
echo ""

if [ "$NODL" != "yes" ]; then
echo -e "$COLINFO"
echo "Mise à jour de la machine (et installation de qq paquets si nécessaire)..."
echo -e "$COLTXT"
MAJSERV
fi

# Configuration de smb.conf 1 : partage docs
echo ""
echo -e "$COLINFO"
echo "Configuration de samba pour le partage Docs..."
echo -e "$COLTXT"

# on crée le répertoire Docs s'il n'existe pas
if [ ! -e "/var/sambaedu/Docs" ]; then
mkdir -p /var/sambaedu/Docs
fi

TEST_DOCS=$(cat /etc/samba/smb.conf | grep "docs")
if [ -n "$TEST_DOCS" ] ;then
echo "smb.conf (docs) deja modifié"
else
CONF_SMB_DOCS
# On a modifié smb.conf pour docs, il faut recharger la nouvelle conf
echo "On recharge la configuration samba..."
smbcontrol all reload-config
sleep 1
fi

# Configuration de smb.conf 2 : partage clinux
echo ""
echo -e "$COLINFO"
echo "Configuration de samba pour le partage clinux..."
echo -e "$COLTXT"

TEST_CLINUX=$(cat /etc/samba/smb.conf | grep "clinux")
if [ -n "$TEST_CLINUX" ] ;then
echo "smb.conf (clinux) deja modifié"
else CONF_SMB_CLINUX

# On a modifié smb.conf pour clinux, il faut recharger la nouvelle conf
echo "On recharge la configuration samba..."
smbcontrol all reload-config
sleep 1
fi

###################################################

# Telechargement et configuration des scripts
script_connect=createlinks.sh
script_connect_perso=createlinks_perso.sh
script_deconnect=deletelinks.sh

if [ "$NODL" != "yes" ]; then #DEBUT NODL

# Scripts de connexion/déconnexion (repertoire /conex)
echo ""
cd $rep04
rm -f init_unefois*
wget --no-check-certificate --no-cache --quiet $ftp/conex/init_unefois.sh
echo -e "$COLINFO"
echo "/conex/init_unefois.sh créé ou mis à jour."
echo -e "$COLTXT"

rm -f $script_connect
wget --no-check-certificate --no-cache --quiet $ftp/conex/$script_connect
echo -e "$COLINFO"
echo "/conex/$script_connect créé ou mis à jour."
echo -e "$COLTXT"
rm -f createlinks_lxde.sh createlinks_mate.sh createlinks_gnome.sh createlinks_cinnamon.sh createlinks_xfce.sh createlinks_kde.sh createlinks_default.sh
wget --no-check-certificate --no-cache --quiet $ftp/conex/createlinks_lxde.sh
wget --no-check-certificate --no-cache --quiet $ftp/conex/createlinks_mate.sh
wget --no-check-certificate --no-cache --quiet $ftp/conex/createlinks_gnome.sh
wget --no-check-certificate --no-cache --quiet $ftp/conex/createlinks_cinnamon.sh
wget --no-check-certificate --no-cache --quiet $ftp/conex/createlinks_xfce.sh
wget --no-check-certificate --no-cache --quiet $ftp/conex/createlinks_kde.sh
wget --no-check-certificate --no-cache --quiet $ftp/conex/createlinks_default.sh
echo -e "$COLINFO"
echo "Scripts pour lxde, mate, gnome, cinnamon, xfce et kde créés ou mis à jour."
echo -e "$COLTXT"

	if [ ! -f $script_connect_perso ];then
	wget --no-check-certificate --no-cache --quiet $ftp/conex/$script_connect_perso
	else
	echo -e "$COLDEFAUT"
	echo "$script_connect_perso existe. On n'y touche pas."
	echo -e "$COLTXT"
	# On en fait une copie, au cas où
	cp $script_connect_perso $rep12/$script_connect_perso-$DATE1
	fi

rm -f $script_deconnect
wget --no-check-certificate --no-cache --quiet $ftp/conex/$script_deconnect
echo -e "$COLINFO"
echo "/conex/$script_deconnect créé ou mis à jour."
echo -e "$COLTXT"

rm -f profil_ff.sh
wget --no-check-certificate --no-cache --quiet $ftp/conex/profil_ff.sh
echo -e "$COLINFO"
echo "/conex/profil_ff.sh créé ou mis à jour."
echo -e "$COLTXT"

	if [ ! -f "profil_ff_perso.sh" ];then
	wget --no-check-certificate --no-cache --quiet $ftp/conex/profil_ff_perso.sh
	else
	echo -e "$COLDEFAUT"
	echo "profil_ff_perso.sh existe. On n'y touche pas."
	# On en fait une copie, au cas où
	cp profil_ff_perso.sh $rep12/profil_ff_perso.sh_$DATE1
	echo -e "$COLTXT"
	fi

rm -f reparer_mon*
wget --no-check-certificate --no-cache --quiet $ftp/conex/reparer_mon_compte.sh
echo -e "$COLINFO"
echo "/conex/reparer_mon_compte.sh créé ou mis à jour."
echo -e "$COLTXT"
rm -f alsavolume*
wget --no-check-certificate --no-cache --quiet $ftp/conex/alsavolume.sh
echo -e "$COLINFO"
echo "/conex/alsavolume.sh créé ou mis à jour."
echo -e "$COLTXT"
rm -f mimeapps.list*
wget --no-check-certificate --no-cache --quiet $ftp/conex/mimeapps.list
echo -e "$COLINFO"
echo "/conex/mimeapps.list créé ou mis à jour."
echo -e "$COLTXT"
rm -f panel*
wget --no-check-certificate --no-cache --quiet $ftp/conex/panel
echo -e "$COLINFO"
echo "/conex/panel créé ou mis à jour."
echo -e "$COLTXT"

chmod +x *.sh
chmod 755 *.sh

# Scripts à exécuter unefois (repertoire /once)
cd $rep03
rm -f test_unefois.sh modif_lxde_icons.sh lxde_icons_unefois.sh cfguu_unefois.sh xscreen_unefois.sh mount-a_unefois.sh
rm -f test_unefois* lxde_icons_unefois* config_wol_unefois* cfguu_unefois*
wget --no-check-certificate --no-cache --quiet $ftp/once/test_unefois.sh
wget --no-check-certificate --no-cache --quiet $ftp/once/lxde_icons_unefois.sh

chmod +x *.sh
chmod 755 *.sh
fi

# Scripts qui peuvent être lancés sur les clients (répertoire /alancer)
if [ "$DEBUG" != "yes" ]; then
#testapt=$(cat $confse4 | grep apt_proxy | grep [1-9])
testapt=$(echo $config_apt_proxy | grep [1-9])
cd $rep02
rm -f maj_client_linux* change_proxy_client*
wget --no-check-certificate --no-cache --quiet $ftp/alancer/maj_client_linux.sh
wget --no-check-certificate --no-cache --quiet $ftp/alancer/change_proxy_client.sh

	if [ -n "$testapt" ] ; then
		sed -i "s|##ipcache##|$config_apt_proxy|g" $rep02/maj_client_linux.sh
		sed -i "s|##ipamon##|$config_http_proxy|g" $rep02/maj_client_linux.sh
		sed -i "s|##ipcache##|$config_apt_proxy|g" $rep02/change_proxy_client.sh
		sed -i "s|##ipamon##|$config_http_proxy|g" $rep02/change_proxy_client.sh
		
	else
		sed -i "s|##ipcache##|$config_http_proxy|g" $rep02/maj_client_linux.sh
		sed -i "s|##ipamon##|$config_http_proxy|g" $rep02/maj_client_linux.sh
		sed -i "s|##ipcache##|$config_http_proxy|g" $rep02/change_proxy_client.sh
		sed -i "s|##ipamon##|$config_http_proxy|g" $rep02/change_proxy_client.sh
	fi
	echo -e "$COLINFO"
	echo "maj_client_linux.sh téléchargé ou mis à jour. "
	echo ""
	echo "change_proxy_client.sh téléchargé ou mis à jour. "
	echo -e "$COLTXT"
fi

# [obsolete] Scripts de correction des clés ssh, de renommage et d'installation
cd $rep02
rm -f $rep02/correctif_cles_ssh*
#wget --no-check-certificate --no-cache --quiet $ftp/alancer/correctif_cles_ssh.sh
#sed -i 's|##ipamon##|$IPAMON|g' $rep02/correctif_cles_ssh.sh
#sed -i 's|##se3ip##|$getse3ip|g' $rep02/correctif_cles_ssh.sh
#echo -e "$COLINFO"
#echo "correctif_cles_ssh.sh téléchargé ou mis à jour. "
#echo -e "$COLTXT"


# Scripts utilitaires qui pourront etre lances sur les clients depuis /mnt/clinux/alancer
rm -f $dirclinux/alancer/renomme_client_linux* se4_mkhome* modif_logout* change_parc* ajoute_police*
wget --no-check-certificate --no-cache --quiet $ftp/alancer/se4_mkhome.sh
wget --no-check-certificate --no-cache --quiet $ftp/alancer/modif_logout_lxde.sh
wget --no-check-certificate --no-cache --quiet $ftp/alancer/renomme_client_linux_v3.sh
wget --no-check-certificate --no-cache --quiet $ftp/alancer/change_parc.sh
wget --no-check-certificate --no-cache --quiet $ftp/alancer/ajoute_police.sh
sed -i 's|##domain##|$DOM2|g' $dirclinux/alancer/renomme_client_linux_v3.sh
echo -e "$COLINFO"
echo "renomme_client_linux_v3.sh téléchargé ou mis à jour. "
echo -e "$COLTXT"
rm -f $dirclinux/alancer/vnc_user*
wget --no-check-certificate --no-cache --quiet $ftp/alancer/vnc_user.sh
echo -e "$COLINFO"
echo "vnc_user.sh téléchargé ou mis à jour. "
echo -e "$COLTXT"
echo ""
echo -e "$COLINFO" "Variables remplacées dans les scripts." ; tput sgr0
cd

# Recuperation et configuration des scripts d'integration des clients
script_int_stretch=rejoint_stretch_se4.sh
script_int_buster=rejoint_buster_se4.sh
script_int_stretch_ipxe=rejoint_stretch_se4_ipxe.sh
script_int_buster_ipxe=rejoint_buster_se4_ipxe.sh

# > dans le répertoire de base $dirclinux
cd $dirclinux
rm -f rejoint_stretch_* rejoint_buster_*
wget --no-check-certificate --no-cache --quiet $ftp/stretch/integration/$script_int_stretch
wget --no-check-certificate --no-cache --quiet $ftp/buster/integration/$script_int_buster

sed -i "s|##ippart##|$IPPART|g" $dirclinux/rejoint_*.sh
sed -i "s|##ipdns##|$IPDNS|g" $dirclinux/rejoint_*.sh
sed -i "s|##dom1##|$DOM1|g" $dirclinux/rejoint_*.sh
sed -i "s|##nomfs##|$NOMFS|g" $dirclinux/rejoint_*.sh
sed -i "s|##nomad##|$NOMAD|g" $dirclinux/rejoint_*.sh

# > dans le répertoire "web" $webclinux pour téléchargement après install ipxe
cd $webclinux
rm -f rejoint_stretch_* rejoint_buster_*
wget --no-check-certificate --no-cache --quiet $ftp/stretch/integration/$script_int_stretch_ipxe
wget --no-check-certificate --no-cache --quiet $ftp/buster/integration/$script_int_buster_ipxe

sed -i "s|##ippart##|$IPPART|g" $webclinux/rejoint_*.sh
sed -i "s|##ipdns##|$IPDNS|g" $webclinux/rejoint_*.sh
sed -i "s|##dom1##|$DOM1|g" $webclinux/rejoint_*.sh
sed -i "s|##nomfs##|$NOMFS|g" $webclinux/rejoint_*.sh
sed -i "s|##nomad##|$NOMAD|g" $webclinux/rejoint_*.sh

echo -e "$VERT" "Les scripts d'intégration des clients sont prêts. "
echo -e "$COLTXT"

# Recuperation et configuration des scripts d'installation des clients
script_install_stretch=install_stretch_etabs.sh
script_install_buster=install_buster_etabs.sh

# Téléchargement des scripts d'installation
testapt=$(echo $config_apt_proxy | grep [1-9])
cd $dirclinux
rm -f $script_install_stretch $script_install_buster
wget --no-check-certificate --no-cache --quiet $ftp/stretch/installation/$script_install_stretch
wget --no-check-certificate --no-cache --quiet $ftp/buster/installation/$script_install_buster
if [ -z "$testapt" ];then
sed -i "s|##aptcache##|aucun|g" $dirclinux/$script_install_buster
else
sed -i "s|##aptcache##|$config_apt_proxy|g" $dirclinux/$script_install_buster
fi

cd $webclinux
rm -f $script_install_stretch $script_install_buster
wget --no-check-certificate --no-cache --quiet $ftp/buster/installation/$script_install_buster
if [ -z "$testapt" ];then
sed -i "s|##aptcache##|aucun|g" $webclinux/$script_install_buster
else
sed -i "s|##aptcache##|$config_apt_proxy|g" $webclinux/$script_install_buster
fi


echo -e "$VERT" "Les scripts d'installation des clients sont prêts. "
echo ""
echo -e "$COLTXT"

# Recuperation du script de copie sur les clients
script_copie_cli=se4_copie_scripts_client.sh
cd $dirclinux
rm -f $script_copie_cli
wget --no-check-certificate --no-cache --quiet $ftp/$script_copie_cli
chmod +x $script_copie_cli

# Téléchargement des raccourcis usuels
# base
cd $rep05
rm -f se4fs.desktop* firefox.desktop* repare_profil.desktop* classes.desktop* partages.desktop*
wget --no-check-certificate --no-cache --quiet $ftp/raccourcis/se4fs.desktop
sed -i "s|##se4fsip##|$IPPART|g" se4fs.desktop
echo -e "$COLINFO"
echo "se4fs.desktop configuré pour ce se4."
echo -e "$COLTXT"
#cat se4fs.desktop
wget --no-check-certificate --no-cache --quiet $ftp/raccourcis/firefox.desktop
wget --no-check-certificate --no-cache --quiet $ftp/raccourcis/repare_profil.desktop
#wget --no-check-certificate --no-cache --quiet $ftp/raccourcis/ent.desktop
wget --no-check-certificate --no-cache --quiet $ftp/raccourcis/classes.desktop
wget --no-check-certificate --no-cache --quiet $ftp/raccourcis/partages.desktop

# cdi
cd $rep06
rm -f esidoc.desktop qwant.desktop
#wget --no-check-certificate --no-cache --quiet $ftp/raccourcis/esidoc.desktop
wget --no-check-certificate --no-cache --quiet $ftp/raccourcis/qwant.desktop

# salle des profs
cd $rep07
#rm -f webmel_clermont.desktop
#wget --no-check-certificate --no-cache --quiet $ftp/raccourcis/webmel_clermont.desktop

# eleves
cd $rep08
rm -f eleves.desk*
wget --no-check-certificate --no-cache --quiet $ftp/raccourcis/eleves.desktop

# profs
cd $rep09
#rm -f prof.desk* horde.desk* webmel_clermont.desk*
wget --no-check-certificate --no-cache --quiet $ftp/raccourcis/prof.desktop
#wget --no-check-certificate --no-cache --quiet $ftp/raccourcis/webmel_clermont.desktop

# autres
#cd $rep10
#rm -f veyon-prof*
#wget --no-check-certificate --no-cache --quiet $ftp/raccourcis/veyon-prof.desktop

echo -e "$COLINFO"
echo "Téléchargement des raccourcis terminé."
echo -e "$COLTXT"
echo -e "$COLINFO"
echo "Ajustement des droits sur les répertoires et les fichiers..."
echo -e "$COLTXT"

#Telechargement du script d'installation d'apt-cacher-ng
echo -e "$COLINFO"
echo "Téléchargement du script d'installation d'apt-cacher-ng"
echo -e "$COLTXT"
cd /usr/share/sambaedu/scripts
rm -f install-apt-cacher-ng.sh
wget --no-check-certificate --no-cache --quiet https://gitlab.sambaedu.org/sambaedu/sambaedu-ipxe/raw/master/sources/usr/share/sambaedu/scripts/install-apt-cacher-ng.sh
chmod +x install-apt-cacher-ng.sh

echo ""
# Réglage des droits sur les répertoires et les scripts
PERM

# Dépot d'un témoin dans /root
rm -f /root/prep_*
touch /root/prep_clinux_$DATE1
cd $dirclinux
echo -e "$VERT"
echo "Terminé !"
echo -e "$COLTXT"
exit 0
