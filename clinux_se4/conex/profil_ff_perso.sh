#!/bin/bash
#
# profil_ff_perso.sh
# Script de personnalisation de la synchronisation
# des profils firefox linux <-> windoze de l'utilisateur.
# Par défaut ce script ne fait rien, et il n'est pas écrasé par les mises à jour.
# 20191127


ladate=$(date +%F+%T)
fichlog=/home/$PAM_USER/session.log
fichlog_ff=/home/$PAM_USER/logprofil_ff.log

echo "# Debut profil_ff_perso.sh" >> $fichlog
echo "# Debut profil_ff_perso.sh" >> $fichlog_ff
#----------------------------------------
# Test d'appartenance à des groupes (AD)
#----------------------------------------
TEST_1=$(groups $PAM_USER | grep eleves)
[ -n "$TEST_1" ] && gruppo=eleves

TEST_2=$(groups $PAM_USER | grep profs)
[ -n "$TEST_2" ] && gruppo=profs

TEST_3=$(groups $PAM_USER | grep kituveu)
[ -n "$TEST_3" ] && gruppo=kituveu
#-----------------------------------

#-----------------------------------------------------------------------
# Actions conditionnelles de groupe (eleves ou profs, on pourrait en ajouter)
#-----------------------------------------------------------------------

if [ "$gruppo" = profs ]; then
	echo "$PAM_USER est membre du groupe profs (ff)" >> $fichlog_ff
	# Actions "profs" à insérer ici

elif [ "$gruppo" = eleves ]; then
	echo "$PAM_USER est membre du groupe eleves (ff)" >> $fichlog_ff
	# Actions "eleves" à insérer ici

elif [ "$gruppo" = kituveu ]; then
	echo "$PAM_USER est membre du groupe kituveu (ff)" >> $fichlog_ff
	# Actions "kituveu" à insérer ici
else
	echo "$PAM_USER n'est ni prof, ni eleve (ff)" >> $fichlog_ff
fi
#--------------------------------

echo "# Fin profil_ff_perso.sh" >> $fichlog_ff
echo "# Fin profil_ff_perso.sh" >> $fichlog
exit 0

