#!/bin/bash

# createlinks_perso.sh
# Par défaut, ce script ne fait rien.
# Ajoutez ici vos actions de connexion.
# Ce script ne sera pas modifié par les mises à jour.
# 20191218

# Répertoire de base des scripts, monté sur le client
clinuxdir=/mnt/clinux

fichlog=/home/$PAM_USER/session.log

# Détection du wm. Ca peut etre utile...
if [ "$GDMSESSION" = "lightdm-xsession" ]; then
WM="default"
#if [ "$GDMSESSION" = "LXDE" -o "$GDMSESSION" = "lightdm-xsession" ]; then
elif [ "$GDMSESSION" = "LXDE" ]; then
WM="lxde"
elif [ "$GDMSESSION" = "mate" ]; then
WM="mate"
elif [ "$GDMSESSION" = "gnome" ]; then
WM="gnome"
elif [ "$GDMSESSION" = "cinnamon" -o "$GDMSESSION" = "cinnamon2d" ]; then
WM="cinnamon"
elif [ "$GDMSESSION" = "xfce" ]; then
WM="xfce"
elif [ "$GDMSESSION" = "plasma" ]; then
WM="kde"
fi

# Détection du DM actif sur le client
mondm=$(cat /etc/X11/default-display-manager | cut -d '/' -f4)
if [ "$mondm" = "gdm3" ]; then
DM="gdm3"
# Si gdm3 est actif, on l'associe à Gnome (pour l'instant)
WM="gnome"
elif [ "$mondm" = "lightdm" ]; then
DM="lightdm"
elif [ "$mondm" = "sddm" ]; then
DM="sddm"
# si ssdm est actif, on l'associe à plasma (kde)
WM="kde"
fi

# Test d'appartenance à des groupes (AD). Ca peut aussi etre utile...
TEST_1=$(groups $PAM_USER | grep eleves)
[ -n "$TEST_1" ] && gruppo=eleves

TEST_2=$(groups $PAM_USER | grep profs)
[ -n "$TEST_2" ] && gruppo=profs

TEST_3=$(groups $PAM_USER | grep kituveu)
[ -n "$TEST_3" ] && gruppo=kituveu


echo "# Début exécution createlinks_perso.sh ($WM)" >> $fichlog
#-----------------------------
# Actions propres au groupe
#-----------------------------
if [ "$gruppo" = "profs" ]; then
	# Actions "profs"
echo ""
elif [ "$gruppo" = "eleves" ]; then
	# Actions "eleves"
echo ""
elif [ "$gruppo" = "kituveu" ]; then
	# Actions "kituveu"
echo ""
fi

#-----------------------------
# Actions propres au wm actif
#-----------------------------
#

#-----------------------------
# Actions propres au dm actif
#-----------------------------
if [ "$DM" = "gdm3" ]; then
	# Actions "gdm3"
echo ""
elif [ "$DM" = "lightdm" ]; then
	# Actions "lightdm"
echo ""
#elif [ "$DM" = "sddm" ]; then
#	# Actions "sddm"
#echo ""
fi

#------------------------------
# Autres scripts a lancer
#------------------------------
#sh /ce/que/tu/veux.sh

echo "# Fin exécution createlinks_perso.sh" >> $fichlog
exit 0

