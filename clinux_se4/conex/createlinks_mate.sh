#!/bin/bash

# createlinks_mate.sh
# script de connexion utilisateur lancé pour une session MATE
# lancé par createlinks.sh
# 20191127

# Répertoire de base des scripts, monté sur le client
clinuxdir=/mnt/clinux

fichlog=/home/$PAM_USER/session.log

# Témoin de lancement
echo "# Début exécution createlinks_mate.sh" >> $fichlog

#----------------------------------------
# Test d'appartenance à des groupes (AD)
#----------------------------------------
TEST_1=$(groups $PAM_USER | grep eleves)
[ -n "$TEST_1" ] && gruppo=eleves

TEST_2=$(groups $PAM_USER | grep profs)
[ -n "$TEST_2" ] && gruppo=profs

TEST_3=$(groups $PAM_USER | grep kituveu)
[ -n "$TEST_3" ] && gruppo=kituveu

#------------------------------------
# Détection du DM actif sur le client
#------------------------------------
mondm=$(cat /etc/X11/default-display-manager | cut -d '/' -f4)
if [ "$mondm" = "gdm3" ]; then
DM="gdm3"
elif [ "$mondm" = "lightdm" ]; then
DM="lightdm"
fi

#----------------------------------------
# Creation du dossier pour les liens
#----------------------------------------
if [ -d /home/$PAM_USER/link ] ; then
echo "dossier link existant"
else
mkdir /home/$PAM_USER/link
echo "Dossier link créé"
fi

#------------------------------
# Actions de base
#------------------------------
#cp $clinuxdir/persolinks/base/*.desktop /home/$PAM_USER/Bureau
#cp $clinuxdir/persolinks/base/* /home/$PAM_USER/link/

#------------------------------
# Actions de parc
#------------------------------
# (/etc/parc.txt contient le nom du parc assigné à l'intégration. Ce n'est pas un "vrai" parc AD!)

PARC=$(cat /etc/parc.txt)
echo "Ce client est membre du parc $PARC" >> $fichlog
#cp $clinuxdir/persolinks/$PARC/*.desktop /home/$PAM_USER/Bureau/
#cp $clinuxdir/persolinks/$PARC/* /home/$PAM_USER/link/

-----------------------------------------------------------------------
# Actions conditionnelles de groupe (eleves ou profs, on pourrait en ajouter)
#-----------------------------------------------------------------------

if [ "$gruppo" = profs ]; then
	echo "$PAM_USER est membre du groupe profs" >> $fichlog
	# Actions "profs" à insérer ici
	#
	#
	
	# Si veyon-master est installé sur le client, on copie aussi un raccourci veyon-prof sur le bureau
	#if [ -e "/usr/bin/veyon-master" ] ; then
	#echo "veyon-master est installé sur ce poste, on copie le raccourci.\"
	#cp $clinuxdir/persolinks/autres/veyon-prof.desktop /home/$PAM_USER/Bureau/
	#cp $clinuxdir/persolinks/autres/veyon-prof.desktop /home/$PAM_USER/links/
	#else
	#echo "veyon-master n'est pas installé sur ce poste."
	#fi

elif [ "$gruppo" = eleves ]; then
	echo "$PAM_USER est membre du groupe eleves" >> $fichlog
	# Actions "eleves" à insérer ici
	#
	#

elif [ "$gruppo" = kituveu ]; then
	echo "$PAM_USER est membre du groupe kituveu" >> $fichlog
	# Actions "kituveu" à insérer ici
	#
	#
else
	echo "$PAM_USER n'est ni prof, ni eleve." >> $fichlog
fi

#----------------------------
# Actions propres au wm actif
#----------------------------

#-----------------------------
# Actions propres au dm actif
#-----------------------------
if [ "$DM" = "gdm3" ]; then
	# Actions "gdm3"
echo ""
elif [ "$DM" = "lightdm" ]; then
	# Actions "lightdm"
echo ""
fi


#------------------------------
# Autres scripts a lancer
#------------------------------
#sh /ce/que/tu/veux.sh

# Témoin de fin
echo "# Fin exécution createlinks_mate.sh" >> $fichlog
exit 0

