#!/bin/bash
# Ce script peut etre lancé par l'utilisateur courant pour nettoyer son profil linux
# Le profil est nettoyé puis la session est fermée automatiquement
# 20191127
cd
rm -rf .adobe .cache .config/lxpanel .config/lxsession .config/pcmanfm .dbus .dmrc .fontconfig .gconf .gconfd .gnome2 .gksu* .gstreamer* .gtk-bookmar$
[ -e /usr/bin/logout1.sh ] && sh /usr/bin/logout1.sh
exit 0

