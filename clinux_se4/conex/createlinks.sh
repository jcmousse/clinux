#!/bin/bash

# createlinks.sh
# script de connexion utilisateur
# Prise en compte du wm utilisé avec les scripts createlinks_<wm>.sh
# Personnalisable avec createlinks_perso.sh
# 20191218

ladate=$(date +%F+%T)
toto=$PAM_USER
tata=$USER
fichlog=/home/$toto/session.log

# Inscription de la date en tête du log
echo $ladate > $fichlog
echo "PAM_USER : $toto" >> $fichlog
echo "USER : $tata" >> $fichlog

# Répertoire de base des scripts, monté sur le client
clinuxdir=/mnt/clinux

#-------------------------------------------------------
# Détection du wm actif (ne fonctionne qu'avec lightdm!)
#-------------------------------------------------------
if [ "$GDMSESSION" = "lightdm-xsession" ]; then
WM="default"
#if [ "$GDMSESSION" = "LXDE" -o "$GDMSESSION" = "lightdm-xsession" ]; then
elif [ "$GDMSESSION" = "LXDE" ]; then
WM="lxde"
elif [ "$GDMSESSION" = "mate" ]; then
WM="mate"
elif [ "$GDMSESSION" = "gnome" ]; then
WM="gnome"
elif [ "$GDMSESSION" = "cinnamon" -o "$GDMSESSION" = "cinnamon2d" ]; then
WM="cinnamon"
elif [ "$GDMSESSION" = "xfce" ]; then
WM="xfce"
elif [ "$GDMSESSION" = "plasma" ]; then
WM="kde"
fi

#----------------------
# Détection du DM actif
#----------------------
mondm=$(cat /etc/X11/default-display-manager | cut -d '/' -f4)
if [ "$mondm" = "gdm3" ]; then
DM="gdm3"
# Si gdm3 est actif, on l'associe à Gnome (pour l'instant)
WM="gnome"
elif [ "$mondm" = "lightdm" ]; then
DM="lightdm"
elif [ "$mondm" = "sddm" ]; then
DM="sddm"
# si ssdm est actif, on l'associe à plasma (kde)
WM="kde"
fi

echo "Environnement : $WM" >> $fichlog
echo "Gestionnaire de connexion : $DM" >> $fichlog

# Les commandes et scripts ci-dessous ne dépendent pas du wm :
# Applications par defaut
touch /home/$toto/.config/mimeapps.list
cat /mnt/clinux/conex/mimeapps.list > /home/$toto/.config/mimeapps.list

# Avant de continuer, utilisateur local ou AD ?
userloc=$(id -ng $toto)
echo $userloc >> $fichlog
if [ "$userloc" = "$toto" ];then
echo "$toto est un user local, on sort!" >> $fichlog
exit 0
else
echo "$toto est un user AD, on continue" >> $fichlog
fi

# Lancement de createlinks_<wm>
echo "# Lancement de createlinks_$WM.sh" >> $fichlog
sh /mnt/clinux/conex/createlinks_$WM.sh

# Lancement de createlinks_perso
echo "# Lancement de createlinks_perso.sh" >> $fichlog
sh /mnt/clinux/conex/createlinks_perso.sh

# Lancement de profil_ff
echo "# Lancement de profil_ff.sh" >> $fichlog
sh /mnt/clinux/conex/profil_ff.sh

# Lancement de profil_ff_perso
echo "# Lancement de profil_ff_perso.sh" >> $fichlog
sh /mnt/clinux/conex/profil_ff_perso.sh

# Lancement des autres scripts
#sh $clinuxdir/conex/alsavolume.sh
#sh $clinuxdir/conex/clean_vartmp.sh
#bash $clinuxdir/conex/warn_overfill.sh

exit 0
