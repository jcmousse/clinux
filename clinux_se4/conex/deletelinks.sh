#!/bin/bash
# deletelinks.sh
# script de déconnexion utilisateur
# Prise en compte eventuelle du wm utilisé
# 20191127

ladate=$(date +%F+%T)
fichlog=/home/$PAM_USER/session.log

# Log fermeture de session
echo $ladate >> $fichlog
echo "Fermeture de session : lancement deletelinks.sh" >> $fichlog

# Détection du wm utilisé par l'utilisateur
sleep 1
if [ "$GDMSESSION" = "LXDE" ]; then
WM="lxde"
echo "LXDE détecté - $WM - $PAM_USER" >> $fichlog
fi
if [ "$GDMSESSION" = "mate" ]; then
WM="mate"
echo "MATE détecté - $WM - $PAM_USER" >> $fichlog
fi
if [ "$GDMSESSION" = "gnome" ]; then
WM="gnome"
echo "GNOME détecté - $WM - $PAM_USER" >> $fichlog
fi

# Suppression des raccourcis copiés à la connexion (tous wm)
ls /home/$USER/link | while read A ; do rm -f /home/$USER/Bureau/$A ; done
echo "Fermeture de session : raccourcis supprimés" >> $fichlog

# Si c'est une session LXDE :
#if [ "$GDMSESSION" = "LXDE" ];then
sed -i 's/icon/list/g' /home/$USER/.config/pcmanfm/LXDE/pcmanfm.conf
#fi
sed -i 's/_exec=0/_exec=1/g' /home/$USER/.config/libfm/libfm.conf
sed -i 's/Desktop/Bureau/g' /home/$USER/.config/user-dirs.dirs

rm -f /home/$USER/link/*

echo "Fermeture de session : fin exécution deletelinks.sh" >> $fichlog

rm -rf /home/$USER/Desktop
rm -f /home/$USER/.mozilla/firefox/default/.parentlock
rm -rf /home/$USER/.local/share/Trash/*

umount /home/$USER
killall -u $USER
sleep 1

umount /media/Classes
umount /media/Partages
umount /home/$USER
exit 0

