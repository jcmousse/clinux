#!/bin/bash
#
# profil_ff.sh
# Script de synchronisation des profils firefox linux <-> windoze de l'utilisateur
# 20191127
#

echo "# Debut exécution profil_ff.sh" >> /home/$PAM_USER/session.log

ladate=$(date +%F+%T)
fichlog_ff=/home/$PAM_USER/logprofil_ff.log
dirclinux=/mnt/clinux

# Inscription de la date en tête du log
echo $ladate > $fichlog_ff

# Test et création du répertoire mozilla linux s'il n'existe pas
if [ ! -e "/home/$PAM_USER/.mozilla/firefox" ]; then
echo "Le répertoire .mozilla/firefox n'existe pas, il faut le créer." >> $fichlog_ff
mkdir -p /home/$PAM_USER/.mozilla/firefox
else echo "Le répertoire .mozilla/firefox existe." >> $fichlog_ff
fi

# Création ou modification du profiles.ini pour linux
echo "[General]
StartWithLastProfile=1

[Profile0]
Name=default
IsRelative=1
Path=default" > /home/$PAM_USER/.mozilla/firefox/profiles.ini
echo "profiles.ini ff linux créé ou corrigé." >> $fichlog_ff

# S'il existe un profil ff linux nommé xxx.default ou xxx.default-esr : on le met de coté en le renommant
test_ff_tux=$(ls /home/$PAM_USER/.mozilla/firefox/ | grep [.]default$)
test_ff_tux_esr=$(ls /home/$PAM_USER/.mozilla/firefox/ | grep [.]default-esr$)
test_ff_tux_rel=$(ls /home/$PAM_USER/.mozilla/firefox/ | grep [.]default-release$)
if [ -n "$test_ff_tux" ]; then
echo "Il existe un repertoire xxx.default pour ff tux. On le renomme." >> $fichlog_ff
mv /home/$PAM_USER/.mozilla/firefox/*.default /home/$PAM_USER/.mozilla/firefox/default.old_$ladate
elif [ -n "$test_ff_tux_esr" ]; then
echo "Il existe un repertoire xxx.default-esr pour ff tux. On le renomme." >> $fichlog_ff
mv /home/$PAM_USER/.mozilla/firefox/*.default-esr /home/$PAM_USER/.mozilla/firefox/default-esr.old_$ladate
elif [ -n "$test_ff_tux_rel" ]; then
echo "Il existe un repertoire xxx.default-release pour ff tux. On le renomme." >> $fichlog_ff
mv /home/$PAM_USER/.mozilla/firefox/*.default-release /home/$PAM_USER/.mozilla/firefox/default-release.old_$ladate
else
echo "Pas de répertoire xxx.default* pour ff tux. On continue." >> $fichlog_ff
fi

# Test de l'existence du répertoire default pour windoze
test_default_win=$(ls /home/$PAM_USER/AppData/Roaming/Mozilla/Firefox/Profiles | grep -o ^default$)
if [ -n "$test_default_win" ]; then
echo "Le dossier default ff pour windoze existe." >> $fichlog_ff
else
echo "Le dossier default ff pour windoze n'existe pas, il faut le créer." >> $fichlog_ff
mkdir -p /home/$PAM_USER/AppData/Roaming/Mozilla/Firefox/Profiles/default
echo "Dossier default ff pour windoze créé." >> $fichlog_ff
fi

# Création ou modification du profiles.ini pour windoze
echo "[General]
StartWithLastProfile=1

[Profile0]
Name=default
IsRelative=1
Path=Profiles/default" > /home/$PAM_USER/AppData/Roaming/Mozilla/Firefox/profiles.ini
echo "profiles.ini ff windoze créé ou corrigé." >> $fichlog_ff

# S'il existe un profil ff windoze nommé xxx.default* : on le met de coté en le renommant
test_ff_win=$(ls /home/$PAM_USER/AppData/Roaming/Mozilla/Firefox/Profiles | grep [.]default$)
test_ff_win_esr=$(ls /home/$PAM_USER/AppData/Roaming/Mozilla/Firefox/Profiles | grep [.]default-esr$)
test_ff_win_rel=$(ls /home/$PAM_USER/AppData/Roaming/Mozilla/Firefox/Profiles | grep [.]default-release$)
if [ -n "$test_ff_win" ]; then
echo "Il exite un dossier ff xxx.default pour windoze, on le renomme." >> $fichlog_ff
cd /home/$PAM_USER/AppData/Roaming/Mozilla/Firefox/Profiles
mv *.default default.old_$ladate
elif [ -n "$test_ff_win_esr" ]; then
echo "Il exite un dossier ff xxx.default-esr pour windoze, on le renomme." >> $fichlog_ff
cd /home/$PAM_USER/AppData/Roaming/Mozilla/Firefox/Profiles
mv *.default-esr default-esr.old_$ladate
elif [ -n "$test_ff_win_rel" ]; then
echo "Il exite un dossier ff xxx.default-release pour windoze, on le renomme." >> $fichlog_ff
cd /home/$PAM_USER/AppData/Roaming/Mozilla/Firefox/Profiles
mv *.default-release default-release.old_$ladate
else
echo "Pas de dossier ff xxx.default* pour windoze, on continue." >> $fichlog_ff
fi

# Création du lien symbolique entre les deux dossiers de profil FF s'il n'existe pas
if [ ! -L "/home/$PAM_USER/.mozilla/firefox/default" ]; then
echo "Le lien symbolique n'existe pas, il faut le créer." >> $fichlog_ff
ln -s /home/$PAM_USER/AppData/Roaming/Mozilla/Firefox/Profiles/default /home/$PAM_USER/.mozilla/firefox/
else
echo "Le lien symbolique existe entre le profil ff win et tux existe." >> $fichlog_ff
fi

echo ""
echo "# Fin exécution profil_ff.sh" >> /home/$PAM_USER/session.log
echo ""
echo "Traitement des profils ff linux <-> windoze terminé." >> $fichlog_ff
echo ""

exit 0

