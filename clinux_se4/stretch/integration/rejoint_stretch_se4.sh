#!/bin/bash

# ****************************************************************
# rejoint_stretch_se4.sh
# Configuration d'un client debian 9.x stretch intégré à un domaine se4
# 20191109
# ****************************************************************

DATERAPPORT=$(date +%F+%0kh%0M)

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

DATESCRIPT="20190624"
VERSION="0.2.0"

ftp=https://gitlab.com/jcmousse/clinux/raw/master/clinux_se4

#####################

# Paramètres complétés par le script de préparation
IPPART=##ippart##
IPDNS=##ipdns##
IPSERV=##ippart##
DOM1=##dom1##
NOMFS=##nomfs##
NOMAD=##nomad##

# Test pour les distraits
[ -e /var/sambaedu/clients-linux ] && echo "Malheureux... Ce script est a exécuter sur les clients Linux, pas sur le se4 !" && exit 1

# Test de la version Debian du client
TEST_VERSION=$(cat /etc/debian_version | grep "9.")
if [ ! -n "$TEST_VERSION" ]; then
echo "Ce script est destiné à un système Debian 9 (Stretch)."
echo "Il n'est pas adapté à ce système."
exit
else
echo "Debian 9 (Stretch) détectée... on poursuit."
fi

# Configuration du pavé numérique au démarrage si c'est un pc de bureau
echo -e "$COLDEFAUT" "Cet ordinateur est-il un portable ? [o/N] " ; tput sgr0
read repport
if [ ! "$repport" = "o" -a ! "$repport" = "oui" ]; then
	echo -e "$COLINFO" "Cet ordinateur n'est pas un portable."
	echo -e "$COLINFO" "On activera le verrouillage numérique au démarrage."
	echo -e "$COLTXT"
	NUMLOCK=o
else
	echo -e "$COLINFO" "Cet ordinateur est un portable."
	echo -e "$COLINFO" "Pas de verrouillage numérique au démarrage."
	echo -e "$COLTXT"
	NUMLOCK=n
fi

# Configuration de l'integration de la machine : hostname et parc
		echo "Configuration des paramètres de l'intégration..."
		echo ""
		echo -e "$COLINFO" "Nom actuel de la machine : $(hostname)" ; tput sgr0
		ANCIENNOM=$(hostname)
		echo ""
		echo -e "$COLDEFAUT" "Entrez un nom pour cette machine, " ; tput sgr0
		echo -e "$COLDEFAUT" "Ou validez pour conserver son nom actuel :" ; tput sgr0
		read NOMMACH
			if [ -z "$NOMMACH" ]; then
			NOMMACH=$ANCIENNOM
			fi
		echo ""
		echo -e "$COLINFO" "Le nom choisi pour la machine est $NOMMACH." ; tput sgr0
		echo ""
		echo -e "$COLDEFAUT" "Dans quel parc sera intégrée cette machine ?" ; tput sgr0
		echo -e "$COLINFO" "Attention : seuls choix possibles : cdi ou sprofs" ; tput sgr0
		echo ""
		read PARK
		echo ""
		echo -e "$COLINFO" "Le client sera affecté dans le parc $PARK." ; tput sgr0
		echo ""

# Inscription du nom du parc sur le client
echo $PARK > /etc/parc.txt
chmod 744 /etc/parc.txt

# Mise a jour de la machine
export DEBIAN_FRONTEND=noninteractive
export DEBIAN_PRIORITY=high

echo -e "$VERT" "Mise à jour du système..."
echo -e "$COLTXT"

# Resolution du probleme de lock
if [ -e "/var/lib/dpkg/lock" ]; then
	rm -f /var/lib/dpkg/lock
fi

# On lance une maj
apt-get update
apt-get -y upgrade

# Installation des paquets necessaires
echo "Installation des paquets nécessaires:"
echo "Ne rien remplir, les fichiers seront configurés/modifiés automatiquement ensuite..."
echo ""
apt-get install -y samba-libs samba-common samba-common-bin smbclient cifs-utils libpam-winbind libpam-mount libnss-winbind ntpdate net-tools numlockx

# Configuration des fichiers
echo "Configuration des fichiers pour le serveur..."
echo ""

# Configuration de ntpdate
echo "Configuration du fichier /etc/default/ntpdate"
cp /etc/default/ntpdate /etc/default/ntpdate_sauve_$DATERAPPORT
echo -e "# /etc/default/ntpdate
# The settings in this file are used by the program ntpdate-debian, but not
# by the upstream program ntpdate.

# Set to "yes" to take the server list from /etc/ntp.conf, from package ntp,
# so you only have to keep it in one place.
NTPDATE_USE_NTP_CONF=no

# List of NTP servers to use  (Separate multiple servers with spaces.)
# Not used if NTPDATE_USE_NTP_CONF is yes.
NTPSERVERS="ntp.ac-creteil.fr swisstime.ethz.ch"

# Additional options to pass to ntpdate
NTPOPTIONS="" " > /etc/default/ntpdate

# Création des points de montage
mkdir -p /media/Classes
mkdir -p /media/Partages

# Configuration du fichier /etc/security/pam_mount.conf.xml
echo "Configuration du fichier /etc/security/pam_mount.conf.xml"
cp /etc/security/pam_mount.conf.xml /etc/security/pam_mount.conf.xml_sauve_$DATERAPPORT
echo "Configuration du fichier /etc/security/pam_mount.conf.xml"
echo ""

echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>

<!--
/etc/security/pam_mount.conf.xml
Configuration pour le serveur
-->

<pam_mount>
  <debug enable=\"1\" />
  <logout wait=\"0\" hup=\"0\" term=\"0\" kill=\"0\" />
  <mkmountpoint enable=\"1\" remove=\"true\" />

  <umount>umount %(MNTPT)</umount>

 <volume
    pgrp=\"domain users\"
    fstype=\"cifs\"
    server=\"$IPPART\"
    path=\"users/%(DOMAIN_USER)\"
    mountpoint=\"/home/%(DOMAIN_USER)\"
    options=\"username=%(DOMAIN_USER),uid=%(USERUID),gid=%(USERGID),cruid=%(USERUID),file_mode=0700,dir_mode=0700,vers=1.0\"
  />

  <volume
    pgrp=\"domain users\"
    fstype=\"cifs\"
    server=\"$IPPART\"
    path=\"classes\"
    mountpoint=\"/media/Classes\"
    options=\"username=%(USER),uid=%(USERUID),gid=%(USERGID),file_mode=0700,dir_mode=0700,vers=3.0\"
  />

  <volume
    pgrp=\"domain users\"
    fstype=\"cifs\"
    server=\"$IPPART\"
    path=\"docs\"
    mountpoint=\"/media/Partages\"
    options=\"username=%(USER),uid=%(USERUID),gid=%(USERGID),file_mode=0700,dir_mode=0700,vers=3.0\"
   />

<msg-authpw>Mot de passe :</msg-authpw>
<msg-sessionpw>Mot de passe :</msg-sessionpw>

</pam_mount>" > /etc/security/pam_mount.conf.xml

# Configuration du fichier /etc/pam.d/common-auth
echo "Configuration du fichier /etc/pam.d/common-auth"
echo ""
cp /etc/pam.d/common-auth /etc/pam.d/common-auth_sauve_$DATERAPPORT
echo "
# /etc/pam.d/common-auth
auth    [success=3 default=ignore]      pam_krb5.so minimum_uid=1000
auth    [success=2 default=ignore]      pam_unix.so nullok_secure try_first_pass
auth    [success=1 default=ignore]      pam_winbind.so krb5_auth krb5_ccache_type=FILE cached_login try_first_pass
auth    requisite                       pam_deny.so
auth    required                        pam_permit.so
auth    optional        		pam_mount.so
auth    optional                        pam_cap.so" > /etc/pam.d/common-auth

# Configuration du fichier /etc/pam.d/common-session
echo "Configuration du fichier /etc/pam.d/common-session"
echo ""
cp /etc/pam.d/common-session /etc/pam.d/common-session_sauve_$DATERAPPORT
echo "
# /etc/pam.d/common-session
session [default=1]                     pam_permit.so
session requisite                       pam_deny.so
session required                        pam_permit.so
session optional                        pam_krb5.so minimum_uid=1000
session required        pam_unix.so
session required        pam_mkhomedir.so umask=0022 skel=/etc/skel
session optional        pam_winbind.so
session optional        pam_mount.so
session optional        pam_systemd.so
session optional        pam_exec.so type=open_session seteuid log=/tmp/pamlogon /bin/bash /usr/bin/logon1.sh
session optional        pam_exec.so type=close_session seteuid log=/tmp/pamlogout /bin/bash /usr/bin/logout1.sh" > /etc/pam.d/common-session

# Configuration du fichier /etc/pam.d/common-account
echo "Configuration du fichier /etc/pam.d/common-account"
echo ""
cp /etc/pam.d/common-account /etc/pam.d/common-account_sauve_$DATERAPPORT
echo "
# /etc/pam.d/common-account
account [success=2 new_authtok_reqd=done default=ignore]        pam_unix.so
account [success=1 new_authtok_reqd=done default=ignore]        pam_winbind.so
account requisite                       pam_deny.so
account required                        pam_permit.so
account required                        pam_krb5.so minimum_uid=1000" > /etc/pam.d/common-account

# Configuration du fichier /etc/pam.d/common-password
echo "Configuration du fichier /etc/pam.d/common-password"
echo ""
cp /etc/pam.d/common-password /etc/pam.d/common-password_sauve_$DATERAPPORT
echo "# /etc/pam.d/common-password
password        [success=3 default=ignore]      pam_krb5.so minimum_uid=1000
password        [success=2 default=ignore]      pam_unix.so obscure use_authtok try_first_pass sha512
password        [success=1 default=ignore]      pam_winbind.so use_authtok try_first_pass
password        requisite                       pam_deny.so
password        required                        pam_permit.so
password        optional        pam_gnome_keyring.so" > /etc/pam.d/common-password

# Configuration du fichier /etc/security/group.conf
echo "Configuration du fichier /etc/security/group.conf"
echo ""
cp /etc/security/group.conf /etc/security/group.conf_sauve_$DATERAPPORT
echo "
# /etc/security/group.conf
lightdm;*;*;Al0000-2400;floppy,cdrom,audio,video,plugdev,dialout
gdm3;*;*;Al0000-2400;floppy,cdrom,audio,video,plugdev,dialout
xdm;*;*;Al0000-2400;floppy,cdrom,audio,video,plugdev,dialout" > /etc/security/group.conf

# Point de montage sur les clients d'un partage du serveur
if [ ! -e /mnt/clinux ] ; then
mkdir -p /mnt/clinux
#chown -R admin:admins /mnt/clinux
chmod -R 755 /mnt/clinux
fi

# Création d'une entrée dans /etc/fstab pour monter un répertoire partagé du serveur se4fs
sed -i '/clinux/d' /etc/fstab
sed -i '/^$/d' /etc/fstab
echo "//$IPPART/clinux /mnt/clinux  cifs  guest" >> /etc/fstab

# init1.sh - script local de lancement des scripts "unefois"
echo "#!/bin/bash
sleep 3
sh /mnt/clinux/conex/init_unefois.sh
exit 0" > /usr/bin/init1.sh

# Activation du pavé numérique au démarrage, si demandé
	if [ $NUMLOCK = "o" ] ; then
	sed -i '/exit 0/d' /usr/bin/init1.sh
	sed -i '/^$/d' /usr/bin/init1.sh
	echo "/usr/bin/X11/numlockx on
exit 0" >> /usr/bin/init1.sh
	fi
chmod +x /usr/bin/init1.sh

# init1.sh (lancement des scripts "unefois" au démarrage du dm)
# Pour lightdm
#if [ -e "/usr/sbin/lightdm" ];then
#sed -i 's/#greeter-setup-script=/greeter-setup-script=\/usr\/bin\/init1.sh/g' /etc/lightdm/lightdm.conf
#fi

# Lancement de init1.sh au démarrage par systemd
if [ ! -e /lib/systemd/system/init1.service ] ; then
echo "[Unit]
Description=Init1 service

[Service]
ExecStart=/usr/bin/init1.sh

[Install]
WantedBy=multi-user.target" > /lib/systemd/system/init1.service
fi

systemctl enable init1.service

#=================================================
# Installation et réglages pour gdm3
if [ -e /usr/sbin/gdm3 ];then

# Desactivation de la liste des utilisateurs sur la mire de connexion
sed -i 's/#\ disable-user-list/disable-user-list/g' /etc/gdm3/greeter.dconf-defaults

# Desactivation du changement d'utilisateur dans le menu
mkdir -p /etc/dconf/profile
echo "user-db:user
system-db:local" > /etc/dconf/profile/user
mkdir -p /etc/dconf/db/local.d/locks
echo "[org/gnome/desktop/lockdown]
# Prevent the user from user switching
disable-user-switching=true

[org/gnome/desktop/screensaver]
user-switch-enabled=false" > /etc/dconf/db/local.d/00-switch
echo "# Lock user switching
/org/gnome/desktop/lockdown/disable-user-switching
# Lock switching from locked screen
#/org/gnome/desktop/screensaver/user-switch-disabled" > /etc/dconf/db/local.d/locks/lockdown
dconf update

fi
#==================================================

# logon1.sh (lancement des scripts de connexion)
echo "#!/bin/sh
echo \"$PAM_USER\" > /tmp/pamuser.log
sh /mnt/clinux/conex/createlinks.sh
exit 0" > /usr/bin/logon1.sh
chmod +x /usr/bin/logon1.sh

# logout1.sh (lancement des scripts de déconnexion)
echo "#!/bin/sh
sh /mnt/clinux/conex/deletelinks.sh
exit 0" > /usr/bin/logout1.sh
chmod +x /usr/bin/logout1.sh

# Alias pour l'extinction par root
testpoweroff=$(cat /root/.bashrc | grep poweroff)
if [ ! -n "$testpoweroff" ] ; then
echo "alias halt='poweroff'" >> /root/.bashrc
fi

# Correction des droits sur les fichiers sensibles
chmod 600 /etc/sambaedu/sambaedu.conf
chmod -R 600 /etc/sambaedu/sambaedu.conf.d

# Activation des maj automatiques
apt-get install --assume-yes unattended-upgrades
cp /usr/share/unattended-upgrades/20auto-upgrades /etc/apt/apt.conf.d/20auto-upgrades
cd /etc/apt/apt.conf.d
rm -f 50unattended-upgrades
wget --no-check-certificate $ftp/stretch/50unattended-upgrades

#=================================================
# Installation et réglages pour lxde-lightdm
# On teste si lxde est installé
lxde=/etc/xdg/lxsession

if [ -e $lxde ]; then
# Suppression de clipit et de screenlock
cd /usr/share/applications
mv lxde-screenlock.desktop lxde-screenlock.desktop.bak
cd /etc/xdg/autostart
mv clipit-startup.desktop clipit-startup.desktop.bak
mv /etc/xdg/autostart/light-locker.desktop /etc/xdg/autostart/light-locker.desktop.bak

# Suppression de wicd
echo "Suppression de wicd..."
echo ""
systemctl stop wicd
systemctl disable wicd
systemctl mask wicd
apt-get remove --purge -y wicd
cd /etc/xdg/autostart
mv wicd-tray.desktop wicd-tray.desktop.bak
apt-get -y autoremove
apt-get clean
apt-get autoclean


# Installation et configuration de network-manager
apt-get install -y network-manager
sed -i 's/false/true/g' /etc/NetworkManager/NetworkManager.conf

echo "# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback" > /etc/network/interfaces
fi
#=================================================

if [ ! -e /etc/xdg/autostart ] ; then
mkdir -p /etc/xdg/autostart
chmod -R 755 /etc/xdg/autostart
fi

####################################################################################

echo -e "$COLDEFAUT" "La machine redémarrera dans 10 s "
echo -e "$COLDEFAUT" "(ou ctrl-c pour empecher le redémarrage)"
sleep 10
reboot

echo -e "$COLTXT"
