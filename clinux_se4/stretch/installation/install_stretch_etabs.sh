#!/bin/bash
# **********************************************************
# Installation automatisée d'un client stretch pour se4
# Après installation de base du système et intégration à l'AD,
# et avant ou après post-intégration.
# Installation de Lxde, Gnome, Mate, Cinnamon ou Xfce au choix.
# version du 20191109 pour se4
# **********************************************************
DATEINST=$(date +%F+%0kh%0M)
FTP=https://gitlab.com/jcmousse/clinux/raw/master/clinux_se4
FTP2=http://jcmousse.free.fr

ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# Test pour les distraits
[ -e /var/sambaedu/clients-linux ] && echo "Malheureux... Ce script est a exécuter sur les clients Linux, pas sur le se4 !" && exit 1

LRA="0" # mettre LRA="1" si se4-internet est installé (mais se4-internet n'existe pas encore!)

#LX="1" # mettre LX="0" pour désactiver l'installation de lxde par défaut

# On neutralise le cache apt utilisé pendant l'installation de base du système
> /etc/apt/apt.conf
> /etc/apt/apt.conf.d/99proxy

# Test de la version Debian du client
TEST_VERSION=$(cat /etc/debian_version | grep "9.")
if [ ! -n "$TEST_VERSION" ]; then
echo "Ce script est destiné à un système Debian 9 (Stretch)."
echo "Il n'est pas adapté à ce système."
exit 0
else
echo "Debian 9 (Stretch) détectée... on poursuit."
echo ""
fi

#========================================
# Ceci ne sera exécuté QUE SI vous passez LRA="1" (Défaut : LRA="0")
if [ "$LRA" == "1" ]; then
	TEST_ROCHE=$(cat /etc/profile | grep "139.253")
	if [ ! -n "$TEST_ROCHE" ]; then
	echo ""
	echo -e "$JAUNE" "Le proxy http n'est pas renseigné dans /etc/profile" ; tput sgr0
	echo -e "$ROUGE" "Lieu d'installation : Etes-vous au lycée Roche-Arnaud ? [o/N] " ; tput sgr0
	read replra
		if [ ! "$replra" = "o" -a ! "$replra" = "oui" ]; then
		echo "Installation dans un établissement standard."
		echo "Poursuivons..."
		ROCHE=n
		else
		echo "Installation au lycée Roche-Arnaud."
		ROCHE=o
		fi


		if [ $ROCHE = "o" ] ; then
		# On declare le proxy http (ip du se4fs) dans /etc/profile si on installe depuis Roche-Arnaud,
		# puis on sort du script : il faut se relogger pour prendre en compte le proxy.
		sed -i '/export/d' /etc/profile
		sed -i '/Configuration/d' /etc/profile
		echo "export http_proxy=\"http://172.17.139.253:3128\"" >> /etc/profile
		echo "export https_proxy=\"https://172.17.139.253:3128\"" >> /etc/profile
		echo "Le proxy http a été renseigné dans /etc/profile."
		echo "Le script va s'interrompre..."
		echo ""
		echo -e "$ROUGE" "Il faut sortir du shell, se relogger (en root) puis relancer le script." ; tput sgr0
		sleep 3
		exit
		else
		# Sinon on continue
		echo "" 
		fi
	else
# On vérifie le lieu d'installation
echo -e "$COLDEFAUT" "Le proxy du lycée Roche-Arnaud a été détecté dans la configuration du client. " ; tput sgr0
echo ""
echo -e "$ROUGE" "Lieu d'installation : Etes-vous bien au lycée Roche-Arnaud ? [O/n] " ; tput sgr0
echo ""
	read replra
		if [ ! "$replra" = "n" -a ! "$replra" = "non" ]; then
		echo "Installation à Roche-Arnaud." ; tput sgr0
		echo "Poursuivons..." ; tput sgr0
		ROCHE=o
		else
		echo "Installation dans un autre bahut." ; tput sgr0
		echo ""
		ROCHE=n
		sed -i '/export/d' /etc/profile
		sed -i '/Configuration/d' /etc/profile
		> /etc/apt/apt.conf
		> /etc/apt/apt.conf.d/99proxy

		echo -e "$COLINFO" "Le proxy http a été supprimé dans /etc/profile."
		echo -e "$COLINFO" "Le script va s'interrompre..."
		echo ""
		echo -e "$COLTXT" 
		echo -e "$ROUGE" "Il faut sortir du shell, se relogger (en root) puis relancer le script." ; tput sgr0
		sleep 3
		exit
		fi
 
	fi
fi
#=====================================================

echo -e "$COLDEFAUT" "Utilisez-vous un cache APT pour l'installation ?" ; tput sgr0
echo -e "$COLINFO" "NB : Ce cache peut être différent du cache que vous utilisez en prod." ; tput sgr0
echo -e "$COLINFO" "Il ne sera activé que pour l'installation, puis désactivé à la fin." ; tput sgr0
echo ""
		PS3='Repondre par o ou n: '   # le prompt
		echo ""
		LISTE=("[o] oui" "[n]  non")  # liste de choix disponibles
		echo ""
		select CHOIX in "${LISTE[@]}" ; do
			case $REPLY in
				1|o)
				echo "Cache apt présent pour l'installation (cool!)"
				echo ""
				APTCACHE=o
				break
				;;
				2|n)
				echo "Pas de cache apt pour l'installation (dommage ;)"
				echo ""
				APTCACHE=n
				break
				;;
			esac
		done

if [ $APTCACHE = "o" ] ; then
	# Définition de l'adresse du proxy apt pour l'installation
	defaultcache="192.168.1.251:3142"
	echo -e "$COLINFO" "Vous utilisez un cache APT pour l'installation. " ; tput sgr0
		echo -e "$COLINFO" "Donnez l'adresse IP et le port du cache APT sous la forme 192.168.1.251:3142"
		echo ""
		echo -e "$COLDEFAUT" "Cache par défaut : [$defaultcache]" ; tput sgr0
		echo ""
		echo "Validez par entrée si cette adresse convient, ou entrez une autre adresse (sans oublier le port) :"
		echo ""
		read APTPROXY
		if [ -z "$APTPROXY" ]; then
		APTPROXY=$defaultcache
		fi
		echo -e "$COLDEFAUT" "L'adresse IP du cache apt est $APTPROXY"
		echo -e "$COLTXT"
		
	echo ""
	echo "Ajout temporaire du cache apt à la configuration..."
	echo ""
	# Déclaration du proxy pour apt
	touch /etc/apt/apt.conf.d/99proxy
	echo "#Configuration du cache apt pour l'installation
Acquire::http { Proxy \"http://$APTPROXY\"; };" > /etc/apt/apt.conf.d/99proxy
else
> /etc/apt/apt.conf.d/99proxy
fi


# Désactivation de l'entrée cle usb dans /etc/fstab si le systeme a ete installé avec une clé
sed -i 's/\/dev\/sdb/#\/dev\sdb/g' /etc/fstab

#=================================================
# Création du menu de choix du wm à installer
echo ""
echo -e "$COLDEFAUT" "Quel environnement voulez-vous installer ?" ; tput sgr0
echo ""
	PS3='Repondre par 1, 2, 3, 4 ou 5: '   # le prompt
	LISTE=("lxde" "gnome" "mate" "cinnamon" "xfce")  # liste de choix disponibles
		select CHOIX in "${LISTE[@]}" ; do
			case $REPLY in
				1|lxde)
				echo ""
				echo "Vous avez choisi LXDE"
				LXDE=o
				GNOME=n
				MATE=n
				CINNAMON=n
				XFCE=n
				break
				;;
				2|gnome)
				echo ""
				echo "Vous avez choisi GNOME"
				LXDE=n
				GNOME=o
				MATE=n
				CINNAMON=n
				XFCE=n
				break
				;;
				3|mate)
				echo ""
				echo "Vous avez choisi MATE"
				LXDE=n
				GNOME=n
				MATE=o
				CINNAMON=n
				XFCE=n
				break
				;;
				4|cinnamon)
				echo ""
				echo "Vous avez choisi CINNAMON"
				LXDE=n
				GNOME=n
				MATE=n
				CINNAMON=o
				XFCE=n
				break
				;;
				5|xfce)
				echo ""
				echo "Vous avez choisi XFCE"
				LXDE=n
				GNOME=n
				MATE=n
				CINNAMON=n
				XFCE=o
				break
				;;		
			esac
		done

# Obsolète (lxde n'est plus installé par défaut)
# LXDE sera installé par défaut (en plus d'un autre wm), sauf si $LX vaut 0
#if [ "$LX" == "1" ] ; then
#LXDE=o
#else
#LXDE=n
#fi

# On vérifie qu'au moins un des wm sera installé :
#if [ $LXDE = "n" ]; then
#echo "LXDE ne sera pas installé."
#	if [ $GNOME = "n" -a $MATE = "n" ]; then
#	echo "Attention, aucun wm ne sera installé : paramètre LX=0"
#	echo "Le script ne peut pas continuer avec ces choix."
#	sleep 3
#	exit 1
#	fi
#else
#echo ""
#fi

#=========================================
# Installation des environnements choisis
#=========================================
export DEBIAN_FRONTEND=noninteractive

echo -e "$COLDEFAUT" "C'est parti!" ; tput sgr0
echo -e "$COLINFO" "En fonction de la connexion internet, l'installation prendra de 30 à 90 minutes. " ; tput sgr0
echo -e "$COLINFO" "Soyez patient(e)..." ; tput sgr0
echo ""

apt-get update && apt-get install -y debconf debconf-utils

# LXDE
if [ $LXDE = "o" ] ; then
echo -e "$VERT" "=======================" ; tput sgr0
echo -e "$VERT" "Installation de lxde..." ; tput sgr0
echo -e "$VERT" "=======================" ; tput sgr0
apt-get update
apt-get install --assume-yes lxde
update-alternatives --set x-session-manager /usr/bin/lxsession
fi

# GNOME a été choisi :
if [ $GNOME = "o" ] ; then
echo -e "$VERT" "========================" ; tput sgr0
echo -e "$VERT" "Installation de Gnome..." ; tput sgr0
echo -e "$VERT" "========================" ; tput sgr0
#echo "lightdm shared/default-x-display-manager select lightdm" | debconf-set-selections
apt-get update
apt-get install --assume-yes gnome
update-alternatives --set x-session-manager /usr/bin/gnome-session
fi

# MATE a été choisi :
if [ $MATE = "o" ] ; then
echo -e "$VERT" "=======================" ; tput sgr0
echo -e "$VERT" "Installation de Mate..." ; tput sgr0
echo -e "$VERT" "=======================" ; tput sgr0
echo "lightdm shared/default-x-display-manager select lightdm" | debconf-set-selections
apt-get update
apt-get install --assume-yes mate lightdm
update-alternatives --set x-session-manager /usr/bin/mate-session
fi

# CINNAMON a été choisi :
if [ $CINNAMON = "o" ] ; then
echo -e "$VERT" "===========================" ; tput sgr0
echo -e "$VERT" "Installation de Cinnamon..." ; tput sgr0
echo -e "$VERT" "===========================" ; tput sgr0
echo "lightdm shared/default-x-display-manager select lightdm" | debconf-set-selections
apt-get update
apt-get install --assume-yes cinnamon lightdm
update-alternatives --set x-session-manager /usr/bin/cinnamon-session
fi

# XFCE a été choisi :
if [ $XFCE = "o" ] ; then
echo -e "$VERT" "===========================" ; tput sgr0
echo -e "$VERT" "Installation de XFCE..." ; tput sgr0
echo -e "$VERT" "===========================" ; tput sgr0
echo "lightdm shared/default-x-display-manager select lightdm" | debconf-set-selections
apt-get update
apt-get install --assume-yes task-xfce-desktop lightdm
update-alternatives --set x-session-manager /usr/bin/xfce4-session
fi
# ===========================

# Ajout des dépots et modification des sources.list
echo -e "$VERT" "Modifications des sources..." ; tput sgr0
cp /etc/apt/sources.list /etc/apt/sources.save

echo "deb http://deb.debian.org/debian stretch main contrib non-free
#deb-src http://deb.debian.org/debian stretch main contrib non-free

deb http://security-cdn.debian.org/debian-security/ stretch/updates main contrib non-free

deb http://deb.debian.org/debian stretch-updates main contrib non-free
#deb-src http://deb.debian.org/debian stretch-updates main contrib non-free

# Stretch-backports
# deb http://ftp.debian.org/debian/ stretch-backports main contrib non-free" > /etc/apt/sources.list

echo "# Deb-multimedia
deb http://www.deb-multimedia.org stretch main non-free" > /etc/apt/sources.list.d/deb-multimedia.list

echo "# Java installer
#deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main
#deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" > /etc/apt/sources.list.d/webupd8team-java.list

echo "# Sources se4 
deb [trusted=yes] http://deb.sambaedu.org/debian stretch se4XP" > /etc/apt/sources.list.d/se4.list

# Ajout du dépot pour geogebra 5
echo "# Geogebra
deb http://www.geogebra.net/linux/ stable main" > /etc/apt/sources.list.d/geogebra.list

# L'install des clés rique de ne pas passer s'il y a un cache
if [ $APTCACHE = "o" ] ; then
> /etc/apt/apt.conf.d/99proxy
fi

echo -e "$VERT" "Sources modifiées, mise à jour des paquets..." ; tput sgr0
apt-get update -o Acquire::AllowInsecureRepositories=true
apt-get install --assume-yes dirmngr debconf

# Ajout de clés pour les dépots supplémentaires
echo -e "$VERT" "Installation des clés des nouveau dépots" ; tput sgr0
#apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886
#apt-key adv --keyserver keyserver.ubuntu.com --recv-keys C072A32983A736CF
apt-get install --allow-unauthenticated --assume-yes deb-multimedia-keyring
apt-get update

#========================================================
# Si LXDE est installé, on allège lxsession-logout (options à la déconnexion)
if [ $LXDE = "o" ] ; then
# Modification de lxsession-logout pour ne conserver que l'essentiel
echo -e "$VERT" "Modification de lxsession-logout" ; tput sgr0
echo "Récupération des sources..."
cd
wget --no-check-certificate $FTP2/stretch/lxsession-0.5.3.tar.xz
echo -e "$VERT" "Décompression de l'archive" ; tput sgr0
tar -Jxvf lxsession-0.5.3.tar.xz

echo -e "$VERT" "Modification de lxsession-logout.c" ; tput sgr0
cd /root/lxsession-0.5.3/lxsession-logout/
rm -f lxsession-logout.c
wget --no-check-certificate $FTP2/stretch/lxsession-logout.c

# S'il y a un cache, on le reactive pour la suite
if [ $APTCACHE = "o" ] ; then
echo "Ajout temporaire du proxy apt à la configuration..."
	# Déclaration du proxy pour apt
	touch /etc/apt/apt.conf.d/99proxy
	echo "#Configuration du proxy apt pour l'installation
Acquire::http { Proxy \"http://$APTPROXY\"; };" > /etc/apt/apt.conf.d/99proxy
apt-get update
else
> /etc/apt/apt.conf.d/99proxy
fi

# Installation des paquets pour la compilation des modifs de lxsession-logout
echo -e "$VERT" "=========================================" ; tput sgr0
echo -e "$VERT" "Installation des outils de compilation..." ; tput sgr0
echo -e "$VERT" "=========================================" ; tput sgr0
apt-get install --assume-yes gcc intltool pkg-config xorg-dev glib-2.0 libdbus-glib-1-dev libspice-client-gtk-3.0-dev libpolkit-agent-1-dev valac libunique-dev

# Installation et compilation des modifications de lxsession-logout
echo -e "$VERT" "Installation et compilation des modifications" ; tput sgr0
cd /root/lxsession-0.5.3
./configure
make && make install
rm -rf /root/lxsession*
fi

#==============================
# Installation des logiciels
#==============================

echo -e "$JAUNE" "On commence une longue série d'installations...." ; tput sgr0
echo ""
#echo -e "$VERT" "installation de java 8 derniere version" ; tput sgr0
#echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 boolean true" | debconf-set-selections

if [ $APTCACHE = "o" ] ; then
> /etc/apt/apt.conf.d/99proxy
fi
#apt-get install --assume-yes oracle-java8-installer
if [ $APTCACHE = "o" ] ; then
echo "Acquire::http { Proxy \"http://$APTPROXY\"; };" > /etc/apt/apt.conf.d/99proxy
fi

# Installation des logiciels sur le client
echo -e "$VERT" "Installation de libreoffice" ; tput sgr0
apt-get install --assume-yes libreoffice libreoffice-l10n-fr
echo -e "$VERT" "Installation de firefox esr" ; tput sgr0
apt-get install --assume-yes firefox-esr firefox-esr-l10n-fr
echo -e "$VERT" "Installation d'un tas de trucs divers et variés..." ; tput sgr0
echo ""
echo -e "$JAUNE" "Il y en a pour un moment, vous pouvez aller boire un coup !" ; tput sgr0
apt-get install --assume-yes flashplayer-mozilla vlc gimp audacity lame
apt-get install --assume-yes gstreamer1.0-fluendo-mp3 ffmpeg sox twolame vorbis-tools faad totem

TEST_VERSION=$(uname -a | grep "i686")
if [ ! -n "$TEST_VERSION" ]; then
echo -e "$COLINFO" "Version 64 bits détectée" ; tput sgr0
apt-get install --assume-yes w64codecs
else
echo -e "$COLINFO" "Version 32 bits détectée" ; tput sgr0
apt-get install --assume-yes w32codecs
fi

apt-get install --assume-yes alsa-utils alsamixergui alsa-oss ntpdate ethtool nictools-pci screen apt-listchanges xinit
apt-get install --assume-yes ttf-mscorefonts-installer libdvdcss2 evince anacron unrar unzip tightvncserver net-tools
apt-get install --assume-yes chromium chromium-l10n flashplayer-chromium
apt-get install --assume-yes --allow-unauthenticated openshot freeplane winff tilem
#apt-get install -y --allow-unauthenticated geogebra5
apt-get install --assume-yes firmware-linux-nonfree lshw
echo -e "$VERT" "Ouf, les installations sont terminées !" ; tput sgr0
echo ""
chmod 755 /etc/X11/xinit/xinitrc
echo -e "$JAUNE" "On fait un peu de ménage..." ; tput sgr0
apt-get --assume-yes autoremove
apt-get clean
apt-get autoclean

# Mise à jour finale
apt-get update && apt-get upgrade -y

#echo -e "$VERT" "Desactivation des autres os dans Grub" ; tput sgr0
#chmod -x /etc/grub.d/30_os-prober
#update-grub

# Installation et configuration des mises à jour automatiques
#if [ $MAJAUTO = "o" ] ; then
echo ""
echo -e "$COLINFO" "Installation des maj automatiques..." ; tput sgr0
echo ""
apt-get install --assume-yes unattended-upgrades
cp /usr/share/unattended-upgrades/20auto-upgrades /etc/apt/apt.conf.d/20auto-upgrades
cd /etc/apt/apt.conf.d
rm -f 50unattended-upgrades
wget --no-check-certificate $FTP/stretch/50unattended-upgrades
echo -e "$VERT" "Mises à jour automatiques installées et configurées" ; tput sgr0
echo ""
#fi

# Détermination du nom de l'interface réseau dans /etc/network/interfaces
net1=$(cat /etc/network/interfaces | grep dhcp | grep enp | cut -d' ' -f2 | awk '{ print $1}')
net2=$(cat /etc/network/interfaces | grep dhcp | grep eno | cut -d' ' -f2 | awk '{ print $1}')
net3=$(cat /etc/network/interfaces | grep dhcp | grep eth | cut -d' ' -f2 | awk '{ print $1}')
if [ -n "$net1" ]; then
net=$net1
elif [ -n "$net2" ]; then
net=$net2
elif [ -n "$net3" ]; then
net=$net3
fi

echo -e "$VERT" "Activation de la fonction WOL sur la carte réseau" ; tput sgr0
ethtool -s $net wol g

#sed -i '/exit 0/d' /etc/rc.local
#echo "ethtool -s eth0 wol g"  >> /etc/rc.local
#echo "exit 0" >> /etc/rc.local

# Configuration du clavier par défaut
sed -i 's/us/fr/g' /etc/default/keyboard

# Changement des mots de passe
echo -e "$ROUGE" "Il est temps de changer le mots de passe de root..." ; tput sgr0
echo ""
echo -e "$JAUNE" "Entrez (2 fois) le nouveau mot de passe pour root :" ; tput sgr0
echo ""
passwd

# Script de renommage
cd /root
rm -f renomme_client*
wget --no-check-certificate $FTP/alancer/renomme_client_linux_v3.sh
chmod +x renomme_client_linux_*

# Datage du script (temoin de passage dans /root)
cd /root
rm -f installation_*
echo "Installation le $DATEINST" >> /root/installation_$DATEINST

# On neutralise le cache apt utilisé pour l'installation
echo -e "$COLINFO" "Neutralisation du proxy APT de l'installation" ; tput sgr0
> /etc/apt/apt.conf.d/99proxy

# Messages de fin d'installation
echo ""
echo -e "$VERT" "La configuration automatique est terminée." ; tput sgr0
echo ""
echo -e "$COLINFO" "Pensez à faire les operations qui manquent :" ; tput sgr0
echo -e "$COLINFO" "- Lancement du script de post-integration" ; tput sgr0
echo ""
echo -e "$JAUNE" "La machine redémarrera dans 10 s.... " ; tput sgr0
echo ""
echo -e "$COLINFO" "(ctrl-c si vous voulez annuler le reboot automatique)" ; tput sgr0
echo ""
sleep 10
reboot
