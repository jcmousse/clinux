#!/bin/bash
###########################################################
# se4_copie_scripts_client.sh
#
# Ce script est a lancer sur le se4fs.
# Il copie les 2 scripts d'installation et d'intégration
# sur un client stretch ou buster
# 20191127
##########################################################
DATE1=$(date +%F+%0kh%0M)

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

script01=install_stretch_etabs.sh
script02=rejoint_stretch_se4.sh
script11=install_buster_etabs.sh
script12=rejoint_buster_se4.sh
script21=install_xxx_etabs.sh
script22=rejoint_xxx_se4.sh

dirclinux=/var/sambaedu/clients-linux

echo -e "$JAUNE"
echo "############################################################################################"
echo ""
echo "Voulez-vous copier les scripts d'installation et d'intégration sur un (futur) client Debian ?"
echo "Il faudra donner l'adresse ip du client à installer-intégrer, et son mdp root."
echo ""
echo "############################################################################################"
echo -e "$COLTXT"
echo -e "$COLINFO"
read -p "Que voulez-vous faire ?

1 Je veux installer et intégrer un client STRETCH - Debian 9.x
2 Je veux installer et intégrer un client BUSTER - Debian 10.x
3 Je veux installer et intégrer un client xxx - Debian x
4 Non merci, je déteste les clients linux ! : " rep
echo -e "$COLTXT"
			case $rep in

				1 )
				if [ ! -e $dirclinux/$script01 -o ! -e $dirclinux/$script02 ]; then
				echo -e "$COLDEFAUT"
				echo "========================================================================================="	
				echo "$script01 ou $script02 n'existent pas encore, ou ils ne sont pas présents sur ce serveur."
				echo "Abandon!"
				echo "========================================================================================="
				echo -e "$COLTXT"
				exit 1
				fi
				echo -e "$JAUNE"
                                echo "Entrez l'adresse ip du client STRETCH : "
                                echo -e "$COLTXT"
                                read IPCLI
				echo ""
				echo "Scripts à copier sur le client : $script01 - $script02 "
				echo ""
				scp $dirclinux/$script01 $dirclinux/$script02 root@$IPCLI:/root/ || ERREUR="1"
                                if [ "$ERREUR" = "1" ];then
                                        echo -e "$COLERREUR"
                                        echo "Erreur lors de la copie des scripts sur le client..."
					echo "Vérifier l'adresse ip du client puis relancez le script."
                                        echo -e "$COLTXT"
                                else
                                        echo -e "$VERT"
                                        echo "Les scripts $script01 et $script02 ont été copiés dans /root sur le client Stretch à l'adresse $IPCLI"
					echo -e "$COLTXT"
                                fi
                                ;;
				
				2 )
				if [ ! -e $dirclinux/$script11 -o ! -e $dirclinux/$script12 ]; then
				echo -e "$COLDEFAUT"
				echo "========================================================================================="	
				echo "$script11 ou $script12 n'existent pas encore, ou ils ne sont pas présents sur ce serveur."
				echo "Abandon!"
				echo "========================================================================================="
				echo -e "$COLTXT"
				exit 1
				fi
				echo -e "$JAUNE"
                                echo "Entrez l'adresse ip du client BUSTER : "
                                echo -e "$COLTXT"
                                read IPCLI
				echo ""
				echo "Scripts à copier sur le client : $script11 - $script12 "
				echo ""
				scp $dirclinux/$script11 $dirclinux/$script12 root@$IPCLI:/root/ || ERREUR="1"
                                if [ "$ERREUR" = "1" ];then
                                        echo -e "$COLERREUR"
                                        echo "Erreur lors de la copie des scripts sur le client..."
					echo "Vérifier l'adresse ip du client puis relancez le script."
                                        echo -e "$COLTXT"
                                else
                                        echo -e "$VERT"
                                        echo "Les scripts $script11 et $script12 ont été copiés dans /root sur le client Buster à l'adresse $IPCLI"
					echo -e "$COLTXT"
                                fi
                                ;;

				3 )
				if [ ! -e $dirclinux/$script21 -o ! -e $dirclinux/$script22 ]; then
				echo -e "$COLDEFAUT"
				echo "========================================================================================="	
				echo "$script21 ou $script22 n'existent pas encore, ou ils ne sont pas présents sur ce serveur."
				echo "Abandon!"
				echo "========================================================================================="
				echo -e "$COLTXT"
				exit 1
				fi
				echo -e "$JAUNE"
                                echo "Entrez l'adresse ip du client XXX : "
                                echo -e "$COLTXT"
                                read IPCLI
				echo ""
				echo "Scripts à copier sur le client : $script21 - $script22 "
				echo ""
				scp $dirclinux/$script21 $dirclinux/$script22 root@$IPCLI:/root/ || ERREUR="1"
                                if [ "$ERREUR" = "1" ];then
                                        echo -e "$COLERREUR"
                                        echo "Erreur lors de la copie des scripts sur le client..."
					echo "Vérifier l'adresse ip du client puis relancez le script."
                                        echo -e "$COLTXT"
                                else
                                        echo -e "$VERT"
                                        echo "Les scripts $script21 et $script22 ont été copiés dans /root sur le client xxx à l'adresse $IPCLI"
					echo -e "$COLTXT"
                                fi
                                ;;

				* ) 	
				echo -e "$COLINFO"
				echo "Pas de copie demandée."
				echo -e "$COLTXT"
				echo -e "$VERT"
				echo "###########################################################################"
				echo ""
				echo "Vous pourrez copier les scripts d'installation et d'intégration plus tard"
				echo "en lançant le script $dirclinux/se4_copie_scripts_client.sh"
				echo ""
				echo "###########################################################################"
				echo -e "$COLTXT"
				echo -e "$COLINFO"
				echo "A bientôt !"
				echo -e "$COLTXT" ;;
			esac
exit 0
