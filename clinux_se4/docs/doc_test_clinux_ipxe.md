# Installation et intégration de clients linux à un domaine se4
## Installation automatisée par ipxe et post-intégration
**Notes de tests 20191220**

## Conditions des tests
* Disposer d'un se4ad et d'un se4fs de test.
* Peupler l'annuaire du se4 en important un jeu de fichiers siecle/sts.

## 1-Préparation du se4fs pour les clients linux

**Sur le se4fs :**  
```
apt update && apt install sambaedu-client-linux sambaedu-clonage sambaedu-clonezilla sambaedu-ipxe sambaedu-boot-server  
wget --no-check-certificate https://gitlab.com/jcmousse/clinux/raw/master/clinux_se4/se4_prepare_clients_linux.sh  
chmod +x se4_prepare_clients_linux.sh  
./se4_prepare_clients_linux.sh
```

**Le script va collecter les données et préparer le se4fs pour les clients linux :**
* création de quelques répertoires dans `/var/sambaedu/clients-linux`  
* téléchargement des scripts de connexion-déconnexion des clients  
* configuration des scripts d'installation et de post-intégration des clients  
* ajout d'un partage samba pour montage sur les clients  

* A chaque nouveau lancement de `se4_prepare_clients_linux.sh`, l'existence d'une mise à jour du script sera détectée, et la màj sera téléchargée automatiquement si le script a changé sur le dépot gitlab *(messages explicites)*.  
Il sera aussi possible de modifier certains paramètres (proxy, cache) en relançant le script.  

* Après le premier lancement (et les opérations de mise en place), si se4_prepare_clients_linux.sh est relancé, seules les mises à jour des scripts client auront lieu.  
*→ On peut relancer se4_prepare_clients_linux.sh plusieurs fois sans problème.*  
`. /var/sambaedu/clients-linux/se4_prepare_clients_linux.sh`  
* Il est inutile de redémarrer le se4fs (ou de recharger la conf. samba) dans ce cas.  

> **Il est fortement conseillé de relancer se4_prepare_clients_linux.sh avant chaque opération de test ou d’installation de nouveaux clients.**  Ainsi, les clients bénéficieront de toutes les mises à jour du dispositif « clinux ».  

ATTENTION, à chaque lancement de se4_prepare_clients_linux.sh, tous les scripts destinés aux clients sont remplacés, sauf :  
`/var/sambaedu/clients-linux/conex/createlinks_perso.sh`  
`/var/sambaedu/clients-linux/conex/profil_ff_perso.sh`  
qui ne seront pas écrasés par les mises à jour.  

## 2- Installation automatisée d'un client debian buster :

**Téléchargement de l'iso Debian**  
* Sur le se4, lancer `/usr/share/sambaedu/scripts/install-debian-64-iso.sh`  

**Installation et intégration automatisées d'un client**
*  Démarrer un client en bootant sur le réseau.  
*  Dans le menu ipxe, choisir  « administration », entrer l’identifiant « admin » et son mdp.  
*  Nommer la machine, puis lancer l’une des installations (base, lxde, gnome, mate, xfce, kde).  
*  Le système va s’installer sans intervention, puis être intégré au domaine.  

* -> Les utilisateurs de l'AD peuvent s'authentifier sur le client depuis la mire de connexion.  
* -> Le /home de l'utilisateur sera monté s'il existe déjà sur le se4fs, ou il sera créé à la première connexion.  
(consulter `/var/log/samba/debug.log` sur le se4fs en cas de problème)  
* -> Les partages classes et docs seront montés avec les bons droits sur le client à la connexion d’un utilisateur.  
* -> Si un utilisateur local s’authentifie, les scripts de connexion ne seront pas exécutés.  

**A la première connexion sur un client Lxde, Mate, Cinnamon, Xfce ou Kde :**  
Il faut *sélectionner l’environnement* que vous avez choisi à l’installation :  
* Entrer un identifiant et passer dans le champ du mdp.
* En haut à droite, choisir l’environnement (autre que « Default Xsession»)  
* Entrez le mdp et connectez vous.  
![selection du wm](selection_wm.png)  
*NB : Ceci est à faire une seule fois par client, le réglage sera conservé ensuite.*  


