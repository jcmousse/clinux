#!/bin/bash
# installation se3 phase 2
# franck molle mars 2008
# modification Romuald Jourdin 2011 - Adaptation lenny Debian Installer ver 2012_02_25
# version pour wheezy maj 04-2015
# modif jcm 20150927 pour install sans fichier se3setup.data

cd /root

# Modification des sources
mv /etc/apt/sources.list /etc/apt/sources.list.sav
wget --no-check-certificate https://raw.githubusercontent.com/jcmousse/clinux/master/preseed/se3/sources.list.se3
wget --no-check-certificate https://raw.githubusercontent.com/jcmousse/clinux/master/preseed/se3/se3.list.se3
cat /root/sources.list.se3 > /etc/apt/sources.list
cat /root/se3.list.se3 > /etc/apt/sources.list.d/se3.list

clear

echo "--------------------------------------------------------------------------------"
echo "L'installeur est maintenant sur le point d'installer SambaEdu3."
echo "--------------------------------------------------------------------------------"
echo "Appuyez sur Entree pour continuer"
read dummy

DEBIAN_PRIORITY="critical"
DEBIAN_FRONTEND="noninteractive"
export  DEBIAN_FRONTEND
export  DEBIAN_PRIORITY

echo "Mise à jour des dépots et upgrade si necessaire"

apt-get update
apt-get upgrade --assume-yes

echo "installation de ssmtp et ntpdate"
apt-get install --assume-yes ssmtp
apt-get install --assume-yes ntpdate
apt-get install --assume-yes makepasswd


echo "Installation de wine et Samba 4.1" 

echo "Ajout du support de l'architecture i386 pour dpkg" 

dpkg --add-architecture i386
apt-get -qq update

echo "Installation de Wine:i386" 

apt-get install wine-bin:i386 -y

apt-get install samba smbclient winbind --allow-unauthenticated -y

echo "On stopppe le service winbind" 
service winbind stop
insserv -r winbind

echo "Installation du paquet se3 et de ses dependances"
/usr/bin/apt-get --yes --force-yes install se3 se3-domain se3-logonpy se3-dhcp se3-wpkg

/usr/bin/apt-get --yes remove nscd
apt-get install -y --allow-unauthenticated se3-clients-linux

while [ "$TEST_PASS" != "OK" ]
do
echo -e "Entrez un mot de passe pour le compte super-utilisateur root"
passwd
    if [ $? != 0 ]; then
        echo -e "Attention : mot de passe a été saisi de manière incorrecte"
        echo "Merci de saisir le mot de passe à nouveau"
        sleep 1
    else
        TEST_PASS="OK"
        echo -e "Mot de passe root changé avec succès :)"
        sleep 1
    fi
done

echo "L'installation est terminée. Bonne utilisation de SambaEdu3 !"

. /etc/profile

DEBIAN_PRIORITY="high"
DEBIAN_FRONTEND="dialog" 
export  DEBIAN_PRIORITY
export  DEBIAN_FRONTEND
exit 0

