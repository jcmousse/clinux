#!/bin/bash

# ****************************************************************************
# install_italc-master.sh
#
# Script permettant d'installer "proprement" italc-master sur un client linux,
# 20151211
# ****************************************************************************

DATESCRIPT="20151208"
DATE1=$(date +%F+%0kh%0M)

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# On rend le script "crétin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a exécuter sur les clients Linux, pas sur le serveur !" && exit 1

# Messages de début
echo -e "$COLINFO"
echo "Installation d'italc-master sur ce poste."
echo -e "$COLTXT"
echo -e "$JAUNE"
echo "Répondez soigneusement aux questions de configuration (clés, groupes...voir la doc)."
echo -e "$COLTXT"
sleep 3

# Installation ou mise à jour d'italc-master
echo -e "$COLINFO"
echo "Installation d'italc-master..."
echo -e "$COLTXT"
apt-get update
apt-get install -y italc-master

# Modification du fichier italc.conf pour les autorisations
echo -e "$COLINFO"
echo "Modification du fichier /etc/italc/italc.conf : autorisations."
echo -e "$COLTXT"
sed -i 's/Authentication=1/Authentication=0/g' /etc/xdg/iTALC\ Solutions/iTALC.conf

# Recherche du parc dans lequel le poste est installé
parc1=$(cat /etc/parc.txt)
echo -e "$JAUNE"
echo "*********************************************************"
echo "Ce poste est actuellement inscrit dans le parc $parc1"
echo "Est-ce correct ?"
echo "*********************************************************"
echo -e "$COLTXT"
echo -e "$COLINFO"
echo "Si le poste est bien dans le parc $parc1, appuyer sur entrée pour continuer."
echo ""
echo "Sinon, sortez du script avec ctrl-c."
echo ""
read lulu
echo -e "$COLTXT"

# On sauvegarde la configuration actuelle...
echo -e "$COLINFO"
echo "Sauvegarde de la configuration actuelle..."
echo -e "$COLTXT"
cd /etc/xdg/iTALC\ Solutions
cp iTALC.conf italc.conf_$DATE1

# Modification de l'emplacement de GlobalConfig.xml dans italc.conf
echo -e "$COLINFO"
echo "Modification du fichier iTALC.conf pour le fichier GlobalConfig.xml"
echo -e "$COLTXT"
repitalc="/media/Partages/italc/$parc1"
sed -i 's#^GlobalConfiguration.*#GlobalConfiguration='$repitalc'\/GlobalConfig.xml#g' /etc/xdg/iTALC\ Solutions/iTALC.conf

# On configure les menus pour qu'italc-master apparaisse pour l'utilisateur
echo -e "$COLINFO"
echo "Configuration des menus..."
echo -e "$COLTXT"
cd /usr/share/applications
rm -f italc-*
cp /var/lib/menu-xdg/applications/menu-xdg/X-Debian-Applications-Education-italc.desktop .
mv X-Debian-Applications-Education-italc.desktop italc.desktop
sed -i 's/Categories=X-Debian-Applications-Education;/Categories=Education;X-Debian-Applications-Education;/g' /usr/share/applications/italc.desktop
update-menus

# Message de fin
echo -e "$VERT"
echo "Installation d'italc-master terminée sur ce poste."
echo -e "$COLTXT"

