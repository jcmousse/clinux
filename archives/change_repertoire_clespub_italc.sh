#!/bin/bash
#
#**************************************************************************
# change_repertoire_clepub_italc.sh
# Script de changement du répertoire de dépot des clés publiques italc
# pour les clients
# 20151214
#**************************************************************************

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# On rend le script "cretin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a exécuter sur les clients Linux, pas sur le serveur !" && exit 1

# Répertoire de base pour le dépot des clés :
# sur le se3 : /home/netlogon/clients-linux/divers/italc/
# sur les clients : /mnt/netlogon/divers/italc/
repitalc=/mnt/netlogon/divers/italc

echo -e "$COLINFO"
echo "******************************************************************"
echo "Ce script va reconfigurer le client italc pour qu'il utilise"
echo "un répertoire partagé pour les clés publiques."
echo ""
echo "Attention, le répertoire /home/netlogon/clients-linux/divers/italc/<parc>/public"
echo "devra être créé sur le se3."
echo ""
echo "Appuyez sur entrée pour continuer."
echo "******************************************************************"
echo -e "$COLTXT"
read zutalor

# Choix de l'emplacement partagé des clés
echo -e "$COLINFO"
echo "Quel répertoire voulez-vous utiliser pour les clés publiques d'italc ?"
echo ""
echo "Indiquez un nom de parc ou de salle : cdi, c4, ..."
echo "et n'oubliez pas de créer le répertoire correspondant sur le se3 !"
echo -e "$COLTXT"
read repcle
repitalc="/mnt/netlogon/divers/italc/$repcle/public"
sed -i 's#^PublicKeyBase.*#PublicKeyBaseDir='$repitalc'#' /etc/xdg/iTALC\ Solutions/iTALC.conf

# Fin
echo -e "$VERT"
echo "La configuration du client a été modifiée."
echo ""
echo "Il faudra déposer les clés publiques sur le se3 dans le répertoire"
echo "/home/netlogon/clients-linux/divers/italc/$repcle/public"
echo ""
echo "Terminé!"
echo -e "$COLTXT"
