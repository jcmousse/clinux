#!/bin/bash
#
# 
#*********************************************************************
# test_nom_pc.sh
# Recherche du nom d'un client linux dans un fichier
# 
#*********************************************************************
# Section a completer
DATESCRIPT="20190206"
VERSION="1.1"

DATE1=$(date +%F+%0kh%0M)
DATE2=$(date +%F)

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# On rend le script "cretin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a exécuter sur les clients Linux, pas sur le serveur !" && exit 1

# Le fichier de correspondances nom - adresses mac
#fichiercsv=/home/jc/Bureau/ent_2018-19/support/Support_LRA/dhcp.csv
fichiercsv="/root/dhcp.csv"

# On convertit le contenu du fichier en minuscules
sed -i -e 's/.*/\L&/' $fichiercsv

# Nom de l'interface câblée
netint=$(ip a | grep BROAD | grep eth | cut -d: -f2 | awk '{ print $1}')

# Adresse mac de l'interface
macad=$(ifconfig $netint | grep ether | cut -d' ' -f10 | awk '{ print $1}')

# Test de l'existence du fichier de noms
if [ -e $fichiercsv ]; then
# Recherche de correspondance de l'adresse mac dans le fichier, extraction du nom associé
echo ""
echo "Recherche de cette machine dans le fichier..."
echo ""
nompc=$(cat $fichiercsv | grep $macad | cut -d';' -f2 | awk '{ print $1}')
	if [ ! -z "$nompc" ]; then
	echo "[$nompc] est le nom qui a été trouve dans le fichier."
	echo ""
	echo "- Si ce nom convient, appuyez simplement sur <entrée>"
	echo "- Si vous voulez changer de nom, entrez le nouveau nom et validez."
	echo ""
	read NOMMACH
		if [ -z "$NOMMACH" ]; then
		NOMMACH=$nompc
		fi
	else
	echo "Aucune correspondance trouvée dans le fichier !"
	echo "Entrez un nom pour cette machine :"
	read NOMMACH
	fi
else
	echo -e "$COLDEFAUT"
	echo "Le fichier de correspondances noms-adresses mac est absent."
	echo ""
	echo "Nom actuel de la machine : $(hostname)"
	echo -e "$COLTXT"
	echo ""
	echo "L'adresse mac de ce poste est $macad"
	echo ""
# Choix du nouveau nom de la machine
	echo "Choix du nouveau nom de cette machine :"
	echo ""
	echo "Quel nom choisissez-vous pour cette machine ?"
	read NOMMACH
	echo "Le nom choisi pour la machine est $NOMMACH"
fi
echo ""
echo "Cette machine aurait été renommée en $NOMMACH."
echo ""
echo "Préparation du fichier de réservations..."
# recup de l'adresse ip actuelle du poste
ipcli=$(ifconfig eth0 | grep "inet " | awk '{ print $2}')
#============================================================================
# Collecte des infos dans un fichier pour préparation des réservations
line1="$ipcli;$NOMMACH;$macad"
echo $line1 >> /root/resas_$DATE2.csv
#============================================================================
echo "Terminé!"
echo ""
