#!/bin/bash
# **********************************************************
# conv_jessie_stretch.sh
# Script de conversion des clients Jessie en Stretch
# 20170919
# **********************************************************

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

FTP=https://raw.githubusercontent.com/jcmousse/clinux/master

# On rend le script "cretin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a executer sur les clients Linux, pas sur le serveur !" && exit 1

# Test de la version Debian du client
TEST_VERSION=$(cat /etc/debian_version | grep "8.")
if [ ! -n "$TEST_VERSION" ]; then
echo "Ce script est destiné à un système Debian 8 (Jessie)."
echo "Il n'est pas adapté à ce système."
exit
else
echo "Debian 8 (Jessie) détectée... on poursuit."
fi

# Avertissements initiaux
echo -e "$COLDEFAUT"
echo "Ce script va convertir ce client Jessie en client Stretch."
echo -e "$COLINFO"
echo "Etape 1 : Conversion des fichiers sources.list"
echo "Etape 2 : Mise à jour des paquets essentiels."
echo "Etape 3 : Migration complète du système."
echo ""
echo -e "$COLDEFAUT"
echo "Peut-on lancer la migration?"
echo "Appuyez sur entrée quand vous voulez!"
echo -e "$COLTXT"
read LULU
echo -e "$COLTXT"

# Si les backports sont présents dans sources.list, on les désactive
TEST_BACKPORTS=$(cat /etc/apt/sources.list | grep "jessie-backports")
	if [ -n "$TEST_BACKPORTS" ] ; then
	sed -i 's/^deb\ http:\/\/ftp.debian.org\/debian\/jessie-backports/#deb\ http:\/\/ftp.debian.org\/debian\/jessie-backports/g' /etc/apt/sources.list
	echo -e "$COLINFO"
	echo "Desactivation des backports dans sources.list : fait"
	echo ""
	echo -e "$COLTXT"
	fi

# Modification des sources
	sed -i 's/jessie/stretch/g' /etc/apt/sources.list
	sed -i 's/Jessie/Stretch/g' /etc/apt/sources.list
	sed -i 's/jessie/stretch/g' /etc/apt/sources.list.d/deb-multimedia.list
	echo -e "$COLINFO"
	echo "sources.list modifié."
	echo ""
	echo -e "$COLTXT"

# Mise à jour des paquets avec les sources stretch
echo -e "$COLINFO"
	echo "Mise à jour des paquets..."
	echo ""
	echo -e "$COLTXT"
apt-get update

# Safe-upgrade des paquets
echo -e "$COLINFO"
	echo "Phase 1 : safe-upgrade du système..."
	echo "Peut-on continuer?"
	echo ""
	echo "Appuyez sur entrée..."
	read LOLO
	echo -e "$COLTXT"
#apt-get install -y kdenlive
apt-get upgrade

# Modification de la conf de unattended-upgrades
cd /etc/apt/apt.conf.d
rm -f 50unattended-upgrades
wget --no-check-certificate $FTP/stretch/50unattended-upgrades


# On termine le boulot
echo -e "$COLINFO"
	echo "Phase 2 : mise à jour totale du système..."
	echo "Peut-on continuer?"
	echo ""
	echo "Appuyez sur entrée..."
	read LILI
	echo -e "$COLTXT"
apt-get dist-upgrade

# Nettoyage avant de partir...
echo -e "$COLINFO"
	echo "On fait le ménage..."
	echo ""
	echo -e "$COLTXT"
apt-get -y autoremove
apt-get autoclean

echo -e "$VERT"
	echo "Le système de ce client a été mis à jour en version Stretch."
	echo ""
	echo -e "$COLDEFAUT"
	echo "Redémarrez le système!"
	echo -e "$COLTXT"
