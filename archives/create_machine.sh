#!/bin/bash

#*************************************************************************
# create_machine.sh
# Creation d'un compte machine dans l'annuaire du se3 pour les clients linux
# jc 20150708
#*************************************************************************

# On rend le script "cretin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a executer sur les clients Linux, pas sur le serveur !" && exit 1
SE3_IP="##se3ip##"

# On récupère l'ip du client
TEST_ADR=$(ifconfig eth0 | grep "inet adr")
if [ -n "$TEST_ADR" ]; then
# ifconfig contient inet adr
ipcli=$(ifconfig eth0 | grep inet\ adr | cut -d: -f2 | awk '{ print $1}')
else
# ifconfig contient inet addr
ipcli=$(ifconfig eth0 | grep inet\ addr | cut -d: -f2 | awk '{ print $1}')
fi

# On récupère le nom du client
nomcli=$(hostname)

# Lancement de machineAdd sur le se3
echo "Le mot de passe qui va être demandé est le root du se3 : "
ssh root@$SE3_IP -o StrictHostKeyChecking=no '/usr/share/se3/sbin/machineAdd.pl '$nomcli\$' '$ipcli''

echo "Le mot de passe qui va être demandé est l'admin du domaine : "
net join -S $SE3_IP -U admin

echo "Terminé!"
echo "La machine possède maintenant un compte dans l'annuaire du se3."
echo "La machine redémarrera dans 5 sec...."
sleep 5
reboot
exit 0
