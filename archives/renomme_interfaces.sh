#!/bin/bash
# **********************************************************
# Renommage de l'interface réseau cablée
# 20181221
# Destiné aux versions >= 9
# **********************************************************

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# On rend le script "cretin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a exécuter sur les clients Linux, pas sur le serveur !" && exit 1

# Ces commandes permettent de retrouver les noms "ethx" et "wlanx" pour les interfaces réseau
#sed -i 's/GRUB_CMDLINE_LINUX=\"\"/GRUB_CMDLINE_LINUX=\"net.ifnames=0 biosdevname=0\"/g' /etc/default/grub
#update-grub

# Test de la version Debian du client
TEST_VERSION=$(cat /etc/debian_version | grep "9.")
if [ ! -n "$TEST_VERSION" ]; then
echo -e "$COLDEFAUT"
echo "Ce script est destiné à un système Debian 9 ou 10 (Stretch ou Buster)."
echo "Il n'est pas adapté à ce système."
echo -e "$COLTXT"
exit 1
else
echo -e "$VERT"
echo "Version correcte... on poursuit."
echo -e "$COLTXT"
fi

apt-get install net-tools

# Détermination du nom de l'interface actuelle
oldnet1=$(ip a | grep BROAD | grep enp | cut -d: -f2 | awk '{ print $1}')
	if [ ! -n "$oldnet1" ]; then
oldnet1=$(ip a | grep BROAD | grep eno | cut -d: -f2 | awk '{ print $1}')
echo ""
echo "Le nom actuel de l'interface est $oldnet1"
echo ""
	else
echo ""
echo  "Le nom actuel de l'interface est $oldnet1"
echo ""
	fi

echo ""
echo "Correction de /etc/default/grub pour le nom de l'interface réseau cablée"
echo ""
sed -i 's/GRUB_CMDLINE_LINUX=\"\"/GRUB_CMDLINE_LINUX=\"net.ifnames=0 biosdevname=0\"/g' /etc/default/grub
update-grub

echo ""
echo "Correction du fichier /etc/network/interfaces"
echo ""
sed -i 's/'$oldnet1'/eth0/' /etc/network/interfaces
echo ""
echo "Terminé. L'interface réseau a été renommée en eth0"
echo ""
