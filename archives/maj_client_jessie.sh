#!/bin/bash
# **********************************************************
# maj_client_jessie.sh
# Script de maj des clients Jessie
# 20180328
# **********************************************************

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# On rend le script "cretin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a executer sur les clients Linux, pas sur le serveur !" && exit 1

# Test de la version Debian du client
TEST_VERSION=$(cat /etc/debian_version | grep "8.")
if [ ! -n "$TEST_VERSION" ]; then
echo -e "$COLDEFAUT"
echo "Ce script est destiné à un système Debian 8 (Jessie)."
echo "Il n'est pas adapté à ce système."
echo -e "$COLTXT"
exit 1
else
echo -e "$VERT"
echo "Debian 8 (Jessie) détectée... on poursuit."
echo -e "$COLTXT"
fi

echo -e "$ROUGE" "Utilisez-vous un cache APT pour les mises à jour ?" ; tput sgr0
		PS3='Repondre par o ou n: '   # le prompt
		LISTE=("[o] oui" "[n]  non")  # liste de choix disponibles
		select CHOIX in "${LISTE[@]}" ; do
			case $REPLY in
				1|o)
				echo "Cache apt présent pour les màj (cool!)"
				APTCACHE=o
				break
				;;
				2|n)
				echo "Pas de cache apt pour les màj (dommage ;)"
				APTCACHE=n
				break
				;;
			esac
		done

if [ $APTCACHE = "o" ] ; then
# Cache activé dans la conf apt, témoin positionné
echo "#Configuration du proxy pour l'établissement
Acquire::http { Proxy \"http://##ipcache##:3142\"; };" > /etc/apt/apt.conf.d/99proxy
# Ecriture du témoin d'état du cache sur le client
echo "Avec cache" > /etc/monproxy
else
# Cache désactivé dans la conf apt, témoin positionné
echo "Neutralisation du proxy apt dans la configuration..."
> /etc/apt/apt.conf.d/99proxy
# Ecriture du témoin d'état du cache sur le client
echo "Sans cache" > /etc/monproxy
fi

# On désinstalle java8 qui commence à être lourdaud!
apt-get purge -y oracle-java8-installer

# On supprime le depot de oracle-java8-installer
> /etc/apt/sources.list.d/webupd8team-java.list 

# On règle les problèmes de mise à jour automatiques inachevées...
dpkg --configure -a
apt-get update
apt-get install -f -y

# Il y a parfois des problèmes avec les màj de vlc...
echo -e "$COLINFO"
echo "Installation ou mise à jour de vlc..."
echo -e "$COLTXT"
apt-get install -y vlc vlc-data
echo -e "$COLINFO"

# Mise à jour de FF
echo "Installation ou mise à jour de firefox en version esr..."
echo -e "$COLTXT"
apt-get install -y firefox-esr firefox-esr-l10n-fr
echo -e "$VERT"
echo "Installation de firefox en version esr terminée."
echo -e "$COLTXT"

# On met à jour les lecteurs flash
echo -e "$COLINFO"
echo "Mise à jour des lecteurs flash pour firefox et chromium..."
echo -e "$COLTXT"
apt-get install -y flashplayer-mozilla flashplayer-chromium

# On en profite pour mettre à jour le client
echo -e "$COLINFO"
echo "Mise à jour de la machine..."
echo ""
echo -e "$COLTXT"
apt-get -y upgrade && apt-get -y dist-upgrade

# Menage sur le poste et mise à jour de la liste des paquets
apt-get autoremove -y
apt-get autoclean
apt-get update

# Message de fin
echo -e "$VERT"
echo "Terminé !"
echo -e "$COLTXT"
