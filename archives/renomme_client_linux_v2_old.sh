#!/bin/bash
#
# 
#*********************************************************************
# renomme_client_linux_v2.sh
# Ce script permet de renommer un client linux
# Correction de l'uuid de la partition swap après clonage en fin de script
#*********************************************************************
# Section a completer
DATESCRIPT="20190204"
VERSION="2.0"

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# On rend le script "cretin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a exécuter sur les clients Linux, pas sur le serveur !" && exit 1

echo -e "$COLINFO"
echo "Renommage du client..."
echo -e "$COLTXT"

# Le fichier de correspondances nom - adresses mac
fichiercsv="/root/dhcp.csv"

# Nom de l'interface câblée
netint=$(ip a | grep BROAD | grep eth | cut -d: -f2 | awk '{ print $1}')

# Adresse mac de l'interface
macad=$(ifconfig $netint | grep ether | cut -d' ' -f10 | awk '{ print $1}')

# Test de l'existence du fichier de noms
if [ -e $fichiercsv ]; then
# On convertit le contenu du fichier en minuscules
sed -i -e 's/.*/\L&/' $fichiercsv
# Recherche de correspondance de l'adresse mac dans le fichier et extraction du nom associé
echo ""
echo "Recherche de cette machine dans le fichier..."
echo ""
nompc=$(cat $fichiercsv | grep $macad | cut -d';' -f2 | awk '{ print $1}')
	if [ ! -z "$nompc" ]; then
	echo -e "$COLDEFAUT"
	echo "[$nompc] est le nom qui a été trouvé dans le fichier de correspondances."
	echo ""
	echo -e "$COLINFO"
	echo "- Si ce nom convient, appuyez simplement sur <entrée>"
	echo "- Si vous voulez changer de nom, entrez le nouveau nom et validez."
	echo -e "$COLTXT"
	echo ""
	read NOMMACH
		if [ -z "$NOMMACH" ]; then
		NOMMACH=$nompc
		fi
	else
	echo "Aucune correspondance trouvée dans le fichier !"
	echo "Entrez un nom pour cette machine :"
	read NOMMACH
	fi
else
	echo -e "$COLDEFAUT"
	echo "Le fichier $fichiercsv de correspondances noms-adresses mac est absent."
	echo ""
	echo -e "$COLINFO"
	echo "Nom actuel de la machine : $(hostname)"
	echo "L'adresse mac de ce poste est $macad"
	echo -e "$COLTXT"
	echo ""
# Choix du nouveau nom de la machine
	echo "Choix du nouveau nom de cette machine :"
	echo ""
	echo -e "$COLDEFAUT"
	echo "Quel nom choisissez-vous pour cette machine ?"
	read NOMMACH
	echo -e "$COLINFO"
	echo "Le nom choisi pour la machine est $NOMMACH"
	echo -e "$COLTXT"
fi
echo ""
echo -e "$COLINFO"
echo "Cette machine va être renommée en $NOMMACH."
echo -e "$COLTXT"
#============================================================================
# On va afficher l'adresse mac du poste
#apt-get install net-tools
#netint=$(ip a | grep BROAD | grep eth | cut -d: -f2 | awk '{ print $1}')
#macad=$(ifconfig $netint | grep ether | cut -d' ' -f10 | awk '{ print $1}')

#echo -e "$COLDEFAUT"
#echo "Nom actuel de la machine : $(hostname)"
#echo -e "$COLTXT"
#echo ""
#echo "L'adresse mac de ce poste est $macad"
#echo ""


# Choix du nouveau nom de la machine
#		echo "Choix du nouveau nom de cette machine :"
#		echo ""
#		echo "Quel nom choisissez-vous pour cette machine ?"
#		read NOMMACH
#		echo "Le nom choisi pour la machine est $NOMMACH"
#=============================================================================

# Choix du RNE de l'etablissement
#                echo "Choix du RNE de l'etablissement : "
#                echo ""
#                echo "Quel est le RNE de l'etablissement ? "
#                read RNE
#                echo "Le RNE est $RNE"

# Modification du nom de la machine dans smb.conf
sed -i 's/^netbios name.*/netbios\ name\ =\ '$NOMMACH'/g' /etc/samba/smb.conf

# Recuperation de la date et de l'heure pour la sauvegarde des fichiers
DATE=$(date +%F+%0kh%0M)

# Configuration du fichier /etc/hosts
echo -e "$COLINFO"
echo "Configuration du fichier /etc/hosts"
echo -e "$COLTXT"

ANCIENNOM=$(hostname)
RNE=##se3rne##
cp /etc/hosts /etc/hosts_sauve_$DATE
#sed -i 's/'$ANCIENNOM'/'$NOMMACH'/g' /etc/hosts
sed -i 's/^127.0.1.1.*/127.0.1.1\	'$NOMMACH'.'$RNE'.ac-clermont.fr\	'$NOMMACH'/g' /etc/hosts
cp /etc/hostname /etc/hostname_sauve_$DATE
echo "$NOMMACH" > /etc/hostname

# Datage du script (temoin de passage dans /root)
cd /root
rm -f renommage_*
echo "Script de renommage version $VERSION du $DATESCRIPT passe le $DATE" >> /root/renommage_$VERSION_$DATESCRIPT

# Si la machine a été clonée, il faut corriger l'uuid de la partition swap
echo -e "$COLINFO"
echo "Correction de l'uuid de la partition swap après clonage..."
echo -e "$COLTXT"
# On recupère l'uuid correct
blkid | grep 'swap' | cut -d= -f2 | awk '{ print $1}' > /etc/swapuuid
sed -i 's/"//g' /etc/swapuuid
swuid=$(cat /etc/swapuuid)
echo "identifiant correct : $swuid"

# On recupere l'uuid contenu dans /etc/fstab
cat /etc/fstab | grep 'swap' | cut -d= -f2 | awk '{ print $1}' > /etc/wronguuid
sed -i 's/#//g' /etc/wronguuid
sed -i '/^$/d' /etc/wronguuid
wrong=$(cat /etc/wronguuid)
echo "identifiant à corriger : $wrong"
echo "contenu de resume : $(cat /etc/initramfs-tools/conf.d/resume)"

# On recupere l'uuid contenu dans /etc/initramfs-tools/conf.d/resume
#cat /etc/initramfs-tools/conf.d/resume | grep 'swap' | cut -d= -f2 | awk '{ print $1}' > /etc/wronguuid2
#sed -i 's/#//g' /etc/wronguuid2
#sed -i '/^$/d' /etc/wronguuid2
#wrong2=$(cat /etc/wronguuid2)
#echo "identifiant à corriger : $wrong2"

# On remplace par la bonne valeur
sed -i 's/'$wrong'/'$swuid'/g' /etc/fstab
echo "RESUME=UUID=$swuid" > /etc/initramfs-tools/conf.d/resume
#sed -i 's/'$wrong2'/'$swuid'/g' /etc/initramfs-tools/conf.d/resume
#swaplabel -U $swuid $perswap
echo -e "$COLINFO"
echo "On lance update-initramfs..."
echo -e "$COLTXT"
update-initramfs -u
echo -e "$COLDEFAUT"
echo "Correction uuid swap terminée !"
echo -e "$COLTXT"

# Fin de la configuration
echo -e "$VERT"
echo "Fin de l'opération."
echo "Il faut redemarrer la machine pour prendre en compte le nouveau nom !"
echo -e "$COLTXT"

