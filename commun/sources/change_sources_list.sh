#!/bin/bash
# **********************************************************
# Changement du sources.list des clients jessie et stretch
# 20181213
# **********************************************************
DATE1=$(date +%F+%0kh%0M)
FTP=https://raw.githubusercontent.com/jcmousse/clinux/master

ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# Test pour les distraits
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a exécuter sur les clients Linux, pas sur le serveur !" && exit 1

# On sauvegarde sources.list
cp /etc/apt/sources.list /etc/apt/sources.liste_$DATE1

# Test de la version Debian du client
TEST_VERSION=$(cat /etc/debian_version | grep "8.")
if [ ! -n "$TEST_VERSION" ]; then
# sources.list stretch
echo ""
echo -e "$VERT" "Stretch détectée." ; tput sgr0
echo ""
echo "deb http://deb.debian.org/debian stretch main contrib non-free
#deb-src http://deb.debian.org/debian stretch main contrib non-free

deb http://security-cdn.debian.org/debian-security/ jessie/updates main contrib non-free
#deb http://deb.debian.org/debian-security/ stretch/updates main contrib non-free
#deb-src http://deb.debian.org/debian-security/ stretch/updates main contrib non-free

deb http://deb.debian.org/debian stretch-updates main contrib non-free
#deb-src http://deb.debian.org/debian stretch-updates main contrib non-free" > /etc/apt/sources.list

else

# sources.list jessie
echo ""
echo -e "$VERT" "Jessie détectée." ; tput sgr0
echo ""
echo "deb http://deb.debian.org/debian jessie main contrib non-free
#deb-src http://deb.debian.org/debian jessie main contrib non-free

deb http://security-cdn.debian.org/debian-security/ jessie/updates main contrib non-free
#deb http://deb.debian.org/debian-security/ jessie/updates main contrib non-free
#deb-src http://deb.debian.org/debian-security/ jessie/updates main contrib non-free

deb http://deb.debian.org/debian jessie-updates main contrib non-free
#deb-src http://deb.debian.org/debian jessie-updates main contrib non-free

# Jessie-backports
# deb http://ftp.debian.org/debian/ jessie-backports main contrib non-free" > /etc/apt/sources.list
fi

# Mise à jour des paquets après le changement des sources
echo ""
echo -e "$VERT" "Sources modifiées, mise à jour des paquets..." ; tput sgr0
echo ""
apt-get update

echo ""
echo -e "$VERT" "La configuration des sources est terminée." ; tput sgr0
echo ""
