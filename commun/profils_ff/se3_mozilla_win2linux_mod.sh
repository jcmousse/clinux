#!/bin/bash
#
##### Script de creation des liens pour disposer des memes profils Firefox sous Windows et Linux #####
# Auteur: Stephane Boireau
# Mofifie par jc mousseron pour :
# - Traitement des seuls profils FF seulement
# - Suppression du choix de profil FF au lancement
# - Synchronisation des repertoires profil/default linux et windoze
#
## $Id$ ##
#
# Derniere modification: 20150101

echo_debug ()
{
	# Passer la variable a 1 pour afficher des messages de debug en cours de traitement
	debug=1
	if [ "$debug" = "1" ]; then
		echo $*
	fi
}

ladate=$(date +%Y%m%d%H%M%S)

# Ajout : avant de commencer, on synchronise les profils FF "default" s'ils existent
# (et on crée le repertoire profil FF windoze s'il n'existe pas)
ls /home|egrep -v "(^_|^netlogon$|^templates$|^aquota.user$|^aquota.group$|^profiles$)"|while read A ; do

if [ ! -e "/home/$A/profil/appdata/Mozilla/Firefox/Profiles/default" ]; then
mkdir -p /home/$A/profil/appdata/Mozilla/Firefox/Profiles/default
fi

if [ -e "/home/$A/.mozilla/firefox/default" ]; then
rsync -a /home/$A/.mozilla/firefox/default/ /home/$A/profil/appdata/Mozilla/Firefox/Profiles/default/
rsync -a /home/$A/profil/appdata/Mozilla/Firefox/Profiles/default/ /home/$A/.mozilla/firefox/default/
fi
done

ls /home/|while read A
do
	t=$(ldapsearch -xLLL uid=$A)
	if [ -n "$t" ]; then
		echo_debug "Compte : $A"

		# Traitement de Firefox
		echo_debug "Traitement de Firefox pour $A "
		cd /home/$A
		if [ ! -e .mozilla/firefox/profiles.ini ]; then
			# Il n'existe pas encore de profil Firefox pour Linux
			echo_debug "Il n'existe pas encore de profil Firefox linux pour $A"
			mkdir -p .mozilla/firefox
			cd .mozilla/firefox
			ln -s ../../profil/appdata/Mozilla/Firefox/Profiles/default ./
			echo "[General]
StartWithLastProfile=1

[Profile0]
Name=default
IsRelative=1
Path=default" > profiles.ini
			chown -R $A:lcs-users /home/$A/.mozilla
		else
			# Il existe un ou des profils Firefox pour Linux
			echo_debug "Il existe un ou des profils Firefox pour Linux"
			if grep -q "Path=default$" .mozilla/firefox/profiles.ini; then
				# Il existe deja un profil pointant vers un dossier "default"
				echo_debug "On ne corrige pas le .mozilla/firefox/profiles.ini pour $A"
			else
			if grep -q "Name=default$" .mozilla/firefox/profiles.ini; then
					# Il existe deja un profil nomme default, mais son Path n'est pas default
					# On va juste donner un autre Nom pour le profil

					# On cherche le nombre de profiles et surtout un indice libre
					echo_debug "On cherche le nombre de profiles et surtout un indice libre"
					num=$(grep "^\[Profile" .mozilla/firefox/profiles.ini | sed -e "s|[^0-9]||g" | sort -n |tail -n 1)
					num=$(($num+1))

					echo "[Profile$num]
Name=default_${ladate}
IsRelative=1
Path=default" >> .mozilla/firefox/profiles.ini

					# On (ne) bascule (pas) en mode Choix du profil au lancement:
					echo_debug "On (ne) bascule (plus) en mode Choix du profil au lancement"
					#sed -i "s|StartWithLastProfile=1|StartWithLastProfile=0|" .mozilla/firefox/profiles.ini
				else
					# On cherche le nombre de profiles et surtout un indice libre
					echo_debug "On (ne) cherche (plus) le nombre de profiles et un indice libre"
					num=$(grep "^\[Profile" .mozilla/firefox/profiles.ini | sed -e "s|[^0-9]||g" | sort -n |tail -n 1)
					num=$(($num+1))

					echo "[Profile$num]
Name=default
IsRelative=1
Path=default" >> .mozilla/firefox/profiles.ini

					# On bascule en mode Choix du profil au lancement:
					echo_debug "On (ne) bascule (plus) en mode Choix du profil au lancement"
					# sed -i "s|StartWithLastProfile=1|StartWithLastProfile=0|" .mozilla/firefox/profiles.ini
				fi

				cd .mozilla/firefox
				ln -s ../../profil/appdata/Mozilla/Firefox/Profiles/default ./
			fi
		fi
done
