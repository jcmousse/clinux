#!/bin/bash

# ****************************************************************
# se3_clean_linux_profiles.sh
# Script de nettoyage de tous les profils linux utilisateurs
# A lancer sur le se3.
# 20190429
# ****************************************************************

DATE1=$(date +%F+%0kh%0M)

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu


echo ""
echo "C'est parti! Il y en a pour un moment..."
echo ""
ls /home|egrep -v "(^_|^netlogon$|^templates$|^aquota.user$|^aquota.group$|^profiles$|^lost\+found$)"|while read A ; do rm -rf /home/$A/Desktop ; rm -rf /home/$A/.adobe ; rm -rf /home/$A/.cache ;  rm -rf /home/$A/.config/lxsession ; rm -rf /home/$A/.config/lxpanel ; rm -rf /home/$A/.config/pcmanfm ; rm -rf /home/$A/.dbus ; rm -rf /home/$A/.dmrc ; rm -rf /home/$A/.fontconfig ; rm -rf /home/$A/.gconf ; rm -rf /home/$A/.gconfd ; rm -rf /home/$A/.gnome2 ; rm -rf /home/$A/.gksu* ; rm -rf /home/$A/.gstreamer* ; rm -rf /home/$A/.gtk-bookmarks ; rm -rf /home/$A/.gvfs ; rm -rf /home/$A/.java ; rm -rf /home/$A/.kde ; rm -rf /home/$A/.local ; rm -rf /home/$A/.macromedia ; rm -rf /home/$A/.pki ; rm -rf /home/$A/.pulse ; rm -rf /home/$A/.thumbnails ; done
echo ""
echo "Traitement terminé."
echo "Tous les profils ont été nettoyés."
echo ""
exit 0

