#!/bin/bash
#
# Script de nettoyage des classes obsolètes (archivage des dossiers élèves) et des classes actives
# jcm
# 20180829
#

ladate=$(date +%Y%m%d_%H%M)

# Creation du dossier d'archivage
rep_trash=/var/se3/Classes/_Trash_2019

if [ ! -e $rep_trash ]; then
mkdir -p $rep_trash
echo ""
echo "*** Dossier $rep_trash créé. ***"
echo ""
else
echo ""
echo "*** Le dossier $rep_trash existe déjà. On continue. ***"
echo ""
fi

# Nettoyage des classes obsoletes .Classe_xxx
echo ""
echo "============================================="
echo "Traitement des classes obsolètes cachées..."
echo "============================================="
echo ""
ls -a /var/se3/Classes | grep [.]"Classe_" | egrep -v "(^_Trash)" | while read Z
        do
                div=$(echo $Z | cut -c 9- | awk '{ print $1}')
                ls /var/se3/Classes/.Classe_$div | egrep -v "(^_)" | while read A
                do
                        cd /var/se3/Classes/.Classe_$div
                        mkdir -p $rep_trash/"${ladate}"_Trash_Classes_"$div"
                        echo "Deplacement de $A dans $rep_trash/"${ladate}"_Trash_Classes_"$div""
                        mv $A $rep_trash/"${ladate}"_Trash_Classes_"$div"
                done
        done

# Nettoyage des classes courantes
# Déplacement des dossiers de classe des élèves qui sont partis

echo ""
echo "============================================="
echo "Traitement des classes actives..."
echo "============================================="
echo ""
ls /var/se3/Classes | egrep -v "(^_Trash)" | while read Y
        do
		div2=$(echo $Y | cut -c 8- | awk '{ print $1}')
		find /var/se3/Classes/Classe_$div2 -maxdepth 1 -name ".[!.]*" | while read B
                do
                        cd /var/se3/Classes/Classe_$div2
                        mkdir -p $rep_trash/"${ladate}"_Trash_Classes_"$div2"
                        echo "Deplacement de $B dans $rep_trash/"${ladate}"_Trash_Classes_"$div2""
                        mv $B $rep_trash/"${ladate}"_Trash_Classes_"$div2"
                done
        done

cd

# Ajustement des droits
chown -R admin:admins /var/se3/Classes/_Trash*
chmod -R 700 /var/se3/Classes/_Trash*

# Petit bilan à destination de l'admin
echo ""
echo "============================================="
echo "Voici la taille des fichiers et dossiers déplacés dans $rep_trash :"
du -sh $rep_trash
echo "============================================="
echo ""
echo "Traitement terminé !"

