#!/bin/bash

ladate=$(date +%Y%m%d_%H%M%S)
mkdir /home/_root_Trash_${ladate}
ls /home|egrep -v "(^_|^netlogon$|^templates$|^aquota.user$|^aquota.group$|^profiles$|^lost\+found$)"|while read A
do
	t=$(ldapsearch -xLLL uid=$A)
	if [ -z "$t" ]; then
		echo "mv /home/$A /home/_root_Trash_${ladate}/"
		mv /home/$A /home/_root_Trash_${ladate}/
	fi
done

chown -R admin:admins /home/_root_Trash_${ladate}
chmod -R 700 /home/_root_Trash_${ladate}

echo ""
echo "Terminé!"
echo ""
