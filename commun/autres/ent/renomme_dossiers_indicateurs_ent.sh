#!/bin/bash
# **********************************************************
# Ce script doit etre lancé dans le répertoire qui contient les noms de dossiers à corriger.
# Il corrige les noms de dossiers.
# 20181206
# **********************************************************
DATE1=$(date +%F+%0kh%0M)

ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

rep_courant=$(pwd)
echo ""
echo "Répertoire actuel : $rep_courant "
echo ""
echo "Peut-on poursuivre ?"
echo "(Appuyez sur "Entrée")"
read lapin

# suppression des espaces (remplacés par _)
echo "suppression des espaces dans les noms de dossiers"
echo ""
rename 's/[[:blank:]]/_/g' *

# suppression des é (remplacés par e)
echo "suppression des é dans les noms de dossiers"
echo ""
rename 's/é/e/' *

# suppression des ' (remplacés par _)
#echo "suppression des espaces dans les noms de dossiers"
#echo ""
#rename 's/\'/_/' *

# suppression des è (remplacés par e)
echo "suppression des è dans les noms de dossiers"
echo ""
rename 's/è/e/g' *

# remplacement des o accentues
echo "suppression des ô dans les noms de dossiers"
echo ""
rename 's/ô/o/' *

# remplacement des chaines casse-b.. 1
echo "Correction Puy-de-Dome"
echo ""
rename 's/Puy-de-Dome/Puy_de_Dome/' *
rename 's/De/de/' *

# remplacement des chaines casse-b.. 2
echo "Correction Haute-Loire"
echo ""
rename 's/Haute-Loire/Haute_Loire/' *

sleep 1
# remplacement posterieur des "__" (double underscore)
echo "remplacement des __ dans les noms de dossiers"
rename 's/__//' *

# remplacement posterieur des "_-_"
echo "remplacement des _-_ dans les noms de dossiers"
rename 's/_-_/-/' *

# remplacement posterieur des "-_"
echo "remplacement des -_ dans les noms de dossiers"
rename 's/-_/-/' *

# remplacement posterieur des "_-"
echo "remplacement des _- dans les noms de dossiers"
rename 's/_-/-/' *

sleep 1
echo ""
echo "Le traitement des noms des dossiers est terminé".
echo ""
