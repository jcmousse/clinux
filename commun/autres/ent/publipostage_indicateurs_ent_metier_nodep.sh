#!/bin/bash
# **********************************************************
# Ce script doit etre lancé dans le répertoire qui contient les fichiers à publiposter.
# Il corrige les noms de fichiers, puis fabrique un fichier destiné au publipostage par courriel.
# Il supprime la mention lyc_metier dans les types d'etabs.
# 20181208
# **********************************************************
DATE1=$(date +%F+%0kh%0M)

ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

rep_courant=$(pwd)
echo ""
echo "Répertoire actuel : $rep_courant "
echo ""
echo "Peut-on poursuivre ?"
echo "(Appuyez sur "Entrée")"
read lapin

# Suppression des anciens fichiers de traitement
rm -f *.csv *.txt

# suppression des espaces (remplacés par _)
echo "suppression des espaces dans les noms de fichiers"
echo ""
rename 's/[[:blank:]]/_/g' *pdf

# suppression des apostrophes
echo "suppression des apostrophes dans les noms de fichiers"
echo ""
rename "s/'//g" *.pdf

# suppression des CEDEX
echo "suppression des CEDEX dans les noms de fichiers"
echo ""
rename 's/CEDEX//g' *.pdf

# suppression des LYC_METIER
echo "suppression des LYC_METIER dans les noms de fichiers"
echo ""
rename 's/_LYC_METIER//g' *.pdf

# remplacement des o accentues
echo "suppression des ô dans les noms de fichiers"
echo ""
rename 's/ô/o/g' *.pdf

# remplacement des chaines casse-b.. 1
echo "correction Puy-de-Dome"
echo ""
rename 's/Puy-de-Dome/Puy_de_Dome/g' *.pdf

# remplacement des chaines casse-b.. 2
echo "correction Haute-Loire"
echo ""
rename 's/Haute-Loire/Haute_Loire/g' *.pdf

# remplacement posterieur des "__" (double underscore)
echo "remplacement des __ dans les noms de fichiers"
rename 's/__//g' *.pdf

# remplacement posterieur des "_-_"
echo "remplacement des _-_ dans les noms de fichiers"
rename 's/_-_/-/g' *.pdf

# remplacement posterieur des "-_"
echo "remplacement des -_ dans les noms de fichiers"
rename 's/-_/-/g' *.pdf

# remplacement posterieur des "_-"
echo "remplacement des _- dans les noms de fichiers"
rename 's/_-/-/g' *.pdf

# remplacement des chaines casse-b.. 3
echo "correction FERRAND1"
echo ""
rename 's/FERRAND1/FERRAND/g' *.pdf

# remplacement des chaines casse-b.. 4
echo "correction FERRAND2"
echo ""
rename 's/FERRAND2/FERRAND/g' *.pdf

# remplacement des chaines casse-b.. 5
echo "correction SANCY-ARTENSE"
echo ""
rename 's/SANCY-ARTENSE/SANCY_ARTENSE/g' *.pdf

# remplacement des chaines casse-b.. 6
echo "correction JOLIOT-CURIE"
echo ""
rename 's/JOLIOT-CURIE/JOLIOT_CURIE/g' *.pdf

# remplacement des chaines casse-b.. 7
echo "correction MONNET-MERMOZ"
echo ""
rename 's/MONNET-MERMOZ/MONNET_MERMOZ/g' *.pdf

# remplacement des chaines casse-b.. 8
echo "correction COMTE-HENRI"
echo ""
rename 's/COMTE-HENRI/COMTE_HENRI/g' *.pdf

# remplacement des chaines casse-b.. 9
echo "correction GARCIN-MAYADE"
echo ""
rename 's/GARCIN-MAYADE/GARCIN_MAYADE/g' *.pdf

# remplacement des chaines casse-b.. 10
echo "correction SAINTE-CLAIRE"
echo ""
rename 's/SAINTE-CLAIRE/SAINTE_CLAIRE/g' *.pdf

# remplacement des chaines casse-b.. 11
echo "correction I._ET_F._"
echo ""
rename 's/I._ET_F._//g' *.pdf

# remplacement des chaines casse-b.. 12
echo "correction C._ET_P._"
echo ""
rename 's/C._ET_P._//g' *.pdf

###

# suppression des noms de departement dans les noms de fichiers
echo "suppression des départements dans les noms de fichiers"
echo ""
rename 's/Allier-//g' *.pdf
rename 's/Cantal-//g' *.pdf
rename 's/Haute_Loire-//g' *.pdf
rename 's/Puy_de_Dome-//g' *.pdf

sleep 1
echo "Le traitement des noms de fichiers est terminé".
echo ""

echo "On va fabriquer le fichier de publipostage"
echo ""
echo "Entrez le nom du fichier texte, sans extension :"
echo "Exemple : colleges_allier puis "entrée" "
echo ""
read nomtxt
# Récupération de la liste des fichiers"
ls -l *.pdf | cut -f9 | awk '{print $9}' > ./$nomtxt.txt
echo ""
echo "La liste des fichiers de ce répertoire est dans $nomtxt.txt"
sleep 1
echo ""
echo "Post-traitement pour obtenir le csv"
echo ""
#echo "Peut-on poursuivre?"
#echo "(appuyez sur entrée)"
#echo ""
#read lapin
echo "Traitement en cours..."
echo ""
# Elaboration du fichier publipostage_ent.csv

fichpubli=publi_1.csv
cat $nomtxt.txt > $fichpubli
#echo "pause 1"
#read lala
cat $nomtxt.txt > nommagetemp.csv
# ent_2017_18-dep-ville-type-nom-uai.pdf
sed -i 's/.pdf//g' $fichpubli
sed -i 's/-/;/g' $fichpubli
#echo "pause 2"
#read lulu


# On vide le fichier temporaire par précaution
> publi_2.csv

# Traitement
while IFS=';' read champ1 champ2 champ3 champ4 champ5
#		prefix    ville  type   nom    uai.pdf	
do line="$champ1;$champ2;$champ3;$champ4;$champ5"
   echo "$line;ce.$champ5@ac-clermont.fr;$(pwd)/" >> publi_2.csv	
done < $fichpubli

# Ajout des noms de fichiers
paste -d '' 'publi_2.csv' 'nommagetemp.csv' > publipostage_ent_$nomtxt.csv

# Modification de la première ligne
#sed -i '/UAI/d' publipostage_ent.csv
sed -i '1iprefix;ville;type;nom;uai;mail;fichier' publipostage_ent_$nomtxt.csv
echo "Terminé."
echo ""

# Suppression des fichiers de traitement
rm -f publi_* nommagetemp*

echo "*******************************************************************************************"
echo "Le fichier de publipostage pour cette série est publipostage_ent_$nomtxt.csv"
echo "*******************************************************************************************"
echo ""
exit 0
