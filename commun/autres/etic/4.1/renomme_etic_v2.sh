#!/bin/bash


ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# Choix du dossier de fichiers a renommer
#echo "Dans quel dossier sont les fichiers pdf a renommer ?"
#read rep
# Choix du fichier a utiliser
#echo -e "$COLDEFAUT"
#echo "Quel est le fichier a utiliser pour le renommage des fichiers pdf ? (fichier .csv) :"
#echo -e "$COLTXT"
#read fichier
fichier=nommagepdf.csv
rep=.
IFS=$'\n' tabl=( $(<$fichier) )
unset IFS
#tabl=(); while read line; do tabl+=( "$line" ); done < $fichier
while read line;do tabl[i++]="$line".pdf ;done < $fichier
for fichier in $rep/*.pdf
do mv "$fichier" "$rep/${tabl[n++]}"
done
echo -e "$VERT"
echo ""
echo "Le renommage est terminé!"
echo ""
echo -e "$COLTXT"
exit 0
