#!/bin/zsh

# **********************************************************
# Script de traitement des fiches Etic
# Nécessite renomme_etic.sh et publipostage_v2.sh dans le même répertoire!
# jcm 20171123
# version du 20190320
# **********************************************************


ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

# Tests préalables de présence des outils indispensables
if [ -x /usr/bin/zip ] ;then
echo -e "$COLINFO"
echo "zip : installé... OK"
echo -e "$COLTXT"
else
echo -e "$COLDEFAUT"
echo "zip : NON installé... NOK"
echo -e "$COLTXT"
nozip="1"
fi

if [ -x /usr/bin/xlsx2csv ] ;then
echo -e "$COLINFO"
echo "xlsx2csv : installé... OK"
echo -e "$COLTXT"
else
echo -e "$COLDEFAUT"
echo "xlsx2csv : NON installé... NOK"
echo -e "$COLTXT"
no2csv="1"
fi

if [ -x /usr/bin/pdfseparate ] ;then
echo -e "$COLINFO"
echo "poppler-utils : installé... OK"
echo -e "$COLTXT"
else
echo -e "$COLDEFAUT"
echo "poppler-utils : NON installé... NOK"
echo -e "$COLTXT"
nopdf="1"
fi

if [ -x /usr/bin/rename ] ;then
echo -e "$COLINFO"
echo "rename : installé... OK"
echo -e "$COLTXT"
else
echo -e "$COLDEFAUT"
echo "rename : NON installé... NOK"
echo -e "$COLTXT"
noren="1"
fi

if [ "$nozip" -eq "1" ]||[ "$no2csv" -eq "1" ]||[ "$nopdf" -eq "1" ]||[ "$noren" -eq "1" ];then
echo -e "$ROUGE"
echo ""
echo "-----------------------------------------------"
echo "Il manque un (des) paquet(s) indispensable(s) !"
echo "-----------------------------------------------"
echo "Installez les paquets manquants, puis relancez le script."
echo""
exit 1
echo -e "$COLTXT"
else
echo -e "$VERT"
echo ""
echo "Tout va bien, on continue..."
echo -e "$COLTXT"
fi

# Petit nettoyage avant de commencer, si le script a déja été lancé
rm -f nommagepubli.csv nommagetemp.csv publipostage_etic.csv
rm -f *.zip

#==============================================================
# Avertissements
echo ""
echo -e "$JAUNE"
echo "**** Attention! ****"
echo ""
echo "Dans le répertoire depuis lequel ce script est lancé, il faut déposer :"
echo "	- Le fichier pdf original à découper,"
echo "	- Le fichier xls ou xlsx qui contient les noms des fichiers à obtenir,"
echo "	- Le script bash renomme_etic_v2.sh"
echo "	- Le script bash publipostage_v2.sh."
echo ""
echo -e "$COLDEFAUT"
echo "Voici la liste des fichiers contenus dans le répertoire actuel :"
echo -e "$COLINFO"
echo "(Il devrait y avoir au moins 5 fichiers)"
echo ""
echo -e "$COLTXT"
ls -l
echo ""
echo -e "$JAUNE"
echo "S'il manque un des éléments requis, sortez par ctrl-c."
echo -e "$COLINFO"
echo "Sinon, vous pouvez poursuivre en appuyant sur entrée!"
echo -e "$COLTXT"
read lapin


#==============================================================
# Partie séparation du document pdf et création des fiches de 2 pages

autoload zargs
echo "==========================================="
echo "PHASE 1 : Découpage du fichier pdf original"
echo "==========================================="
echo ""
echo "	(Cette opération peut être un peu longue !)"
echo ""
dir=$(pwd)

# Suppression des espaces dans les noms de fichiers pdf
rename 's/[[:blank:]]/_/g' $dir/*.pdf
echo ""
echo -e "$COLINFO" "Note : Les espaces ont été supprimés dans les noms des fichiers pdf de ce répertoire."

# Comptage (et affichage) du nombre de fichiers pdf
NB1=$(ls $dir | grep pdf | wc -l)
echo ""
echo -e "$COLTXT" "Nombre de fichiers pdf du répertoire : $NB1"
echo ""

# Création du menu qui liste les fichiers pdf du dossier courant
    PS3="
Choisissez le fichier à traiter (ou q pour quitter) : "
    select fichierpdf in $(ls $dir | grep ".pdf" )
	do
	case "$REPLY" in
            q|Q)
                # On quitte le programme
		echo ""
		echo "		Sortie !"
		echo ""
                exit 0
                ;;
            [1-$NB1])
		echo ""
		echo -e "$COLTITRE"
                echo "Fichier $fichierpdf sélectionné"
		echo -e "$COLTXT"
		echo ""
		break
                ;;
	    *)
		echo ""
		echo "		------------------------------"
                echo "		 Choix invalide! Recommencez."
		echo "		------------------------------"
                ;;
   	 esac
	done

#============
# Traitement 
#============

echo -e "$COLINFO"
echo "Traitement des fichiers en cours...Patientez"
echo -e "$COLTXT"

# On renomme le fichier choisi
mv $fichierpdf $fichierpdf.ori

# Renommage des autres pdf s'il y en a 
test1=$(ls -l | grep .pdf$)
if [ -n "$test1" ]; then
rename 's/.pdf/.pdf.RENOMME/g' $dir/*.pdf
fi

# Lancement du traitement
fich=$fichierpdf.ori
reunite() pdfunite "$@" file-$1-$argv[-1].pdf

pdfseparate $fich p%04d
zargs -n 2 p<->(n) -- reunite
rm -f p<->
echo -e "$COLINFO"
echo ""
echo "Traitement terminé !"
echo -e "$COLTXT"
echo ""

# Messages d'info
echo -e "$COLINFO"
echo "Note : Le fichier $fichierpdf a été renommé en $fichierpdf.ori"
echo "Et les autres fichiers .pdf ont maintenant l'extension .RENOMME"
echo " pour éviter les erreurs dans la phase de renommage."
echo -e "$COLTXT"

# Message de fin de traitement
echo -e "$VERT"
echo ""
echo "Découpage en fiches terminé !"
echo -e "$COLTXT"

#=================================================================
# Phase 2 : creation d'un fichier csv pour le renommage
echo "======================================"
echo "PHASE 2 : Elaboration des fichiers csv"
echo "======================================"
echo ""

dir=$(pwd)

# Choix du fichier a utiliser
# Suppression des espaces dans les noms de fichiers xls(x)

test2=$(ls -l | grep .xls$)
if [ -n "$test2" ]; then
rename 's/[[:blank:]]/_/g' $dir/*.xls
fi

test3=$(ls | grep .xlsx$)
if [ -n "$test3" ]; then
rename 's/[[:blank:]]/_/g' $dir/*.xlsx
fi

echo ""
echo -e "$COLINFO" "Note : Les espaces ont été supprimés dans les noms des fichiers xls(x) de ce répertoire."

# Comptage (et affichage) du nombre de fichiers xls
NB2=$(ls $dir | grep xls | wc -l)
echo ""
echo -e "$COLTXT" "Nombre de fichiers xls(x) du répertoire : $NB2"
echo ""

# Création du menu qui liste les fichiers pdf du dossier courant
    PS3="
Choisissez le fichier à traiter (ou q pour quitter) : "
    select fichxls in $(ls $dir | grep xls )
	do
	case "$REPLY" in
            q|Q)
                # On quitte le programme
		echo ""
		echo "		Sortie !"
		echo ""
                exit 0
                ;;
            [1-$NB2])
		echo ""
		echo -e "$COLTITRE"
                echo "Fichier $fichxls sélectionné"
		echo -e "$COLTXT"
		echo ""
		break
                ;;
	    *)
		echo ""
		echo "		------------------------------"
                echo "		 Choix invalide! Recommencez."
		echo "		------------------------------"
                ;;
   	 esac
	done

# Conversion du fichier xls en fichier csv avec _ comme séparateur de champs
xlsx2csv -d '_' $fichxls nommagepdf.csv

# Copie
cp nommagepdf.csv nommagetemp.csv

# Conversion du fichier xls en fichier csv avec ; comme séparateur de champs
xlsx2csv -d ';' $fichxls nommagepubli.csv

# Traitement des fichiers de nommage pour les caractères à la con
iconv -f utf8 -t ascii//TRANSLIT nommagepdf.csv -o nommagepdf.csv
iconv -f utf8 -t ascii//TRANSLIT nommagetemp.csv -o nommagetemp.csv

# Remplacement des / par -
sed -i 's/\//-/g' nommage*.csv

# Ajout de ETIC201x au debut de chaque ligne
sed -i 's/^/ETIC2018_/g'  nommagepdf.csv
sed -i 's/^/ETIC2018_/g'  nommagetemp.csv

# Remplacement des espaces par _
sed -i 's/\ /_/g' nommagepdf.csv
sed -i 's/\ /_/g' nommagetemp.csv

# Remplacement des apostrophes par _
sed -i s/\'/_/g nommagepdf.csv
sed -i s/\'/_/g nommagetemp.csv

# Remplacement des _-_ par -
sed -i s/_-_/-/g nommagepdf.csv
sed -i s/_-_/-/g nommagetemp.csv

# Ajout de .pdf à la fin de chaque ligne
sed -i 's/$/\.pdf/g'  nommagetemp.csv

# Suppression de la premiere ligne du fichier de nommage des pdf
sed 1d nommagepdf.csv -i

echo ""
echo "=================================="
echo "PHASE 3 : Renommage des fiches pdf"
echo "=================================="
echo ""
echo -e "$COLINFO"
echo "On va maintenant renommer les fichiers d'après les noms contenus dans un fichier csv de référence."
echo -e "$COLTXT"
echo ""
# On lance le script qui va renommer chaque fiche pdf
./renomme_etic_v2.sh

echo ""
echo "=================================="
echo "PHASE 4 : Archivage des fiches pdf"
echo "=================================="
# On va créer une archive zip qui contiendra toutes les fiches pdf traitées
echo ""
echo "Creation (optionnelle) d'une archive zip qui contiendra tous les nouveaux fichiers pdf."
echo ""
echo -e "$JAUNE"
echo "Voulez-vous créer une archive zip qui contiendra toutes les fiches pdf ?"
echo ""
echo -e "$COLTXT"
#	read -p "	o (oui)
#	n (non) : " rep
#	case $rep in
		PS3='Répondre par o ou n: '
		LISTE=("[o] oui" "[n]  non")
		select CHOIX in "${LISTE[@]}" ; do
			case $REPLY in
				1|o)
				echo -e "$VERT"
				echo "Création d'une archive zip."
				echo -e "$COLTXT"
				ARCH=o
				break
				;;
				2|n)
				echo -e "$COLDEFAUT"
				echo "Pas de création d'archive."
				echo -e "$COLTXT"
				echo ""
				ARCH=n
				break
				;;
			esac
done

if [ $ARCH = "o" ] ; then
echo ""
echo -e "$COLDEFAUT"
echo "Entrez le nom que vous voulez donner à l'archive (sans extension) :"
echo -e "$COLTXT"
read archivepdf
zip $archivepdf.zip *.pdf
echo -e "$VERT"
echo "Création de l'archive terminée !"
echo -e "$COLTXT"
else
echo -e "$VERT"
echo "On passe à l'étape suivante..."
echo -e "$COLTXT"	
fi
echo ""
echo "================================================"
echo "PHASE 5 : Préparation du fichier de publipostage"
echo "================================================"
echo ""

# Lancement du script de fabrication du fichier csv de publipostage
echo "Creation du fichier de publipostage..."
echo ""
./publipostage_v2.1.sh
exit 0


