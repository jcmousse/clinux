#!/bin/bash
# **********************************************************
# Activation de la configuration resau pour le vlan210
# sur la machine du formateur
# 20190118
# **********************************************************
ROUGE="\\033[1;31m"
DATECONF=$(date +%F+%0kh%0M)

echo "Configuration du reseau pour le vlan 210,"
echo "avec configuration statique dhcp ou statique,"
echo "ou retour à la configuration initiale (avec network-manager)"

# On teste si le paquet vlan est installé
[ ! -e /sbin/vconfig ] && apt-get update && apt-get install vlan

# Arret de network-manager et des services réseau
service network-manager stop
#service networking stop

echo -e "$ROUGE" "Que voulez-vous faire ?" ; tput sgr0
read -p "Choisissez dans cette liste :
1 (ou 'static')
2 (ou 'dhcp')
3 (ou 'retour')	: " rep
	case $rep in

	1|static )
	# Sauvegarde de la config actuelle
	cp /etc/network/interfaces /etc/network/interfaces_nm_$DATECONF
	echo -e "$ROUGE" "Configuration : Statique" ; tput sgr0
	echo "Quelle adresse ip voulez-vous fixer à la machine pour le vlan210 ?"
	echo "Indiquez seulement le dernier octet :"
	read IP210
	# Ajout du vlan 210 sur eth0
	modprobe 8021q
	vconfig add eth0 210
echo "# The loopback network interface
auto lo
iface lo inet loopback

# eth0
auto eth0
iface eth0 inet dhcp

# Vlan 210 sur eth0
auto eth0.210
iface eth0.210 inet static
  address 192.168.224.$IP210
  network 192.168.224.0
  netmask 255.255.255.0
  broadcast 192.168.224.255
  gateway 192.168.224.254" > /etc/network/interfaces
	# Redémarrage du réseau
	service networking start ;;

	2|dhcp )
	# Sauvegarde de la config actuelle
	cp /etc/network/interfaces /etc/network/interfaces_nm_$DATECONF
	echo -e "$ROUGE" "Configuration : dhcp" ; tput sgr0
	# Ajout du vlan 210 sur eth0
	modprobe 8021q
	vconfig add eth0 210
echo "# The loopback network interface
auto lo
iface lo inet loopback

# eth0
auto eth0
iface eth0 inet dhcp

# Vlan 210 sur eth0
auto eth0.210
iface eth0.210 inet dhcp" > /etc/network/interfaces
# Redémarrage du réseau
service networking start ;;

	3|retour )
	# Sauvergarde de la config actuelle
	cp /etc/network/interfaces /etc/network/interfaces_210_$DATECONF
	echo -e "$ROUGE" "Retour à la configuration network-manager" ; tput sgr0
	# suppression de la config vlan210 sur la carte eth0
	vconfig rem eth0.210
echo "# The loopback network interface
auto lo
iface lo inet loopback" > /etc/network/interfaces
	# Redémarrage du réseau
	service networking start
	service network-manager start ;;

	* ) echo -e "$ROUGE" "Erreur - Relancez le script !" ; tput sgr0 ;;
	esac
