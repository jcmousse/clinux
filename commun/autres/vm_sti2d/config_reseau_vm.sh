#!/bin/bash
# **********************************************************
# Modification de la configuration reseau pour les vms de formation
# Script à lancer sur les vm
# **********************************************************
ROUGE="\\033[1;31m"
DATECONF=$(date +%F+%0kh%0M)


echo "Configuration statique ou dhcp"

# Arret de network-manager et des services réseau
service network-manager stop
service networking stop

netint=$(ip a | grep enp | cut -d: -f2 | awk '{ print $1}')

echo -e "$ROUGE" "Que voulez-vous faire ?" ; tput sgr0
read -p "Choisissez dans cette liste :
1 (ou 'static')
2 (ou 'dhcp') : " rep
	case $rep in

	1|static )
	# Sauvergarde de la config actuelle	
	cp /etc/network/interfaces /etc/network/interfaces_nm_$DATECONF
	echo -e "$ROUGE" "Configuration : Statique" ; tput sgr0
	echo "Quelle adresse ip voulez-vous fixer à la machine pour le vlan210 ?"
	echo "Indiquez seulement le dernier octet :"
	read IP210
	
echo "# The loopback network interface
auto lo
iface lo inet loopback

# Static $netint
auto $netint
iface $netint inet static
  address 192.168.224.$IP210
  network 192.168.224.0
  netmask 255.255.255.0
  broadcast 192.168.224.255
  gateway 192.168.224.254" > /etc/network/interfaces
	
	2|dhcp )
	# Sauvergarde de la config actuelle				
	cp /etc/network/interfaces /etc/network/interfaces_$DATECONF
	echo -e "$ROUGE" "Configuration : dhcp" ; tput sgr0
	
echo "# The loopback network interface
auto lo
iface lo inet loopback

# $netint
auto $netint
iface $netint inet dhcp" > /etc/network/interfaces

	* ) echo -e "$ROUGE" "Erreur - Relancez le script !" ; tput sgr0 ;;
	esac
