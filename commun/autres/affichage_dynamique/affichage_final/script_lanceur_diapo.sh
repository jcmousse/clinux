#!/bin/sh

UTI="affiche"
IP_SE3="172.17."
#date1=$(date +%F+%0kh%0M)

# Pour le débugguage : fichiers de log journalier (les rasaffiche redémarrant chaque jour)
exec 1> "/home/affiche/sorties.log" 2> "/home/affiche/erreurs.log"

# Pour éviter que l'écran ne s'éteigne au bout d'un certain temps 
xset s off
xset -dpms

#Desactivation de xscreensaver
xscreensaver-command -deactivate

# On affiche l'image d'arrière fond pour "masquer" le bureau LXDE de Raspbian.
feh -Z -F -Y "/home/affiche/Splash_screen_miniature.png" &

# Initialisation des variables
VERSION_OLD="toto"
afficheD=""

# On attend 15 secondes que wicd configure le réseau
sleep 15

# On teste toutes 20 secondes si une nouvelle version du diapo est disponible sur le partage et on met à jour si nécessaire ...
while true; do

	# On rècupère la date de dernière modification du diaporama, sur le partage Samba
	VERSION_NEW=$(smbclient //$IP_SE3/Docs -U admin.affichage%adaf -c 'ls \affichage\commun\diapo_eleves.odp' | cut -f 1 | awk '{print $4 $5 $6 $7 $8}')

	# Si la diapo sur le partage est plus récent que celui projeté actuellement, on re-télécharge la nouvelle version du diapo puis on la projette
	if [ "$VERSION_NEW" != "$VERSION_OLD" ]
	then
		# On ferme le diapo projeté
		kill "$afficheD"

		# On attend 2 secondes avant de passer à la suite
		sleep 2

		# On nettoie le répertoire avant d'y mettre la nouvelle version du diaporama
		mkdir -p /home/affiche/archives_diaporamas
		#rm -rf "/home/affiche/diapo_eleves*"
		#mkdir "/home/affiche/"
		date1=$(date +%F+%0kh%0M)
		mv /home/affiche/diapo_eleves.odp /home/affiche/archives_diaporamas/diapo_eleves_$date1\_.odp

                smbclient //$IP_SE3/Docs -U admin.affichage%adaf -c 'get \affichage\commun\diapo_eleves.odp /home/affiche/diapo_eleves_temp.odp'

		sleep 2

                # On relance le diapo uniquement si le téléchargement s'est bien passé, sinon on affiche un message d'erreur
		if [ -e "/home/affiche/diapo_eleves_temp.odp" ]
		then
			mv -f "/home/affiche/diapo_eleves_temp.odp" "/home/affiche/diapo_eleves.odp"
			VERSION_OLD="$VERSION_NEW"
		else
			cp -f "/home/affiche/diapo_erreur.odp" "/home/affiche/diapo_eleves.odp"
		fi

		# On attend 1 seconde avant de passer à la suite (ce qui fait que les utilisateurs visionnent l'image d'arrière fond pendant 5 secondes au total avant la projection du nouveau diaporama ...)
		sleep 1

                # On relance libreoffice avec la nouvelle version du diapo
		libreoffice --norestore --show "/home/affiche/diapo_eleves.odp" &
        	afficheD=$(echo "$!");  

        	# On attend 15 secondes avant la prochaine synchro
		sleep 15

	else
		# Pas de mise à jour du diapo : on attend 20 secondes avant de re-vérifier la synchro
		xscreensaver-command -deactivate
		sleep 20
	fi
done
