AFFICHAGE DYNAMIQUE SUR ECRANS

PRINCIPE
- Un pc Debian par écran.
- Diaporama(s) à afficher déposés sur le se3 (partage /var/se3/Docs/affichage)
- Script local sur chaque pc avec copie du diaporama en local à chaque mise à jour du diaporama sur le se3.


PC ECRAN
- Debian Stretch + LXDE
- Un utilisateur local "affiche"
- Script de mise à jour et affichage du diaporama : /home/affiche/script_lanceur_diapo.sh
- Répertoire local de stockage du diaporama : /home/affiche/

Installation du pc écran :
- Installer Debian Stretch avec environnement LXDE

Installation des paquets supplémentaires :
# apt-get install libreoffice-l10n-fr cifs-utils smbclient feh net-tools

Modification /etc/sudoers :
# visudo
	ajouter :
## user "affiche" is allowed to execute reboot
affiche ALL=NOPASSWD:/sbin/reboot

# Autologin de l'utilisateur affiche
/etc/lightdm/lightdm.conf ligne 122
autologin-user=affiche

# Lancement automatique du script
mkdir -p /home/affiche/.config/autostart
nano /home/affiche/.config/autostart/lanceur.desktop

[Desktop Entry]
Type=Application
Name=Affichage
Exec=lxterminal -e /home/affiche/script_lanceur_diapo.sh

Dépot du script de changement de diaporama dans /usr/bin :
# cd /usr/bin
# wget --no-check-certificate https://raw.githubusercontent.com/jcmousse/clinux/master/commun/autres/affichage_dynamique/change

Dépot du script d'affichage dans /home/affiche :
# cd /home/affiche
# wget --no-check-certificate https://raw.githubusercontent.com/jcmousse/clinux/master/commun/autres/affichage_dynamique/affichage_final/script_lanceur_diapo_v3.sh

Configuration du script d'affichage en fonction de l'écran :
Ecrans disponibles : cdt, cdi, adm, self, commun
# sed -i 's/<ancien>/<nouveau>/g' /home/affiche/script_lanceur_diapo_v3.sh
exemple :
# sed -i 's/self/commun/g' /home/affiche/script_lanceur_diapo_v3.sh

SUR LE SE3
- Création d'un utilisateur "admin.affichage" sur le se3
- Groupe "administratifs"
- Création d'un groupe "autre" affichage_dyn
- Ajout au groupe "affichage_dyn" de admin.affichage et des autres utilisateurs autorisés du ldap
- Réglage des acl sur le répertoire /Docs/affichage
	Tous droits à admin et au groupe affichage_dyn
	Aucun droit à "autres"

Crontab sur les postes :
- on efface les diaporamas plus vieux que 21 jours
0 8 * * * find /home/affiche/archives_diaporamas/ -mtime +21 -delete
- on efface les fichiers vides
1 8 * * * find /home/affiche/archives_diaporamas/ -type f -empty -delete

Crontab sur le se3
/etc/cron.d/perso
# Reboot périodique du poste affichage (à 9,11,13... h20 du lundi au vendredi)
# Pour éviter que l'affichage reste planté
20 9,11,13,15,17 * * 1-5 root /usr/bin/ssh -o StrictHostKeyChecking=no root@172.17.xxx.xxx reboot &> /dev/null

