#!/bin/sh

# Script de changement de l'affichage sur l'écran :"
# Choix entre l'affichage spécifique et l'affichage commun."
# Configurer le nom de l'écran et l'ip du pc de pilotage de l'écran

ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

ECRAN="cdt"
IP_ECRAN="172.17.139.247"

echo "$COLINFO"
echo ""
echo "Ce script permet de changer le diaporama de l'écran $ECRAN."
echo ""
echo "Passage de l'affichage spécifique $ECRAN à l'affichage commun à tous les écrans,"
echo "ou retour à l'affichage spécifique de l'écran $ECRAN."
echo ""
echo "$COLTXT"
echo "$JAUNE"
echo "Le mot de passe de la machine qui pilote l'écran $ECRAN va vous etre demandé."
echo "Après avoir entré le mot de passe, tapez "change""
echo ""
echo "$COLTXT"
echo "$COLINFO" "Appuyez sur entrée pour continuer."
echo ""
echo "$COLTXT"

read "nada"

ssh affiche@$IP_ECRAN

