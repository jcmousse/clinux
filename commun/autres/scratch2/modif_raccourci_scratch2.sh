#!/bin/bash
####################################
# modif_raccourci_scratch2.sh
# 20161207
####################################

mv /usr/share/applications/edu.media.mit.scratch2editor.desktop /usr/share/applications/scratch2.desktop
sed -i 's/^Categories.*/Categories=Application;Education;Development;/g' /usr/share/applications/scratch2.desktop
update-menus
echo "Raccourci modifié"
