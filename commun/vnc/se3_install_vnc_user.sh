#!/bin/bash
#
# se3_install_vnc_user.sh
# 
# Ce script permet de configurer, pour un ou plusieurs users du domaine,
# La possibilité d'ouvrir une session graphique a distance en vnc
# Ne pas oublier de configurer le client :
#
# Configuration du fichier xinitrc
# chmod 755 /etc/X11/xinit/xinitrc
#
# Version du 20160505
DATE=$(date +%F+%0kh%0M)

# Serveur FTP de depot des fichiers de configuration de vnc 
#FTP_SERVER="http://jcmousse.free.fr/se3"
FTP_SERVER="https://raw.githubusercontent.com/jcmousse/clinux/master"
#########################################
# On fait le menage pour virer une ancienne version
rm -f /root/se3_install_vnc_user.sh

# Choix de l'utilisateur VNC
echo "Pour quel utilisateur voulez vous installer vncserver?"
read USERVNC
echo "Vous avez choisi l'utilisateur $USERVNC"

# Creation du repertoire vnc
cd /home/$USERVNC
rm -rf .vnc
mkdir .vnc

# On recupere le fichier xstartup
cd /home/$USERVNC/.vnc
wget --no-check-certificate --quiet $FTP_SERVER/commun/vnc/xstartup
chown $USERVNC:admins xstartup
chmod 755 xstartup
chown -R $USERVNC:admins /home/$USERVNC/.vnc

# Temoin et datage de creation de l'utilisateur vnc
touch /root/creation_uservnc-$USERVNC-$DATE

echo "Une session a distance vnc pourra etre ouverte pour l'utilisateur $USERVNC"
echo "Penser à faire un #chmod 755 /etc/X11/xinit/xinitrc sur la machine distante"
echo "Ouvrir une session ssh avec $USERVNC@<ip_du_client> puis lancer xvnc4server"
