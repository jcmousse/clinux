#!/bin/bash
#
# /usr/share/se3/scripts/cron_df_quotidien.sh
# enregistre 1 fois par 24h l'espace occupé par
# /home et /var/se3 sur le se3
# 20151219

dt2=$(date '+%A_%d/%m/%Y %H:%M')
ven=$(echo $dt2 | grep vendredi)
if [ -z "$ven" ]; then
# La réponse est vide : ce n'est pas vendredi, on ajoute le df du jour au log
echo $dt2 >> /var/log/df_home_varse3_daily.log
df -h | grep /home >> /var/log/df_home_varse3_daily.log
df -h | grep /var/se3 >> /var/log/df_home_varse3_daily.log
echo "" >> /var/log/df_home_varse3_daily.log
else
# La réponse n'est pas vide : c'est vendredi, on recommence avec un nouveau log
echo $dt2 > /var/log/df_home_varse3_daily.log
df -h | grep Taille >> /var/log/df_home_varse3_daily.log
df -h | grep /home >> /var/log/df_home_varse3_daily.log
df -h | grep /var/se3 >> /var/log/df_home_varse3_daily.log
echo "" >> /var/log/df_home_varse3_daily.log
fi
