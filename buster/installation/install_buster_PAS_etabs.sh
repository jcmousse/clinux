#!/bin/bash
# **********************************************************
# Installation automatisée d'un client buster
# après installation de base du système.
# Installation pour des clients hors domaine.
# auteur jc mousseron 02/2019
# Ce script est distribué selon les termes de la licence GPL
# version du 20190413
# **********************************************************
DATEINST=$(date +%F+%0kh%0M)

FTP=https://raw.githubusercontent.com/jcmousse/clinux/master
FTP2=http://jcmousse.free.fr

ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu


# On neutralise le cache apt utilisé pendant l'installation de base du système
> /etc/apt/apt.conf

# Test de la version Debian du client
TEST_VERSION=$(cat /etc/debian_version | grep "buster")
if [ ! -n "$TEST_VERSION" ]; then
echo "Ce script est destiné à un système Debian 10 (Buster)."
echo "Il n'est pas adapté à ce système."
exit
else
echo "Debian 10 (Buster) détectée... on poursuit."
fi

AFFICHE_ERREUR()
{
if [ "$ERREUR" = "1" ];then
	echo -e "$COLERREUR"
	echo "Erreur pendant cette opération...Corrigez le problème, puis relancez le script."
	echo -e "$COLTXT"
	exit 1
fi
}


echo -e "$ROUGE" "Utilisez-vous un cache APT pour l'installation ?" ; tput sgr0
		PS3='Repondre par o ou n: '   # le prompt
		LISTE=("[o] oui" "[n]  non")  # liste de choix disponibles
		select CHOIX in "${LISTE[@]}" ; do
			case $REPLY in
				1|o)
				echo "Cache apt présent pour l'installation (cool!)"
				APTCACHE=o
				break
				;;
				2|n)
				echo "Pas de cache apt pour l'installation (dommage ;)"
				APTCACHE=n
				> /etc/apt/apt.conf
				> /etc/apt/apt.conf.d/99proxy
				break
				;;
			esac
		done

if [ $APTCACHE = "o" ] ; then
	# Définition de l'adresse du proxy apt pour l'installation
	defaultcache="192.168.1.251:3142"
	echo -e "$COLINFO" "Vous utilisez un cache APT pour l'installation. " ; tput sgr0
		echo -e "$COLINFO" "Donnez l'adresse IP et le port du cache APT sous la forme 192.168.1.251:3142"
		echo -e "$COLDEFAUT" "Cache par défaut : [$defaultcache]"
		echo "Validez par entrée si cette adresse convient, ou entrez une autre adresse (sans oublier le port) :"
		read APTPROXY
		if [ -z "$APTPROXY" ]; then
		APTPROXY=$defaultcache
		fi
		echo -e "$DEFAUT" "L'adresse IP du cache apt est $APTPROXY"
		echo -e "$COLTXT"
		
	echo ""
	echo "Ajout temporaire du proxy apt à la configuration..."
	# Déclaration du proxy pour apt
	touch /etc/apt/apt.conf.d/99proxy
	echo "#Configuration du proxy apt pour l'installation
Acquire::http { Proxy \"http://$APTPROXY\"; };" > /etc/apt/apt.conf.d/99proxy
fi


# Désactivation de l'entrée cle usb dans /etc/fstab si le systeme a ete installé avec une clé
sed -i 's/\/dev\/sdb/#\/dev\sdb/g' /etc/fstab

echo -e "$ROUGE" "C'est parti!" ; tput sgr0
echo -e "$JAUNE" "En fonction de la connexion internet, l'installation prendra de 30 à 90 minutes. " ; tput sgr0
echo -e "$JAUNE" "Soyez patient(e)..." ; tput sgr0
echo ""
echo -e "$VERT" "Installation de lxde..." ; tput sgr0

apt-get update || ERREUR="1"
# Arret du script en cas d'erreur
AFFICHE_ERREUR

apt-get install --assume-yes lxde || ERREUR="1"
# Arret du script en cas d'erreur
AFFICHE_ERREUR

echo -e "$VERT" "Modifications des sources..." ; tput sgr0
cp /etc/apt/sources.list /etc/apt/sources.save

echo "deb http://deb.debian.org/debian buster-updates main contrib non-free
#deb-src http://deb.debian.org/debian stretch-updates main contrib non-free

deb http://deb.debian.org/debian/ buster main contrib non-free
#deb-src http://deb.debian.org/debian/ buster main contrib non-free

deb http://security.debian.org/debian-security buster/updates main contrib non-free
#deb-src http://security.debian.org/debian-security buster/updates main" > /etc/apt/sources.list

echo "# Deb-multimedia
deb http://www.deb-multimedia.org buster main non-free
#deb http://www.deb-multimedia.org stretch-backports main" > /etc/apt/sources.list.d/deb-multimedia.list

echo "# Java installer
#deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main
#deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" > /etc/apt/sources.list.d/webupd8team-java.list


echo -e "$VERT" "Sources modifiées, mise à jour des paquets..." ; tput sgr0

# Autorisation du ftp pour apt
echo "Dir::Bin::Methods::ftp \"ftp\";"> /etc/apt/apt.conf.d/55ftp

apt-get update -o Acquire::AllowInsecureRepositories=true

apt-get install --assume-yes dirmngr

# Ajout de clés pour les dépots supplémentaires
echo -e "$VERT" "Installation des clés des nouveau dépots" ; tput sgr0
#apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886

# Clé deb-multimedia
apt-get install --assume-yes --allow-unauthenticated deb-multimedia-keyring

apt-get update || ERREUR="1"
# Arret du script en cas d'erreur
AFFICHE_ERREUR

export DEBIAN_FRONTEND=noninteractive

echo -e "$ROUGE" "On commence une longue série d'installations...." ; tput sgr0
echo ""

apt-get install debconf

echo -e "$VERT" "Installation de libreoffice" ; tput sgr0
apt-get install --assume-yes libreoffice libreoffice-l10n-fr
echo -e "$VERT" "Installation d'iceweasel" ; tput sgr0
apt-get install --assume-yes firefox-esr firefox-esr-l10n-fr flashplayer-mozilla
echo -e "$VERT" "Installation d'un tas de trucs divers et variés..." ; tput sgr0
echo -e "$JAUNE" "Il y en a pour un moment, vous pouvez aller boire un coup!" ; tput sgr0
apt-get install --assume-yes --allow-unauthenticated vlc gimp scribus audacity lame 
apt-get install --assume-yes --allow-unauthenticated ffmpeg sox twolame vorbis-tools faad totem
TEST_VERSION=$(uname -a | grep "i686")
if [ ! -n "$TEST_VERSION" ]; then
echo -e "$VERT" "Version 64 bits détectée" ; tput sgr0
apt-get install --assume-yes w64codecs
else
echo -e "$VERT" "Version 32 bits détectée" ; tput sgr0
apt-get install --assume-yes w32codecs
fi
apt-get install --assume-yes --allow-unauthenticated alsa-utils alsamixergui alsa-oss ntpdate ethtool nictools-pci screen apt-listchanges xinit
apt-get install --assume-yes --allow-unauthenticated ttf-mscorefonts-installer libdvdcss2 evince xdg-user-dirs anacron unrar tightvncserver net-tools
#apt-get install --assume-yes chromium chromium-l10n flashplayer-chromium
apt-get install --assume-yes --allow-unauthenticated openshot freeplane winff
apt-get install -y --allow-unauthenticated geogebra5
apt-get install --assume-yes --allow-unauthenticated firmware-linux-nonfree lshw

# Installation de python
#apt-get install -y python3 python3-matplotlib idle3 python3-setuptools python3-pip
#pip3 install pygame

echo -e "$VERT" "Ouf, les installations sont terminées !" ; tput sgr0
echo ""
chmod 755 /etc/X11/xinit/xinitrc

echo -e "$JAUNE" "On fait un peu de ménage..." ; tput sgr0
apt-get --assume-yes autoremove
apt-get clean
apt-get autoclean

# Configuration de xdg-user-dirs
sed -i 's/Desktop/Bureau/g' /etc/xdg/user-dirs.defaults

# Configuration de libfm pour le lancement des raccourcis sans confirmation
sed -i 's/_exec=0/_exec=1/g' /etc/xdg/libfm/libfm.conf

# Installation et configuration des mises à jour automatiques
#if [ $MAJAUTO = "o" ] ; then
echo ">>>> Installation des maj automatiques <<<<<"
apt-get install --assume-yes unattended-upgrades
cp /usr/share/unattended-upgrades/20auto-upgrades /etc/apt/apt.conf.d/20auto-upgrades
cd /etc/apt/apt.conf.d
rm -f 50unattended-upgrades
wget --no-check-certificate $FTP/buster/50unattended-upgrades
echo -e "$VERT" "Mises à jour automatiques installées et configurées" ; tput sgr0
#fi

# Ajout et activation de /etc/rc.local
# Ajout du service rc-local.service
echo "[Unit]
Description=/etc/rc.local
ConditionPathExists=/etc/rc.local

[Service]
Type=forking
ExecStart=/etc/rc.local start
TimeoutSec=0
StandardOutput=tty
RemainAfterExit=yes
SysVStartPriority=99

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/rc-local.service

# Insertion d'un /etc/rc.local standard (vide)
echo "#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.

exit 0" > /etc/rc.local

# On rend rc.local exécutable
chmod +x /etc/rc.local

# Le service rc-local est activé
systemctl enable rc-local

#echo -e "$VERT" "Modifications pour le démarrage a distance...." ; tput sgr0
#echo -e "$VERT" "Désactivation de NETDOWN dans /etc/init.d/halt" ; tput sgr0
#sed -i 's/NETDOWN=yes/NETDOWN=no/g' /etc/init.d/halt

# Activation du wol pour l'interface eth0 (défaut)
echo -e "$VERT" "Activation de la fonction WOL sur la carte réseau" ; tput sgr0
ethtool -s eth0 wol g

# Activation du wol dans rc.local au démarrage
sed -i '/exit 0/d' /etc/rc.local
echo "ethtool -s eth0 wol g"  >> /etc/rc.local
echo "exit 0" >> /etc/rc.local

# Configuration du clavier
sed -i 's/us/fr/g' /etc/default/keyboard

# Alias poweroff
echo "alias halt='poweroff'" >> /root/.bashrc

# Modification du nom des interfaces réseau
echo ""
echo "Renommage des interfaces it mise à jour de grub..."
sed -i 's/GRUB_CMDLINE_LINUX=\"\"/GRUB_CMDLINE_LINUX=\"net.ifnames=0 biosdevname=0\"/g' /etc/default/grub
update-grub
echo "Interfaces réseau renommées."

# Renommage de l'interface réseau
echo ""
echo -e "$JAUNE" "Renommage (éventuel) de l'interface réseau de cette machine..." ; tput sgr0
echo ""
echo "- Si cette machine ne comporte qu'une interface réseau, celle ci sera renommée automatiquement."
echo "- S'il y a plusieurs interfaces, il faudra modifier la configuration à la main."
echo ""
echo "Voici le contenu actuel du fichier /etc/network/interfaces :"
echo ""
echo "=============================================================================="
cat /etc/network/interfaces
echo "=============================================================================="
echo ""
echo "Appuyez sur une touche pour continuer..."
read lapin
echo ""

echo -e "$COLDEFAUT" "Y a-t-il plus d'une interface sur cette machine ?" ; tput sgr0
echo ""
echo -e "$COLINFO" "- Une seule interface réseau : tapez "1" " ; tput sgr0
echo -e "$COLINFO" "- Plus d'une interface réseau : tapez "2" " ; tput sgr0
		echo ""
		LISTE=("[o] oui" "[n] non")  # liste de choix disponibles
		select CHOIX in "${LISTE[@]}" ; do
			case $REPLY in
				2|o)
				echo ""
				echo -e "$COLINFO" "Il y a plus d'une interface réseau sur cette machine." ; tput sgr0
				INTER=o
				break
				;;
				1|n)
				echo ""
				echo -e "$COLINFO" "L'interface va être renommée automatiquement si besoin." ; tput sgr0
				echo ""
				INTER=n
				break
				;;
			esac
		echo ""
		done 

if [ $INTER = "n" ] ; then
# Détermination du nom de l'interface actuelle dans /etc/network/interfaces
oldnet1=$(cat /etc/network/interfaces | grep dhcp | grep enp | cut -d' ' -f2 | awk '{ print $1}')
if [ ! -n "$oldnet1" ]; then
	oldnet1=$(cat /etc/network/interfaces | grep dhcp | grep eno | cut -d' ' -f2 | awk '{ print $1}')
		if [ ! -n "$oldnet1" ]; then
		oldnet1=$(cat /etc/network/interfaces | grep dhcp | grep eth | cut -d' ' -f2 | awk '{ print $1}')
			if [ "$oldnet1" = eth0 -o "$oldnet1" = eth1 ]; then
			echo ""
			echo  "Le nom actuel de l'interface est $oldnet1"
			echo "Pas de changement de nom."
			else
			echo ""
			echo -e "$COLDEFAUT" "Erreur dans la détection du nom. Il faudra corriger à la main." ; tput sgr0
			fi
		else
		echo ""
		echo  "Le nom actuel de l'interface est $oldnet1"
		sed -i 's/'$oldnet1'/eth0/g' /etc/network/interfaces
		echo ""
		echo "L'interface $oldnet1 a été renommée en eth0"
		echo ""
		fi	
else
echo ""
echo  "Le nom actuel de l'interface est $oldnet1"
echo ""
sed -i 's/'$oldnet1'/eth0/g' /etc/network/interfaces
echo "L'interface $oldnet1 a été renommée en eth0"
fi
echo ""
else
echo ""
echo -e "$COLDEFAUT" "Il faut configurer /etc/network/interfaces à la main !" ; tput sgr0
echo ""
echo "On passe à la suite."
echo ""
fi

# Changement des mots de passe par défaut
echo -e "$ROUGE" "Il est temps de changer le mots de passe de root" ; tput sgr0
echo -e "$JAUNE" "Entrez (2 fois) le nouveau mot de passe pour root" ; tput sgr0
passwd

# Datage du script (temoin de passage dans /root)
cd /root
rm -f installation_*
echo "Installation le $DATEINST" >> /root/installation_$DATEINST

# Mise à jour finale
echo -e "$JAUNE" "Mise à jour finale de la machine..." ; tput sgr0
apt-get update
apt-get -y upgrade && apt-get -y dist-upgrade
apt-get -y autoremove && apt-get autoclean
apt-get update

echo -e "$JAUNE" "Neutralisation du proxy APT de l'installation" ; tput sgr0
sed -i 's/Acquire/#Acquire/g' /etc/apt/apt.conf.d/99proxy

# Téléchargement des scripts utiles pour la maintenance
cd /usr/bin
wget --no-check-certificate https://raw.githubusercontent.com/jcmousse/clinux/master/se3/alancer/maj_client.sh
wget --no-check-certificate https://raw.githubusercontent.com/jcmousse/clinux/master/se3/alancer/change_proxy.sh
chmod +x maj_client.sh
chmod +x change_proxy.sh
cd

# Activation des services
#systemctl enable NetworkManager-wait-online.service
#systemctl enable ssh.service
#systemctl enable smbd

# Messages de fin d'installation
echo ""
echo -e "$VERT" "La configuration automatique est terminée." ; tput sgr0
echo ""
echo "La machine redémarrera dans 10 secondes..."
sleep 10
reboot
