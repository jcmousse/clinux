#!/bin/bash
#
# 
#############################################################################
##### Script permettant de joindre un client Debian Buster au serveur SE3
# Montage des /home/user, de /var/se3/Classes et de /var/se3/Docs en cifs
# 
# En développement pour Debian Buster (Lxde) sur se3 wheezy
# 20190209
#############################################################################

DATERAPPORT=$(date +%F+%0kh%0M)

FTP=https://gitlab.com/jcmousse/clinux/raw/master

#Couleurs
ROUGE="\\033[1;31m"
VERT="\\033[1;32m"
BLEU="\\033[1;34m"
JAUNE="\\033[1;33m"

COLTITRE="\033[1;35m"   # Rose
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLTXT="\033[0;37m"     # Gris
COLINFO="\033[0;36m"	# Cyan
COLPARTIE="\033[1;34m"	# Bleu

#{
# Section a compléter avec vos paramètres !
# Il faut lancer le script rejoint_autoconfig.sh sur le se3, puis exporter ce script sur les clients.
SE3_SERVER="##se3server##" # nom dns du se3 (hostname)
SE3_IP="##se3ip##" # ip du se3
DOMAIN="##se3domain##" # Nom du domaine samba
LDAP_SUFFIX="##se3suffix##" # voir dans /etc/samba/smb.conf
LDAP_ADMIN_DN="##se3admindn##" # voir dans /etc/samba/smb.conf
LDAP_SERVER="##se3ip##" # ip de l'annuaire ldap
PASS_LDAP="##se3passldap##" # mdp de l'administrateur du ldap
SERVINV="http://fi-inv.ac-clermont.fr/glpi/plugins/fusioninventory/" # spécifique académie de Clermont-Fd
TAG="##se3rne##P" # RNE + 'P' (péda) ou 'A' (administratif)
RNE="##se3rne##" # RNE du bahut
PROXY="##ipamon##"
APTCACHE="##ipcache##"
NTPSERVERS="ntp.ac-creteil.fr" # adresse d'un serveur de temps : ex ntp.ac-clermont.fr
IOCHARSET="utf8" # normalement utf8 - voir /etc/samba/smb.conf
TLS="0" # laisser a 0
DATESCRIPT="20190208"
VERSION="1.6"
##########################################

# On rend le script "cretin-resistant"
[ -e /var/www/se3 ] && echo "Malheureux... Ce script est a exécuter sur les clients Linux, pas sur le serveur !" && exit 1

# Test de la version Debian du client
TEST_VERSION=$(cat /etc/debian_version | grep "10.")
echo ""
if [ ! -n "$TEST_VERSION" ]; then
echo "Ce script est destiné à un système Debian 10 (Buster)."
echo "Il n'est pas adapté à ce système."
exit
else
echo "Debian 10 (Buster) détectée... on poursuit."
fi
echo ""

AFFICHE_ERREUR()
{
if [ "$ERREUR" = "1" ];then
	echo -e "$COLERREUR"
	echo "Erreur pendant cette opération...Corrigez le problème, puis relancez le script."
	echo -e "$COLTXT"
	exit 1
fi
}


# Test renseignement variables fondamentales
if [ -z "${SE3_SERVER}" -o "${SE3_SERVER:0:1}" = "#" ]; then
echo " Erreur: la variable SE3_SERVER n'est pas renseignée."
exit
fi

if [ -z "${SE3_IP}" -o "${SE3_IP:0:1}" = "#" ]; then
echo " Erreur: la variable SE3_IP n'est pas renseignée."
exit
fi

if [ -z "$LDAP_SUFFIX" -o "${LDAP_SUFFIX:0:1}" = "#" ]; then
echo " Erreur: la variable BASE_DN n'est pas renseignée."
exit
fi

if [ -z "${LDAP_SERVER}" -o "${LDAP_SERVER:0:1}" = "#" ]; then
echo " Erreur: la variable LDAP_SERVER n'est pas renseignée."
exit
fi

echo "La version du script utilisé est $VERSION datée du $DATESCRIPT"
echo ""

TEST_ROCHE=$(cat /etc/profile | grep "139.253")
if [ ! -n "$TEST_ROCHE" ]; then
	echo -e "$JAUNE" "Le proxy http n'est pas (encore) renseigné dans /etc/profile." ; tput sgr0
	echo ""
	echo -e "$ROUGE" "Lieu d'intégration : Etes-vous au lycée Roche-Arnaud ? [o/N] " ; tput sgr0
	read replra
		if [ ! "$replra" = "o" -a ! "$replra" = "oui" ]; then
		echo "Installation dans un établissement standard."
		echo "On poursuit..."
		ROCHE=n	
		else
		echo "Installation au lycée Roche-Arnaud."
		ROCHE=o
		fi


		if [ $ROCHE = "o" ] ; then
		# On déclare le proxy http dans /etc/profile si on intègre à Roche-Arnaud,
		# Puis on sort du script : il faut se relogger pour prendre le proxy en compte
		sed -i '/export/d' /etc/profile
		sed -i '/Configuration/d' /etc/profile
		echo "export http_proxy=\"http://172.17.139.253:3128\"" >> /etc/profile
		echo "export https_proxy=\"http://172.17.139.253:3128\"" >> /etc/profile
		echo "Le proxy http a été renseigné dans /etc/profile."
		echo "Le script va s'interrompre..."
		echo ""
		echo -e "$ROUGE" "Il faut sortir du shell, se relogger (en root) puis relancer le script." ; tput sgr0
		sleep 2
		exit
		else
		# Sinon, on continue
		echo ""
		fi
else
# On vérifie le lieu d'installation
echo -e "$COLDEFAUT" "Le proxy du lycée Roche-Arnaud a été détecté dans la configuration du client. " ; tput sgr0
echo -e "$ROUGE" "Lieu d'intégration : Etes-vous bien au lycée Roche-Arnaud ? [O/n] " ; tput sgr0
echo ""
	read replra
		if [ ! "$replra" = "n" -a ! "$replra" = "non" ]; then
		echo -e "$COLINFO" "Intégration à Roche-Arnaud." ; tput sgr0
		echo -e "$COLINFO" "Poursuivons..." ; tput sgr0
		ROCHE=o
		else
		echo -e "$COLINFO" "Intégration dans un autre bahut." ; tput sgr0
		echo ""
		ROCHE=n
		sed -i '/export/d' /etc/profile
		sed -i '/Configuration/d' /etc/profile
		> /etc/apt/apt.conf
		> /etc/apt/apt.conf.d/99proxy
		echo -e "$COLDEFAUT" "Le proxy http a été supprimé dans /etc/profile." ; tput sgr0
		echo -e "$COLINFO" "Le script va s'interrompre..." ; tput sgr0
		echo ""
		echo -e "$ROUGE" "Il faut sortir du shell, se relogger (en root) puis relancer le script." ; tput sgr0
		sleep 3
		exit
		fi
 
fi

# Configuration du pavé numérique au démarrage si c'est un pc de bureau
echo -e "$COLDEFAUT" "Cet ordinateur est-il un portable ? [o/N] " ; tput sgr0
read repport
if [ ! "$repport" = "o" -a ! "$repport" = "oui" ]; then
	echo -e "$COLINFO" "Cet ordinateur n'est pas un portable."
	echo -e "$COLINFO" "On activera le verrouillage numérique au démarrage."
	echo -e "$COLTXT"	
	NUMLOCK=o	
else
	echo -e "$COLINFO" "Cet ordinateur est un portable."
	echo -e "$COLINFO" "Pas de verrouillage numérique au démarrage."
	echo -e "$COLTXT"
	NUMLOCK=n
fi


# Configuration de l'integration de la machine : hostname, inventaire, proxy, parc
		defaultname=$(hostname)
		echo "Configuration des paramètres de l'intégration..."
		echo ""
		echo -e "$COLINFO" "Nom actuel de la machine : $defaultname" ; tput sgr0
		echo ""
		echo -e "$COLDEFAUT" "Quel nom choisissez-vous pour cette machine?" ; tput sgr0
		echo -e "$COLDEFAUT" "Validez par <entrée> si vous voulez conserver le nom $defaultname" ; tput sgr0
		read NOMMACH
		if [ -z "$NOMMACH" ]; then
		NOMMACH=$defaultname
		fi
		echo ""
		echo -e "$COLINFO" "Le nom choisi pour la machine est $NOMMACH." ; tput sgr0
		echo ""
		echo -e "$COLDEFAUT" "Dans quel parc sera intégrée cette machine ?" ; tput sgr0
		echo -e "$COLINFO" "Attention : seuls choix possibles : cdi ou sprofs" ; tput sgr0
		echo ""
		read PARK
		echo ""
		echo -e "$COLINFO" "Le client sera affecté dans le parc $PARK." ; tput sgr0
		echo ""
		
echo $PARK > /etc/parc.txt
chmod 744 /etc/parc.txt

		#echo -e "$COLDEFAUT" "A la fin de l'installation, voulez-vous une remontée des informations dans l'inventaire ?" ; tput sgr0
		#echo ""
		#LISTE=("[o] oui" "[n] non")  # liste de choix disponibles
		#select CHOIX in "${LISTE[@]}" ; do
		#	case $REPLY in
		#		1|o)
		#		echo -e "$COLINFO" "Vous avez choisi la remontée des infos de la machine dans l'inventaire."
		#		echo -e "$COLTXT"
		#		OCS=o
		#		break
		#		;;
		#		2|n)
		#		echo -e "$COLINFO" "Pas de remontée des infos de la machine dans l'inventaire."
		#		echo -e "$COLTXT"
		#		echo ""
		#		OCS=n
		#		break
		#		;;
		#	esac
		#echo ""		
		#done


		
		if [ $ROCHE = "n" ] ; then
		echo -e "$COLDEFAUT" "En principe, il existe un proxy http (Amon) sur ce réseau." ; tput sgr0
		echo -e "$COLDEFAUT" "Adresse du proxy existant sur ce réseau : $PROXY" ; tput sgr0
		echo -e "$COLDEFAUT" "Il est recommandé de déclarer ce proxy dans la configuration du client. " ; tput sgr0
		echo -e "$COLINFO" "En répondant oui, ce proxy sera déclaré automatiquement sur ce client. " ; tput sgr0
		echo ""
		echo -e "$COLDEFAUT" "Voulez-vous déclarer un proxy http-https (Amon ou autre) sur ce client ? [O/n] " ; tput sgr0
		echo ""
			read repamon
			if [ ! "$repamon" = "n" -a ! "$repamon" = "non" ]; then
			echo -e "$COLINFO" "Le proxy http adresse $PROXY sera déclaré sur ce client."
			echo -e "$COLTXT"	
			CONFPROXY=o	
			else
			echo -e "$COLINFO" "Pas de proxy http déclaré sur ce client."
			echo -e "$COLTXT"
			CONFPROXY=n
			fi
		

			if [ $CONFPROXY = "o" ] ; then
			echo "export http_proxy=\"http://$PROXY:3128\"" >> /etc/profile
			echo "export https_proxy=\"http://$PROXY:3128\"" >> /etc/profile
			else
			echo -e "$COLINFO" "Pas de proxy http !" ; tput sgr0
			sed -i '/export/d' /etc/profile
			fi
		fi
		
		echo -e "$COLDEFAUT" "Voulez-vous configurer un cache apt sur la machine ?" ; tput sgr0
		echo -e "$COLINFO" "S'il y a un cache apt sur ce réseau et que vous répondez "o", ce client s'en servira automatiquement." ; tput sgr0
		echo ""
		LISTE=("[o] oui" "[n] non")  # liste de choix disponibles
		select CHOIX in "${LISTE[@]}" ; do
			case $REPLY in
				1|o)
				echo -e "$COLINFO" "Configuration automatique d'un cache apt sur la machine." ; tput sgr0
				echo -e "$COLINFO" "Adresse du cache apt déclaré : $APTCACHE " ; tput sgr0
				CACHE=o
				break
				;;
				2|n)
				echo -e "$COLINFO" "Pas de cache apt à configurer sur la machine." ; tput sgr0
				echo ""
				CACHE=n
				break
				;;
			esac
		echo ""
		done


#if [ $CACHE = "o" ] ; then
#echo "Acquire::http { Proxy \"http://$APTCACHE:3142\"; };"> /etc/apt/apt.conf.d/99proxy
#else
# On neutralise le cache apt éventuel pendant l'intégration, il sera réactivé à la fin
> /etc/apt/apt.conf
> /etc/apt/apt.conf.d/99proxy
#fi

# Mise a jour de la machine
export DEBIAN_FRONTEND=noninteractive
export DEBIAN_PRIORITY=high

echo -e "$VERT" "Mise à jour du système..."
echo -e "$COLTXT"

# Resolution du probleme de lock
if [ -e "/var/lib/dpkg/lock" ]; then
	rm -f /var/lib/dpkg/lock
fi

# On lance une maj
apt-get update || ERREUR="1"
# Arret du script en cas d'erreur
AFFICHE_ERREUR

apt-get -y upgrade || ERREUR="1"
# Arret du script en cas d'erreur
AFFICHE_ERREUR

apt-get install -y dirmngr xdg-user-dirs
# Configuration de xdg-user-dirs
sed -i 's/Desktop/Bureau/g' /etc/xdg/user-dirs.defaults

# Installation des paquets necessaires
echo "Installation des paquets nécessaires:"
echo "Ne rien remplir, les fichiers seront configurés/modifiés automatiquement ensuite..."
echo ""
apt-get install -y --force-yes samba-libs samba-common samba-common-bin smbclient libnss-ldap lsof libpam-mount cifs-utils ntpdate ssh aptitude ldap-utils cron-apt numlockx dkms rsync
# apt-get install -y --force-yes samba

# Configuration des fichiers
echo "Configuration des fichiers pour SambaEdu3..."
echo ""

# Configuration du fichier /etc/hosts
echo "Configuration du fichier /etc/hosts"
echo ""
cp /etc/hosts /etc/hosts_sauve_$DATERAPPORT
OK_SE3=`cat /etc/hosts | grep $SE3_SERVER`
if [ -z "$OK_SE3" ]; then
	echo "$SE3_IP	$SE3_SERVER" >> /etc/hosts
fi

ANCIENNOM=$(hostname)
cp /etc/hosts /etc/hosts_sauve_$DATERAPPORT
#sed -i 's/'$ANCIENNOM'/'$NOMMACH'/g' /etc/hosts
sed -i 's/^127.0.1.1.*/127.0.1.1\	'$NOMMACH'.'$RNE'.ac-clermont.fr\	'$NOMMACH'/g' /etc/hosts

echo "Configuration du fichier /etc/hostname"
echo ""
cp /etc/hostname /etc/hostname_sauve_$DATERAPPORT
echo "$NOMMACH" > /etc/hostname

# Configuration du fichier /etc/nsswitch.conf
echo "Configuration du fichier /etc/nsswitch.conf"
echo ""
cp /etc/nsswitch.conf /etc/nsswitch.conf_sauve_$DATERAPPORT
echo "
# /etc/nsswitch.conf
# Configuration pour SambaEdu3

passwd:         files ldap
group:          files ldap
shadow:         files ldap

hosts:          files dns
networks:       files

protocols:      db files
services:       db files
ethers:         db files
rpc:            db files

netgroup:       nis" > /etc/nsswitch.conf

# Configuration du fichier /etc/libnss-ldap.conf
echo "Configuration du fichier /etc/libnss-ldap.conf"
echo ""
cp /etc/libnss-ldap.conf /etc/libnss-ldap.conf_sauve_$DATERAPPORT
echo "
# /etc/libnss-ldap.conf
# Configuration pour SambaEdu3

host $LDAP_SERVER
base $LDAP_SUFFIX
uri ldap://$LDAP_SERVER
ldap_version 3
rootbinddn $LDAP_ADMIN_DN

# The port.
# Optional: default is 389.
#port 389

bind_policy soft" > /etc/libnss-ldap.conf


# Configuration du fichier /etc/pam_ldap.conf
echo "Configuration du fichier /etc/pam_ldap.conf"
echo ""
cp /etc/pam_ldap.conf /etc/pam_ldap.conf_sauve_$DATERAPPORT
echo "
# /etc/pam_ldap.conf
# Configuration pour SambaEdu3

host $LDAP_SERVER
base $LDAP_SUFFIX
uri ldap://$LDAP_SERVER
ldap_version 3

# The port.
# Optional: default is 389.
#port 389

bind_policy soft" > /etc/pam_ldap.conf

# Configuration du fichier /etc/ldap.conf
echo "Configuration du fichier /etc/pam_ldap.conf"
echo ""
cp /etc/ldap.conf /etc/ldap.conf_sauve_$DATERAPPORT
echo "
# /etc/ldap.conf
# Configuration pour SambaEdu3

host $LDAP_SERVER
base $LDAP_SUFFIX
uri ldap://$LDAP_SERVER
ldap_version 3

# The port.
# Optional: default is 389.
#port 389

bind_policy soft" > /etc/ldap.conf

# Configuration du fichier /etc/libnss-ldap.secret
echo "Configuration du fichier /etc/libnss-ldap.secret "
echo ""
echo -n "$PASS_LDAP" > /etc/libnss-ldap.secret

# Configuration du fichier /etc/pam.d/common-auth
echo "Configuration du fichier /etc/pam.d/common-auth"
echo ""
cp /etc/pam.d/common-auth /etc/pam.d/common-auth_sauve_$DATERAPPORT
echo "
# /etc/pam.d/common-auth
# Configuration pour SambaEdu3

auth	[success=2 default=ignore]	pam_unix.so nullok_secure
auth	[success=1 default=ignore]	pam_ldap.so use_first_pass
auth	requisite			pam_deny.so
auth	required			pam_permit.so
auth	optional			pam_mount.so
auth	optional			pam_group.so" > /etc/pam.d/common-auth

# Configuration du fichier /etc/pam.d/common-session
echo "Configuration du fichier /etc/pam.d/common-session"
echo ""
cp /etc/pam.d/common-session /etc/pam.d/common-session_sauve_$DATERAPPORT
echo "
# /etc/pam.d/common-session
# Configuration pour SambaEdu3

session	[default=1]	pam_permit.so
session	requisite	pam_deny.so
session	required	pam_permit.so
session	required	pam_mkhomedir.so	skel=/etc/skel/	umask=0022 
session	optional	pam_mount.so
session optional 	pam_exec.so type=open_session /usr/bin/logon1.sh
session optional 	pam_exec.so type=close_session /usr/bin/logout1.sh
session	optional	pam_ldap.so
session	optional	pam_loginuid.so
session	optional	pam_systemd.so" > /etc/pam.d/common-session

# Ajout de admin a la liste des sudoers avec pouvoir de root
#cp -a /etc/sudoers /etc/sudoers_sauve_$DATERAPPORT
#sed -i '/root /a\admin	ALL=(ALL) ALL' /etc/sudoers

# Configuration du fichier /etc/security/group.conf
echo "Configuration du fichier /etc/security/group.conf"
echo ""
cp /etc/security/group.conf /etc/security/group.conf_sauve_$DATERAPPORT
echo "
# /etc/security/group.conf
# Configuration pour SambaEdu3
lightdm;*;*;Al0000-2400;floppy,cdrom,audio,video,plugdev
gdm3;*;*;Al0000-2400;floppy,cdrom,audio,video,plugdev
xdm;*;*;Al0000-2400;floppy,cdrom,audio,video,plugdev" > /etc/security/group.conf

# Configuration du fichier /etc/samba/smb.conf
echo "Configuration du fichier /etc/samba/smb.conf"
echo ""
mv /etc/samba/smb.conf /etc/samba/smb.conf.ori
echo "[global]
domain master = False
local master = no
#lm announce = False
#preferred master = no
#smb ports = 445
workgroup = $DOMAIN
netbios name = $NOMMACH
#wins server = $SE3_IP
#security = DOMAIN" > /etc/samba/smb.conf

# Désactivation de l'affichage de la liste des utilisateurs antérieurs (gdm3 uniquement)
#sed -i 's/# disable-user-list=true/disable-user-list=true/g' /etc/gdm3/greeter.gsettings

# Création des points de montage
mkdir -p /media/Classes
mkdir -p /media/Partages

# Configuration du fichier /etc/security/pam_mount.conf.xml
cp /etc/security/pam_mount.conf.xml /etc/security/pam_mount.conf.xml_sauve_$DATERAPPORT
echo "Configuration du fichier /etc/security/pam_mount.conf.xml"
echo ""

echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>

<!--
/etc/security/pam_mount.conf.xml
Configuration pour SambaEdu3
-->

<pam_mount>
  <debug enable=\"1\" />
  <logout wait=\"0\" hup=\"0\" term=\"0\" kill=\"0\" />
  <mkmountpoint enable=\"1\" remove=\"true\" />

  <umount>umount %(MNTPT)</umount>

  <volume
    pgrp=\"lcs-users\"
    fstype=\"cifs\"
    server=\"$SE3_IP\"
    path=\"%(USER)\" 
    mountpoint=\"/home/%(USER)\"
    options=\"user=%(USER),nobrl,serverino,iocharset=utf8,sec=ntlmv2,vers=1.0\"
  />

  <volume
    pgrp=\"lcs-users\"
    fstype=\"cifs\"
    server=\"$SE3_IP\"
    path=\"Classes\" 
    mountpoint=\"/media/Classes\"
    options=\"user=%(USER),nobrl,serverino,iocharset=utf8,sec=ntlmv2,vers=1.0\"
  />

  <volume
    pgrp=\"lcs-users\"
    fstype=\"cifs\"
    server=\"$SE3_IP\"
    path=\"Docs\" 
    mountpoint=\"/media/Partages\"
    options=\"user=%(USER),nobrl,serverino,iocharset=utf8,sec=ntlmv2,vers=1.0\"
  />

<msg-authpw>Mot de passe :</msg-authpw>
<msg-sessionpw>Mot de passe :</msg-sessionpw>

</pam_mount>" > /etc/security/pam_mount.conf.xml

killall trackerd 2>/dev/null
killall bluetooth-applet 2>/dev/null

# Modifications pour le montage sur les clients d'un partage du se3
if [ ! -e /mnt/netlogon ] ; then
mkdir -p /mnt/netlogon
chown -R admin:admins /mnt/netlogon
chmod -R 755 /mnt/netlogon
fi

# Création d'une entrée dans /etc/fstab pour monter un répertoire partagé du se3
sed -i '/linux/d' /etc/fstab
sed -i '/^$/d' /etc/fstab
echo "//$SE3_IP/netlogon-linux  /mnt/netlogon   cifs    guest,nobrl,serverino,iocharset=utf8,sec=ntlmv2,vers=1.0 0       0" >> /etc/fstab

# On teste si lxde est installé
lxde=/etc/xdg/lxsession
if [ -e $lxde ]; then
# Lancement des scripts "unefois" au chargement de lightdm
echo "#!/bin/bash
#sleep 4
#mount -a
sleep 1
sh /mnt/netlogon/conex/init_unefois.sh
exit 0" > /usr/bin/init1.sh
# Activation du pavé numérique au démarrage si demandé
	if [ $NUMLOCK = "o" ] ; then
	sed -i '/exit 0/d' /usr/bin/init1.sh
	sed -i '/^$/d' /usr/bin/init1.sh
	echo "/usr/bin/X11/numlockx on
exit 0" >> /usr/bin/init1.sh
	fi
chmod +x /usr/bin/init1.sh

sed -i 's/#greeter-setup-script=/greeter-setup-script=\/usr\/bin\/init1.sh/g' /etc/lightdm/lightdm.conf


# Configuration de l'ouverture de session
# Créations de liens sur le bureau si on utilise lxde
echo "#!/bin/bash
sh /mnt/netlogon/conex/createlinks.sh &
exit 0" > /usr/bin/logon1.sh
chmod +x /usr/bin/logon1.sh

# Configuration de la fermeture de session
# kill des processus de l'utilisateur courant et effacement du lock firefox si présent
# Effacement des raccourcis (liens symboliques) créés a l'ouverture de session
echo "#!/bin/bash
sh /mnt/netlogon/conex/deletelinks.sh
exit 0" > /usr/bin/logout1.sh
chmod +x /usr/bin/logout1.sh

fi

# Configuration du fichier /etc/default/ntpdate
echo "Configuration du fichier /etc/default/ntpdate"
cp /etc/default/ntpdate /etc/default/ntpdate_sauve_$DATERAPPORT
echo "
# /etc/default/ntpdate
# Configuration pour SambaEdu3

NTPSERVERS=\"$NTPSERVERS\"

# additional options for ntpdate
# $NTPOPTIONS" > /etc/default/ntpdate

# On récupère les clés publiques du serveur
mkdir -p /root/.ssh
chmod 700 /root/.ssh
cd /root/.ssh
if [ -f "authorized_keys" ];then
wget -o log_recuperation_cle_pub_se3_$DATERAPPORT -O authorized_keys_se3 $SE3_IP:909/authorized_keys
cat authorized_keys_se3 >> authorized_keys
wget -o log_recuperation_cle_pub_wwwse3_$DATERAPPORT -O authorized_keys_wwwse3 $SE3_IP:909/authorized_keys_wwwse3
cat authorized_keys_wwwse3 >> authorized_keys
else
wget -o log_recuperation_cle_pub_se3_$DATERAPPORT -O authorized_keys $SE3_IP:909/authorized_keys
wget -o log_recuperation_cle_pub_wwwse3_$DATERAPPORT -O authorized_keys_wwwse3 $SE3_IP:909/authorized_keys_wwwse3
cat authorized_keys_wwwse3 >> authorized_keys
fi
#chmod 400 /root/.ssh/authorized_keys
cd

# Configuration de /etc/cron-apt pour n'installer que les maj de sécurité
echo "Configuration de /etc/cron-apt"
echo ""
less /etc/apt/sources.list|grep security > /etc/apt/security.sources.list
cp /etc/cron-apt/config /etc/cron-apt/config_sauve_$DATERAPPORT

echo "# /etc/cron-apt/config
# Configuration pour SambaEdu3
OPTIONS=\"-o quiet=1 -o Dir::Etc::SourceList=/etc/apt/security.sources.list\"" > /etc/cron-apt/config

echo "# Configuration pour Samba Edu3
dist-upgrade -y -o APT::Get::Show-Upgraded=true" > /etc/cron-apt/action.d/5-install
ln -s /usr/sbin/cron-apt /etc/cron.daily/cron-apt

export DEBIAN_FRONTEND=dialog

# Alias pour l'extinction par root
testpoweroff=$(cat /root/.bashrc | grep poweroff)
if [ ! -n "$testpoweroff" ] ; then
echo "alias halt='poweroff'" >> /root/.bashrc
fi

# Activation des maj automatiques
apt-get install --assume-yes unattended-upgrades
cp /usr/share/unattended-upgrades/20auto-upgrades /etc/apt/apt.conf.d/20auto-upgrades
cd /etc/apt/apt.conf.d
rm -f 50unattended-upgrades
wget --no-check-certificate $FTP/buster/50unattended-upgrades

# Configuration de fusion_inventory
if [ $OCS = "o" ] ; then
echo "Configuration de fusion_inventory"
echo ""
aptitude -y purge ocsinventory-agent
aptitude -y install fusioninventory-agent
cp /etc/default/fusioninventory-agent /etc/default/fusioninventory-agent.ori
sed -i 's/MODE=cron/MODE=daemon/g' /etc/default/fusioninventory-agent
echo "server=$SERVINV">/etc/fusioninventory/agent.cfg
echo "tag=$TAG">>/etc/fusioninventory/agent.cfg
	if [ $CONFPROXY = "o" ] ; then
echo "proxy = $PROXY">> /etc/fusioninventory/agent.cfg
	else
		if [ $ROCHE = "o" ] ; then
		echo "proxy = http://172.17.139.253:3128" >> /etc/fusioninventory/agent.cfg
		fi
	fi
service fusioninventory-agent restart
fi

# Suppression de clipit et de screenlock
cd /usr/share/applications
mv lxde-screenlock.desktop lxde-screenlock.desktop.bak
cd /etc/xdg/autostart
mv clipit-startup.desktop clipit-startup.desktop.bak
mv /etc/xdg/autostart/light-locker.desktop /etc/xdg/autostart/light-locker.desktop.bak

# Suppression de wicd
echo "Suppression de wicd..."
echo ""
systemctl stop wicd
systemctl disable wicd
systemctl mask wicd
apt-get remove --purge -y wicd
cd /etc/xdg/autostart
mv wicd-tray.desktop wicd-tray.desktop.bak
apt-get -y autoremove
apt-get clean
apt-get autoclean

# Installation de network-manager
apt-get install -y network-manager

# Configuration de network-manager
sed -i 's/false/true/g' /etc/NetworkManager/NetworkManager.conf

# Demarrage des services au boot
# Activation des services
#systemctl enable NetworkManager-wait-online.service
systemctl enable ssh
#systemctl enable smbd

# On fait le ménage si la machine a deja ete intégrée
# [ -e /root/temoins ] && rm -f /root/temoins/*

# On ajoute l'Amon comme proxy http si besoin
if [ $CONFPROXY = "o" ] ; then
	sed -i '/export/d' /etc/profile
	sed -i '/Configuration/d' /etc/profile
	echo "#Configuration du proxy pour l'établissement
	export http_proxy=\"http://$PROXY:3128\"
	export https_proxy=\"http://$PROXY:3128\"" >> /etc/profile
	else
	if [ $ROCHE = "n" ] ; then
	sed -i '/export/d' /etc/profile
	sed -i '/Configuration/d' /etc/profile
	fi
fi

# On configure le cache apt si nécessaire
#if [ $CACHE = "o" ] ; then
#echo "Acquire::http { Proxy \"http://$APTCACHE:3142\"; };"> /etc/apt/apt.conf.d/99proxy
#echo "Avec cache" > /etc/monproxy
#else
#> /etc/apt/apt.conf
#> /etc/apt/apt.conf.d/99proxy
#echo "Sans cache" > /etc/monproxy
#fi

# Datage du script (témoin de passage dans /root)
cd /root
rm -f integration_*
rm -f SE3_rapport_integration*
echo "Script d'intégration version $VERSION du $DATESCRIPT passe le $DATERAPPORT" >> /root/integration_$VERSION_$DATERAPPORT

# Fin de la configuration
echo -e "$VERT" "Fin de l'installation."
echo -e "$COLTXT"
echo ""
echo -e "$COLINFO"
echo "ATTENTION : Seul les comptes ayant un shell pourront se connecter !"
echo ""
echo -e "$COLTXT"
echo -e "$ROUGE"
echo "Intégration terminée !"
echo "Le système redémarrera dans 10 secondes..."
sleep 10
echo -e "$COLTXT"

#} | tee /root/SE3_rapport_integration_stretch_$DATERAPPORT
reboot

