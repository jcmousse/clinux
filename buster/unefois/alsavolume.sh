#!/bin/bash
TEST_ALSA=$(cat /home/$USER/.config/lxpanel/LXDE/panels/panel | grep "volumealsa")
if [ ! -n "$TEST_ALSA" ]; then
echo "Plugin {
    type = volumealsa
}" >> /home/$USER/.config/lxpanel/LXDE/panels/panel
fi
exit 0

